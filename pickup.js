var pickup_items = {
    "common": {
        "0": "Potion",
        "1": "Antidote",
        "2": "Super Potion",
        "3": "Great Ball",
        "4": "Sweet Heart",
        "5": "Type Gem",
        "6": "Full Heal",
        "7": "Hyper Potion",
        "8": "Ultra Ball",
        "9": "Revive"
    },

    "uncommon": {
        "0": ["Antidote", "Super Potion", "Great Ball", "Sweet Heart", "Type Gem", "Full Heal"],
        "1": ["Super Potion", "Great Ball", "Sweet Heart", "Type Gem", "Full Heal", "Hyper Potion"],
        "2": ["Great Ball", "Sweet Heart", "Type Gem", "Full Heal", "Hyper Potion", "Ultra Ball"],
        "3": ["Sweet Heart", "Type Gem", "Full Heal", "Hyper Potion", "Ultra Ball", "Revive"],
        "4": ["Type Gem", "Full Heal", "Hyper Potion", "Ultra Ball", "Revive", "Rare Candy"],
        "5": ["Full Heal", "Hyper Potion", "Ultra Ball", "Revive", "Rare Candy", "Leaf Stone"],
        "6": ["Hyper Potion", "Ultra Ball", "Revive", "Rare Candy", "Thunderstone", "Water Stone"],
        "7": ["Ultra Ball", "Revive", "Rare Candy", "Fire Stone", "Water Stone", "Big Mushroom"],
        "8": ["Revive", "Rare Candy", "Fire Stone", "Sun Stone", "Big Mushroom", "Full Restore"],
        "9": ["Rare Candy", "Shiny Stone", "Sun Stone", "Big Mushroom", "Full Restore", "Max Revive"]
    },

    "rare": {

        "0": ["Hyper Potion", "Ultra Ball"],
        "1": ["Ultra Ball", "Revive"],
        "2": ["Revive", "Rare Candy"],
        "3": ["Rare Candy", "Everstone"],
        "4": ["Everstone", "Leaf Stone"],
        "5": ["Thunderstone", "Big Mushroom"],
        "6": ["Big Mushroom", "Full Restore"],
        "7": ["Full Restore", "Max Revive"],
        "8": ["Max Revive", "Dawn Stone"],
        "9": ["Dawn Stone", "Dusk Stone"]
    },

    "vrare": {

        "0": ["Revive", "Quick Claw"],
        "1": ["Quick Claw", "King's Rock"],
        "2": ["King's Rock", "Full Restore"],
        "3": ["Full Restore", "Metronome"],
        "4": ["Metronome", "Iron Ball"],
        "5": ["Iron Ball", "Sage Crystal"],
        "6": ["Prism Scale", "Expert Belt"],
        "7": ["Expert Belt", "Snow Crown"],
        "8": ["Brave Bracer", "Leftovers"],
        "9": ["Leftovers", "Elite Scale"]
    }
};

var type_gems = [
        "Fire Gem",
        "Water Gem",
        "Electric Gem",
        "Grass Gem",
        "Ice Gem",
        "Fighting Gem",
        "Poison Gem",
        "Ground Gem",
        "Flying Gem",
        "Psychic Gem",
        "Bug Gem",
        "Rock Gem",
        "Ghost Gem",
        "Dark Gem",
        "Steel Gem",
        "Dragon Gem",
        "Normal Gem"
];

var vowels = [
	"A",
	"E",
	"I",
	"O",
	"U",
	"Y"
];

function clean_input(input) {

	var level = parseInt(input);

	if (isNaN(level))
	{
		document.getElementById('error').firstChild.nodeValue = "Please enter the pokemon's level in the box provided. The level must be an integer.";
		document.getElementById('results').firstChild.nodeValue = "";
		return false;
	} else if (level < 1 || level > 100) {
		document.getElementById('error').firstChild.nodeValue = "The pokemon's level must be between 1 and 100.";
		document.getElementById('results').firstChild.nodeValue = "";
		return false;
	} else {
		return level;
	}
}

function try_pickup(level) {

	if (clean_input(level)) {

	    // first, does pickup even go off?
	    var pickuproll = Math.random();

	    if (pickuproll > 0.1) {

		var result = 'absolutely nothing!';

	    } else {

		// what rarity of item?
		var catroll = Math.random();

		if (catroll <= 0.02) {
		    var category = 'vrare';
		} else if (catroll > 0.02 && catroll <= 0.10) {
		    var category = 'rare';
		} else if (catroll > 0.10 && catroll <= 0.70) {
		    var category = 'uncommon';
		} else {
		    var category = 'common';
		}

		// Now choose bracket based on pokémon's level
		if (level === 1) {
		    var bracket = '0';
		} else {
		    var bracket = Math.floor((level - 1) / 10).toString();
		}

		// now choose a random item
		// from the appropriate rarity/bracket
		if (category === 'common') {
		    // only one item here
		    // just choose it
		    var item = pickup_items[category][bracket];
		} else {
		    itemroll = Math.floor(Math.random() * pickup_items[category][bracket].length);
		    //console.log('itemroll ' + itemroll.toString());
		    var item = pickup_items[category][bracket][itemroll];
		}

		if (item === 'Type Gem') {
		    // one more roll to make...
		    gemroll = Math.floor(Math.random() * type_gems.length);
		    item = type_gems[gemroll];
		}

		var article = 'a ';

		// which message do we want?
		// ugh, there must be some better way
		// to check for array membership...
		for (var i = 0; i < vowels.length; i++) {
			if (item[0] == vowels[i]) {
				article = 'an ';
				break;
			}
		}

		var result = article + item + '!';
	    }

	    document.getElementById('error').firstChild.nodeValue = "";
	    document.getElementById('results').firstChild.nodeValue = 'The pokemon picked up... ' + result;
	}
}
