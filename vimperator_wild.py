#!./env/bin/python
# -*- encoding: utf-8 -*-

'''
    A temporary, manually-activated script intended for execution from Vimperator. Takes the url of a thread, determines the player a wild needs to be generated for, the location they're searching, and outputs that trainer in a format that can easily be copy-and-pasted into the final reply box.

'''

import argparse
import codecs
import requests
from lxml import etree
from ConfigParser import SafeConfigParser

from turquoise_utils import get_profile
from mod_utils import parse_profile, get_wild, get_location, format_pokemon

parser = argparse.ArgumentParser(description='get a random wild pokémon for the player in a target thread')
parser.add_argument('thread', help='the complete url of the thread for which a new wild needs to be generated')
parser.add_argument('--area', help='the sub-area of the location to be searched for a wild.', type=str, default='')
args = parser.parse_args()

# Set up resource info
config = SafeConfigParser()

with codecs.open('turquoise.cfg', 'r', encoding='utf-8') as f:
    config.readfp(f)

# GET the thread contents
r = requests.get(args.thread)

html_parser = etree.HTMLParser(encoding='utf-8')
thread = etree.fromstring(r.text, html_parser)

'''
# Extract posts and get player info
posts = thread.xpath('id("posts")/table/@id')
last_pid = posts[-1][posts[-1].rfind('_') + 1:]
'''

profile_raw = get_profile(thread, html_parser)

player = parse_profile(profile_raw)

# Determine the location id
loc = get_location(thread, config.get('database', 'main_db'), args.area)

if loc:
    # Now actually get the pokémon...
    mon = get_wild(player['team'], loc, config.get('database', 'main_db'))
    if mon:
        print 'A wild {0} appeared!\n{1}'.format(mon['name'], format_pokemon(mon))
