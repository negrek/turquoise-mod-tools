#!./env/bin/python
# -*- encoding: utf-8 -*-

'''

A small script that takes a list of pokémon and levels, then generates a nicely formatted list of them, ready for pasting into a thread somewhere. Does all the heavy lifting of sex and ability rolling, determining HP, and so on.

Really just a straight-up wrapper for the "generate_pokemon" function.

TODO: Appropriate error message if login info is incorrect.
TODO: Way to add TM moves to generated profile 'mons? Specified special moves in general
TODO: Specifying dreamworld abilities?

'''

import argparse
from ConfigParser import SafeConfigParser
import sqlite3
import codecs
import random

from mod_utils import generate_pokemon, format_pokemon, parse_profile, edit_profile
from turquoise_utils import turquoise_login, get_profile, edit_post

parser = argparse.ArgumentParser(description='generate any number of pokémon of specified species and level')
parser.add_argument('pokemon', help='set of any number of pokemon:level pairs to generate. Example: vulkhet 5 spraylet 10 josuche 7', nargs='+')
parser.add_argument('-m', '--moves', help='give these pokémon X special moves (each)', type=int, default=0)
parser.add_argument('-r', '--random_moves', help='roll for random moves on these pokémon, using the same formula used in random trainer generation. Overrides the "moves" flag if it\'s set', action='store_true')
parser.add_argument('-i', '--item', help='roll for wild held items on these pokémon', action='store_true')
parser.add_argument('-s', '--shiny', help='roll for shininess on these pokémon', action='store_true')
parser.add_argument('-p', '--profile', help='URL of the profile this pokémon should be added to, if applicable', type=str)
parser.add_argument('-l', '--location', help='if this pokémon is being added to a profile, should it be added to the PC or their team? (Default is PC)', type=str, default='pc', choices=['pc', 'team'])
parser.add_argument('-n', '--nickname', help='if this pokémon is being added to a profile, give it this nickname', type=str)
parser.add_argument('--nickname_f', help='if this pokémon is being added to a profile and is female, give it this nickname', type=str)
parser.add_argument('--nickname_m', help='if this pokémon is being added to a profile and is male, give it this nickname', type=str)
parser.add_argument('-o', '--original_trainer', help='manually set the pokémon\'s OT (if this is not set, defaults to the target player\'s OT)', type=str)
args = parser.parse_args()

# Set up resource info
config = SafeConfigParser()

with codecs.open('turquoise.cfg', 'r', encoding='utf-8') as f:
    config.readfp(f)

con = sqlite3.connect(config.get('database', 'main_db'))
cursor = con.cursor()

mon_tuples = [(mon, int(level)) for mon, level in zip(args.pokemon[::2], args.pokemon[1::2])]

# Log in and get the player's profile, if that's where we're going to stick this
if args.profile:

    from lxml import etree
    import requests

    html_parser = etree.HTMLParser(encoding='utf-8')

    session = turquoise_login(config.get('account_info', 'username'), config.get('account_info', 'password'))

    # Note how we don't use the session we just made here
    # When a logged-in user requests this page, the page structure
    # is different and proceeds to break the XPath used to select profile info.
    # Fun!
    r = requests.get(args.profile)
    profile_raw = etree.fromstring(r.text, html_parser)

    player = parse_profile(profile_raw)

for mon in mon_tuples:
    species_id = cursor.execute('''select id from pokemon where identifier=?''', (mon[0].lower(),))

    mon_id = species_id.fetchone()

    if mon_id:

        # Do we need to roll for a number of special moves for this 'mon?
        if args.random_moves:
            # Do the thing
            special_moves = sum([1 for i in xrange(mon[1]) if random.random() <= 0.003])
        else:
            special_moves = args.moves

        pokemon_raw = generate_pokemon(cursor, mon_id[0], mon[1], get_item=args.item, get_shiny=args.shiny, special_moves=special_moves)

        # Hokay, now either into the profile or onto the screen...
        if args.profile:
            # We need to get all appropriate level-up moves
            # for the 'mon's move list
            movequery = cursor.execute('''select moves.name from pokemon_moves left join moves on pokemon_moves.move_id = moves.id where pokemon_moves.move_method_id = 1 and pokemon_moves.pokemon_id = ? and pokemon_moves.level_learned <= ?''', (mon_id[0], mon[1]))
            moves = movequery.fetchall()
            moves_flattened = [move[0] for move in moves]

            # Now add any special moves
            if 'special_moves' in pokemon_raw:
                moves_flattened += pokemon_raw['special_moves']

            pokemon_raw['moves_literal'] = ', '.join(moves_flattened)

            # Add the nickname, if applicable
            if args.nickname_f and pokemon_raw['sex'] == 'f':
                pokemon_raw['name'] = args.nickname_f
            elif args.nickname_m and pokemon_raw['sex'] == 'male':
                pokemon_raw['name'] = args.nickname_m
            elif args.nickname:
                pokemon_raw['name'] = args.nickname

            # Either add custom OT or generate it from profile data
            if args.original_trainer:
                pokemon_raw['ot'] = args.original_trainer
            else:
                pokemon_raw['ot'] = '{0} ({1})'.format(player['name'], player['player'])

            # To make this compatible with our formatting function,
            # add a "max" declaration to represent HP
            pokemon_raw['max'] = pokemon_raw['hp']

            # It's assumed that the pokémon's EXP should be zero
            pokemon_raw['exp'] = 0

            # Append pokémon's info to the appropriate part of their profile
            if args.location == 'pc':
                player['pc'][-1] = pokemon_raw
            else:
                player['team'][-1] = pokemon_raw

            # Update the appropriate profile post
            profile_posts = edit_profile(player)

            if args.location == 'pc':
                edit_post(session, config, player['profile_ids'][2], profile_posts[2])
            else:
                edit_post(session, config, player['profile_ids'][1], profile_posts[1])

            print 'Profile successfully updated!'

        else:
            print format_pokemon(pokemon_raw)

    else:
        print "Sorry, I don't know any pokémon called {0}!".format(mon[0])
        break

cursor.close()
con.close()
