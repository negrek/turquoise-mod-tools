#!/usr/bin/env python
# -*- encoding: utf-8 -*-

'''
    A dead-simple dice roller for simple rpg rng tasks.
'''

from random import randint
import argparse

parser = argparse.ArgumentParser(description='get any number of random rolls between one and some maximum value (default 100)')
parser.add_argument('number', help='number of times to roll', type=int)
parser.add_argument('--max', '-m', help='upper bound of rolls', type=int, default=100)
args = parser.parse_args()

for i in xrange(args.number):
    print randint(1, args.max)
