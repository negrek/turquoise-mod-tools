#!./env/bin/python
# -*- encoding: utf-8 -*-

'''
    A temporary, manually-activated script intended for execution from Vimperator. Takes the url of a thread, determines the player a trainer needs to be generated for, and outputs that trainer in a format that can easily be copy-and-pasted into the final reply box.

'''

from sys import argv
from lxml import etree
from ConfigParser import SafeConfigParser
import codecs
import requests

from turquoise_utils import get_profile
from mod_utils import parse_profile, get_trainer

# Set up resource info
config = SafeConfigParser()

with codecs.open('turquoise.cfg', 'r', encoding='utf-8') as f:
    config.readfp(f)

r = requests.get(argv[1])

html_parser = etree.HTMLParser(encoding='utf-8')

thread = etree.fromstring(r.text, html_parser)
'''
posts = thread.xpath('id("posts")/table/@id')

# It seems pretty dumb to be stripping the 'post_'
# off here and then re-adding it in the construction
# of the profile url... but I guess that makes it
# maximally agnostic? In some cases we might just have
# the number, I suppose.
last_pid = posts[-1][posts[-1].rfind('_') + 1:]
'''

profile_raw = get_profile(thread, html_parser)

# GET and parse the profile for the person who made the last post
player = parse_profile(profile_raw)

trainer = get_trainer(player['team'], config.get('database', 'main_db'), config.get('database', 'trainer_db'))

print trainer
