The files honko_base.js and util.js are based on [Honko's damage calculator](http://pokemonshowdown.com/damagecalc/), which is in turn based on [AMF's damage calculator](http://pokedex-br.info/damage.html), neither of which is released under any particular license.

All fan-made content related to the Pokémon Turquoise RPG, including fake pokémon, items, abilities, and other original concepts, are &copy; Pokémon Turquoise and may not be used without permission; see http://turquoise.alteredorigin.net/w/TurquoiseDex:General_disclaimer for further information.

All other code is released under an MIT license:

Copyright (C) 2013 Negrek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
