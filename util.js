// This is all ripped off the "util.js" script
// from http://pokemonshowdown.com/damagecalc/util.js
// and modified for use with Turquoise, where all the pokémon are SO SRS, etc.

/* 7/28/13

   - Hardcoded IV's = 31, EV's = 252, nature = 1 for both the calcStat and calcHP functions.
   - Added NL-exclusive moves to ALL_ATTACKS
   - Redid the whole moves section because previously there were attacks *missing* (at least Ember and Dragon Rage)
   - Added ALL_POKEMON

10/22/13

 - Hardcoded IV's = 0, EV's = 0

*/

/* TODO

   - Fix the 'speed'/'user speed' issue for attackers/defenders; should only affect moves like electro ball, but eh
   - Add various Turquoise-related ability modifiers and multipliers (Draft Rider, Lucky Shot, etc.)
   - Add Turquoise-related weather stuffs
*/

function calcStat(base, level) {
    return Math.floor(Math.floor(2*base) * level/100 + 5);
}

function calcHP(base, level) {
    if(base == 1) return 1; //shedinja
    else return Math.floor(Math.floor(2*base + 94) * level/100 + 1*level + 10);
}

function getTypeMatch(atkType, defType) {
    var atk = ALL_TYPES[atkType];
    if (atk.nve && atk.nve.indexOf(defType) !== -1) return 0.5;
    if (atk.se && atk.se.indexOf(defType) !== -1) return 2;
    if (atk.immune && atk.immune.indexOf(defType) !== -1) return 0;
    return 1;
}

var ALL_ATTACKS = {
    // Format: 'Name': [bp, type, class, base_crit],
    'Pound': [40, 'Normal', 'Physical', 0],
    'Karate Chop': [50, 'Fighting', 'Physical', 2],
    'Double Slap': [15, 'Normal', 'Physical', 0],
    'Comet Punch': [18, 'Normal', 'Physical', 0],
    'Mega Punch': [80, 'Normal', 'Physical', 0],
    'Pay Day': [40, 'Normal', 'Physical', 0],
    'Fire Punch': [75, 'Fire', 'Physical', 0],
    'Ice Punch': [75, 'Ice', 'Physical', 0],
    'Thunder Punch': [75, 'Electric', 'Physical', 0],
    'Scratch': [40, 'Normal', 'Physical', 0],
    'Vice Grip': [55, 'Normal', 'Physical', 0],
    'Guillotine': [1, 'Normal', 'Physical', 0],
    'Razor Wind': [80, 'Normal', 'Special', 0],
    'Cut': [65, 'Steel', 'Physical', 0],
    'Gust': [40, 'Flying', 'Special', 0],
    'Wing Attack': [60, 'Flying', 'Physical', 0],
    'Fly': [90, 'Flying', 'Physical', 0],
    'Bind': [15, 'Normal', 'Physical', 0],
    'Slam': [80, 'Normal', 'Physical', 0],
    'Vine Whip': [45, 'Grass', 'Physical', 0],
    'Stomp': [65, 'Normal', 'Physical', 0],
    'Double Kick': [30, 'Fighting', 'Physical', 0],
    'Mega Kick': [120, 'Normal', 'Physical', 0],
    'Jump Kick': [100, 'Fighting', 'Physical', 0],
    'Rolling Kick': [60, 'Fighting', 'Physical', 0],
    'Headbutt': [70, 'Normal', 'Physical', 0],
    'Horn Attack': [65, 'Normal', 'Physical', 0],
    'Fury Attack': [15, 'Normal', 'Physical', 0],
    'Horn Drill': [1, 'Normal', 'Physical', 0],
    'Tackle': [50, 'Normal', 'Physical', 0],
    'Body Slam': [85, 'Normal', 'Physical', 0],
    'Wrap': [15, 'Normal', 'Physical', 0],
    'Take Down': [90, 'Normal', 'Physical', 0],
    'Thrash': [120, 'Normal', 'Physical', 0],
    'Double-Edge': [120, 'Normal', 'Physical', 0],
    'Poison Sting': [15, 'Poison', 'Physical', 0],
    'Twineedle': [25, 'Bug', 'Physical', 0],
    'Pin Missile': [25, 'Bug', 'Physical', 0],
    'Bite': [60, 'Dark', 'Physical', 0],
    'Sonic Boom': [1, 'Normal', 'Special', 0],
    'Acid': [40, 'Poison', 'Special', 0],
    'Ember': [40, 'Fire', 'Special', 0],
    'Flamethrower': [90, 'Fire', 'Special', 0],
    'Water Gun': [40, 'Water', 'Special', 0],
    'Hydro Pump': [110, 'Water', 'Special', 0],
    'Surf': [90, 'Water', 'Special', 0],
    'Ice Beam': [90, 'Ice', 'Special', 0],
    'Blizzard': [110, 'Ice', 'Special', 0],
    'Psybeam': [65, 'Psychic', 'Special', 0],
    'Bubble Beam': [65, 'Water', 'Special', 0],
    'Aurora Beam': [65, 'Ice', 'Special', 0],
    'Hyper Beam': [150, 'Normal', 'Special', 0],
    'Peck': [35, 'Flying', 'Physical', 0],
    'Drill Peck': [80, 'Flying', 'Physical', 0],
    'Submission': [80, 'Fighting', 'Physical', 0],
    'Low Kick': [1, 'Fighting', 'Physical', 0],
    'Counter': [1, 'Fighting', 'Physical', 0],
    'Seismic Toss': [1, 'Fighting', 'Physical', 0],
    'Strength': [80, 'Normal', 'Physical', 0],
    'Absorb': [20, 'Grass', 'Special', 0],
    'Mega Drain': [40, 'Grass', 'Special', 0],
    'Razor Leaf': [55, 'Grass', 'Physical', 2],
    'Solar Beam': [120, 'Grass', 'Special', 0],
    'Petal Dance': [120, 'Grass', 'Special', 0],
    'Dragon Rage': [1, 'Dragon', 'Special', 0],
    'Fire Spin': [35, 'Fire', 'Special', 0],
    'Thunder Shock': [40, 'Electric', 'Special', 0],
    'Thunderbolt': [90, 'Electric', 'Special', 0],
    'Thunder': [110, 'Electric', 'Special', 0],
    'Rock Throw': [50, 'Rock', 'Physical', 0],
    'Earthquake': [100, 'Ground', 'Physical', 0],
    'Fissure': [1, 'Ground', 'Physical', 0],
    'Dig': [80, 'Ground', 'Physical', 0],
    'Confusion': [50, 'Psychic', 'Special', 0],
    'Psychic': [90, 'Psychic', 'Special', 0],
    'Quick Attack': [40, 'Normal', 'Physical', 0],
    'Rage': [20, 'Normal', 'Physical', 0],
    'Night Shade': [1, 'Ghost', 'Special', 0],
    'Bide': [1, 'Normal', 'Physical', 0],
    'Self-Destruct': [200, 'Normal', 'Physical', 0],
    'Egg Bomb': [100, 'Normal', 'Physical', 0],
    'Lick': [30, 'Ghost', 'Physical', 0],
    'Smog': [30, 'Poison', 'Special', 0],
    'Sludge': [65, 'Poison', 'Special', 0],
    'Bone Club': [65, 'Ground', 'Physical', 0],
    'Fire Blast': [110, 'Fire', 'Special', 0],
    'Waterfall': [80, 'Water', 'Physical', 0],
    'Clamp': [35, 'Water', 'Physical', 0],
    'Swift': [60, 'Normal', 'Special', 0],
    'Skull Bash': [130, 'Normal', 'Physical', 0],
    'Spike Cannon': [20, 'Normal', 'Physical', 0],
    'Constrict': [10, 'Normal', 'Physical', 0],
    'High Jump Kick': [130, 'Fighting', 'Physical', 0],
    'Dream Eater': [100, 'Psychic', 'Special', 0],
    'Barrage': [15, 'Normal', 'Physical', 0],
    'Leech Life': [20, 'Bug', 'Physical', 0],
    'Sky Attack': [140, 'Flying', 'Physical', 2],
    'Bubble': [40, 'Water', 'Special', 0],
    'Dizzy Punch': [70, 'Normal', 'Physical', 0],
    'Psywave': [1, 'Psychic', 'Special', 0],
    'Crabhammer': [100, 'Water', 'Physical', 2],
    'Explosion': [250, 'Normal', 'Physical', 0],
    'Fury Swipes': [18, 'Normal', 'Physical', 0],
    'Bonemerang': [50, 'Ground', 'Physical', 0],
    'Rock Slide': [75, 'Rock', 'Physical', 0],
    'Hyper Fang': [80, 'Normal', 'Physical', 0],
    'Tri Attack': [80, 'Normal', 'Special', 0],
    'Super Fang': [1, 'Normal', 'Physical', 0],
    'Slash': [70, 'Normal', 'Physical', 2],
    'Struggle': [50, 'Normal', 'Physical', 0],
    'Triple Kick': [10, 'Fighting', 'Physical', 0],
    'Thief': [60, 'Dark', 'Physical', 0],
    'Flame Wheel': [60, 'Fire', 'Physical', 0],
    'Snore': [50, 'Normal', 'Special', 0],
    'Flail': [1, 'Normal', 'Physical', 0],
    'Aeroblast': [100, 'Flying', 'Special', 2],
    'Reversal': [1, 'Fighting', 'Physical', 0],
    'Powder Snow': [40, 'Ice', 'Special', 0],
    'Mach Punch': [40, 'Fighting', 'Physical', 0],
    'Feint Attack': [60, 'Dark', 'Physical', 0],
    'Sludge Bomb': [90, 'Poison', 'Special', 0],
    'Mud-Slap': [20, 'Ground', 'Special', 0],
    'Octazooka': [65, 'Water', 'Special', 0],
    'Zap Cannon': [120, 'Electric', 'Special', 0],
    'Icy Wind': [55, 'Ice', 'Special', 0],
    'Bone Rush': [25, 'Ground', 'Physical', 0],
    'Outrage': [120, 'Dragon', 'Physical', 0],
    'Giga Drain': [75, 'Grass', 'Special', 0],
    'Rollout': [30, 'Rock', 'Physical', 0],
    'False Swipe': [40, 'Normal', 'Physical', 0],
    'Spark': [65, 'Electric', 'Physical', 0],
    'Fury Cutter': [40, 'Bug', 'Physical', 0],
    'Steel Wing': [70, 'Steel', 'Physical', 0],
    'Return': [1, 'Normal', 'Physical', 0],
    'Present': [1, 'Normal', 'Physical', 0],
    'Frustration': [1, 'Normal', 'Physical', 0],
    'Sacred Fire': [100, 'Fire', 'Physical', 0],
    'Magnitude': [1, 'Ground', 'Physical', 0],
    'Dynamic Punch': [100, 'Fighting', 'Physical', 0],
    'Megahorn': [120, 'Bug', 'Physical', 0],
    'Dragon Breath': [60, 'Dragon', 'Special', 0],
    'Pursuit': [40, 'Dark', 'Physical', 0],
    'Rapid Spin': [20, 'Normal', 'Physical', 0],
    'Iron Tail': [100, 'Steel', 'Physical', 0],
    'Metal Claw': [50, 'Steel', 'Physical', 0],
    'Vital Throw': [70, 'Fighting', 'Physical', 0],
    'Hidden Power': [60, 'Normal', 'Special', 0],
    'Cross Chop': [100, 'Fighting', 'Physical', 2],
    'Twister': [40, 'Dragon', 'Special', 0],
    'Crunch': [80, 'Dark', 'Physical', 0],
    'Mirror Coat': [1, 'Psychic', 'Special', 0],
    'Extreme Speed': [80, 'Normal', 'Physical', 0],
    'Ancient Power': [60, 'Rock', 'Special', 0],
    'Shadow Ball': [80, 'Ghost', 'Special', 0],
    'Future Sight': [120, 'Psychic', 'Special', 0],
    'Rock Smash': [40, 'Fighting', 'Physical', 0],
    'Whirlpool': [35, 'Water', 'Special', 0],
    'Beat Up': [1, 'Dark', 'Physical', 0],
    'Fake Out': [40, 'Normal', 'Physical', 0],
    'Uproar': [90, 'Normal', 'Special', 0],
    'Spit Up': [1, 'Normal', 'Special', 0],
    'Heat Wave': [95, 'Fire', 'Special', 0],
    'Facade': [70, 'Normal', 'Physical', 0],
    'Focus Punch': [150, 'Fighting', 'Physical', 0],
    'Smelling Salts': [70, 'Normal', 'Physical', 0],
    'Superpower': [120, 'Fighting', 'Physical', 0],
    'Revenge': [60, 'Fighting', 'Physical', 0],
    'Brick Break': [75, 'Fighting', 'Physical', 0],
    'Knock Off': [65, 'Dark', 'Physical', 0],
    'Endeavor': [1, 'Normal', 'Physical', 0],
    'Eruption': [150, 'Fire', 'Special', 0],
    'Secret Power': [70, 'Normal', 'Physical', 0],
    'Dive': [80, 'Water', 'Physical', 0],
    'Arm Thrust': [15, 'Fighting', 'Physical', 0],
    'Luster Purge': [70, 'Psychic', 'Special', 0],
    'Mist Ball': [70, 'Psychic', 'Special', 0],
    'Blaze Kick': [85, 'Fire', 'Physical', 2],
    'Ice Ball': [30, 'Ice', 'Physical', 0],
    'Needle Arm': [60, 'Grass', 'Physical', 0],
    'Hyper Voice': [90, 'Normal', 'Special', 0],
    'Poison Fang': [50, 'Poison', 'Physical', 0],
    'Crush Claw': [75, 'Normal', 'Physical', 0],
    'Blast Burn': [150, 'Fire', 'Special', 0],
    'Hydro Cannon': [150, 'Water', 'Special', 0],
    'Meteor Mash': [90, 'Steel', 'Physical', 0],
    'Astonish': [30, 'Ghost', 'Physical', 0],
    'Weather Ball': [50, 'Normal', 'Special', 0],
    'Air Cutter': [60, 'Flying', 'Special', 2],
    'Overheat': [130, 'Fire', 'Special', 0],
    'Rock Tomb': [60, 'Rock', 'Physical', 0],
    'Silver Wind': [60, 'Bug', 'Special', 0],
    'Water Spout': [150, 'Water', 'Special', 0],
    'Signal Beam': [75, 'Bug', 'Special', 0],
    'Shadow Punch': [60, 'Ghost', 'Physical', 0],
    'Extrasensory': [80, 'Psychic', 'Special', 0],
    'Sky Uppercut': [85, 'Fighting', 'Physical', 0],
    'Sand Tomb': [35, 'Ground', 'Physical', 0],
    'Sheer Cold': [1, 'Ice', 'Special', 0],
    'Muddy Water': [90, 'Water', 'Special', 0],
    'Bullet Seed': [25, 'Grass', 'Physical', 0],
    'Aerial Ace': [60, 'Flying', 'Physical', 0],
    'Icicle Spear': [25, 'Ice', 'Physical', 0],
    'Dragon Claw': [80, 'Dragon', 'Physical', 0],
    'Frenzy Plant': [150, 'Grass', 'Special', 0],
    'Bounce': [85, 'Flying', 'Physical', 0],
    'Mud Shot': [55, 'Ground', 'Special', 0],
    'Poison Tail': [50, 'Poison', 'Physical', 2],
    'Covet': [60, 'Normal', 'Physical', 0],
    'Volt Tackle': [120, 'Electric', 'Physical', 0],
    'Magical Leaf': [60, 'Grass', 'Special', 0],
    'Leaf Blade': [90, 'Grass', 'Physical', 2],
    'Rock Blast': [25, 'Rock', 'Physical', 0],
    'Shock Wave': [60, 'Electric', 'Special', 0],
    'Water Pulse': [60, 'Water', 'Special', 0],
    'Doom Desire': [140, 'Steel', 'Special', 0],
    'Psycho Boost': [140, 'Psychic', 'Special', 0],
    'Wake-Up Slap': [70, 'Fighting', 'Physical', 0],
    'Hammer Arm': [100, 'Fighting', 'Physical', 0],
    'Gyro Ball': [1, 'Steel', 'Physical', 0],
    'Brine': [65, 'Water', 'Special', 0],
    'Natural Gift': [1, 'Normal', 'Physical', 0],
    'Feint': [50, 'Normal', 'Physical', 0],
    'Pluck': [60, 'Flying', 'Physical', 0],
    'Metal Burst': [1, 'Steel', 'Physical', 0],
    'U-turn': [70, 'Bug', 'Physical', 0],
    'Close Combat': [120, 'Fighting', 'Physical', 0],
    'Payback': [50, 'Dark', 'Physical', 0],
    'Assurance': [60, 'Dark', 'Physical', 0],
    'Fling': [1, 'Dark', 'Physical', 0],
    'Trump Card': [1, 'Normal', 'Special', 0],
    'Wring Out': [1, 'Normal', 'Special', 0],
    'Punishment': [1, 'Dark', 'Physical', 0],
    'Last Resort': [140, 'Normal', 'Physical', 0],
    'Sucker Punch': [80, 'Dark', 'Physical', 0],
    'Flare Blitz': [120, 'Fire', 'Physical', 0],
    'Force Palm': [60, 'Fighting', 'Physical', 0],
    'Aura Sphere': [80, 'Fighting', 'Special', 0],
    'Poison Jab': [80, 'Poison', 'Physical', 0],
    'Dark Pulse': [80, 'Dark', 'Special', 0],
    'Night Slash': [70, 'Dark', 'Physical', 2],
    'Aqua Tail': [90, 'Water', 'Physical', 0],
    'Seed Bomb': [80, 'Grass', 'Physical', 0],
    'Air Slash': [75, 'Flying', 'Special', 0],
    'X-Scissor': [80, 'Bug', 'Physical', 0],
    'Bug Buzz': [90, 'Bug', 'Special', 0],
    'Dragon Pulse': [85, 'Dragon', 'Special', 0],
    'Dragon Rush': [100, 'Dragon', 'Physical', 0],
    'Power Gem': [80, 'Rock', 'Special', 0],
    'Drain Punch': [75, 'Fighting', 'Physical', 0],
    'Vacuum Wave': [40, 'Fighting', 'Special', 0],
    'Focus Blast': [120, 'Fighting', 'Special', 0],
    'Energy Ball': [90, 'Grass', 'Special', 0],
    'Brave Bird': [120, 'Flying', 'Physical', 0],
    'Earth Power': [90, 'Ground', 'Special', 0],
    'Giga Impact': [150, 'Normal', 'Physical', 0],
    'Bullet Punch': [40, 'Steel', 'Physical', 0],
    'Avalanche': [60, 'Ice', 'Physical', 0],
    'Ice Shard': [40, 'Ice', 'Physical', 0],
    'Shadow Claw': [70, 'Ghost', 'Physical', 2],
    'Thunder Fang': [65, 'Electric', 'Physical', 0],
    'Ice Fang': [65, 'Ice', 'Physical', 0],
    'Fire Fang': [65, 'Fire', 'Physical', 0],
    'Shadow Sneak': [40, 'Ghost', 'Physical', 0],
    'Mud Bomb': [65, 'Ground', 'Special', 0],
    'Psycho Cut': [70, 'Psychic', 'Physical', 2],
    'Zen Headbutt': [80, 'Psychic', 'Physical', 0],
    'Mirror Shot': [65, 'Steel', 'Special', 0],
    'Flash Cannon': [80, 'Steel', 'Special', 0],
    'Rock Climb': [90, 'Normal', 'Physical', 0],
    'Draco Meteor': [130, 'Dragon', 'Special', 0],
    'Discharge': [80, 'Electric', 'Special', 0],
    'Lava Plume': [80, 'Fire', 'Special', 0],
    'Leaf Storm': [130, 'Grass', 'Special', 0],
    'Power Whip': [120, 'Grass', 'Physical', 0],
    'Rock Wrecker': [150, 'Rock', 'Physical', 0],
    'Cross Poison': [70, 'Poison', 'Physical', 2],
    'Gunk Shot': [120, 'Poison', 'Physical', 0],
    'Iron Head': [80, 'Steel', 'Physical', 0],
    'Magnet Bomb': [60, 'Steel', 'Physical', 0],
    'Stone Edge': [100, 'Rock', 'Physical', 2],
    'Grass Knot': [1, 'Grass', 'Special', 0],
    'Chatter': [65, 'Flying', 'Special', 0],
    'Judgment': [100, 'Normal', 'Special', 0],
    'Bug Bite': [60, 'Bug', 'Physical', 0],
    'Charge Beam': [50, 'Electric', 'Special', 0],
    'Wood Hammer': [120, 'Grass', 'Physical', 0],
    'Aqua Jet': [40, 'Water', 'Physical', 0],
    'Attack Order': [90, 'Bug', 'Physical', 2],
    'Head Smash': [150, 'Rock', 'Physical', 0],
    'Double Hit': [35, 'Normal', 'Physical', 0],
    'Roar of Time': [150, 'Dragon', 'Special', 0],
    'Spacial Rend': [100, 'Dragon', 'Special', 2],
    'Crush Grip': [1, 'Normal', 'Physical', 0],
    'Magma Storm': [100, 'Fire', 'Special', 0],
    'Seed Flare': [120, 'Grass', 'Special', 0],
    'Ominous Wind': [60, 'Ghost', 'Special', 0],
    'Shadow Force': [120, 'Ghost', 'Physical', 0],
    'Psyshock': [80, 'Psychic', 'Special', 0],
    'Venoshock': [65, 'Poison', 'Special', 0],
    'Smack Down': [50, 'Rock', 'Physical', 0],
    'Storm Throw': [60, 'Fighting', 'Physical', 6],
    'Flame Burst': [70, 'Fire', 'Special', 0],
    'Sludge Wave': [95, 'Poison', 'Special', 0],
    'Heavy Slam': [1, 'Steel', 'Physical', 0],
    'Synchronoise': [120, 'Psychic', 'Special', 0],
    'Electro Ball': [1, 'Electric', 'Special', 0],
    'Flame Charge': [50, 'Fire', 'Physical', 0],
    'Low Sweep': [65, 'Fighting', 'Physical', 0],
    'Acid Spray': [40, 'Poison', 'Special', 0],
    'Foul Play': [95, 'Dark', 'Physical', 0],
    'Round': [60, 'Normal', 'Special', 0],
    'Echoed Voice': [40, 'Normal', 'Special', 0],
    'Chip Away': [70, 'Normal', 'Physical', 0],
    'Clear Smog': [50, 'Poison', 'Special', 0],
    'Stored Power': [20, 'Psychic', 'Special', 0],
    'Scald': [80, 'Water', 'Special', 0],
    'Hex': [65, 'Ghost', 'Special', 0],
    'Sky Drop': [60, 'Flying', 'Physical', 0],
    'Circle Throw': [60, 'Fighting', 'Physical', 0],
    'Incinerate': [60, 'Fire', 'Special', 0],
    'Acrobatics': [55, 'Flying', 'Physical', 0],
    'Retaliate': [70, 'Normal', 'Physical', 0],
    'Final Gambit': [1, 'Fighting', 'Special', 0],
    'Inferno': [100, 'Fire', 'Special', 0],
    'Water Pledge': [80, 'Water', 'Special', 0],
    'Fire Pledge': [80, 'Fire', 'Special', 0],
    'Grass Pledge': [80, 'Grass', 'Special', 0],
    'Volt Switch': [70, 'Electric', 'Special', 0],
    'Struggle Bug': [50, 'Bug', 'Special', 0],
    'Bulldoze': [60, 'Ground', 'Physical', 0],
    'Frost Breath': [60, 'Ice', 'Special', 6],
    'Dragon Tail': [60, 'Dragon', 'Physical', 0],
    'Electroweb': [55, 'Electric', 'Special', 0],
    'Wild Charge': [90, 'Electric', 'Physical', 0],
    'Drill Run': [80, 'Ground', 'Physical', 2],
    'Dual Chop': [40, 'Dragon', 'Physical', 0],
    'Heart Stamp': [60, 'Psychic', 'Physical', 0],
    'Horn Leech': [75, 'Grass', 'Physical', 0],
    'Sacred Sword': [90, 'Fighting', 'Physical', 0],
    'Razor Shell': [75, 'Water', 'Physical', 0],
    'Heat Crash': [1, 'Fire', 'Physical', 0],
    'Leaf Tornado': [65, 'Grass', 'Special', 0],
    'Steamroller': [65, 'Bug', 'Physical', 0],
    'Night Daze': [85, 'Dark', 'Special', 0],
    'Psystrike': [100, 'Psychic', 'Special', 0],
    'Tail Slap': [25, 'Normal', 'Physical', 0],
    'Hurricane': [110, 'Flying', 'Special', 0],
    'Head Charge': [120, 'Normal', 'Physical', 0],
    'Gear Grind': [50, 'Steel', 'Physical', 0],
    'Searing Shot': [100, 'Fire', 'Special', 0],
    'Techno Blast': [120, 'Normal', 'Special', 0],
    'Relic Song': [75, 'Normal', 'Special', 0],
    'Secret Sword': [85, 'Fighting', 'Special', 0],
    'Glaciate': [65, 'Ice', 'Special', 0],
    'Bolt Strike': [130, 'Electric', 'Physical', 0],
    'Blue Flare': [130, 'Fire', 'Special', 0],
    'Fiery Dance': [80, 'Fire', 'Special', 0],
    'Freeze Shock': [140, 'Ice', 'Physical', 0],
    'Ice Burn': [140, 'Ice', 'Special', 0],
    'Snarl': [55, 'Dark', 'Special', 0],
    'Icicle Crash': [85, 'Ice', 'Physical', 0],
    'V-create': [180, 'Fire', 'Physical', 0],
    'Fusion Flare': [100, 'Fire', 'Special', 0],
    'Fusion Bolt': [100, 'Electric', 'Physical', 0],
    'Belch': [120, 'Poison', 'Special', 0],
    'Boomburst': [140, 'Normal', 'Special', 0],
    'Dazzling Gleam': [80, 'Fairy', 'Special', 0],
    'Diamond Storm': [100, 'Rock', 'Physical', 0],
    'Disarming Voice': [40, 'Fairy', 'Special', 0],
    'Draining Kiss': [50, 'Fairy', 'Special', 0],
    'Fairy Wind': [40, 'Fairy', 'Special', 0],
    'Fell Stinger': [25, 'Bug', 'Physical', 0],
    'Flying Press': [80, 'Fighting', 'Physical', 0],
    'Freeze-Dry': [70, 'Ice', 'Special', 0],
    'Hold Back': [40, 'Normal', 'Physical', 0],
    'Infestation': [20, 'Bug', 'Special', 0],
    'Lands Wrath': [90, 'Ground', 'Physical', 0],
    'Nuzzle': [20, 'Electric', 'Physical', 0],
    'Oblivion Wing': [80, 'Flying', 'Special', 0],
    'Parabolic Charge': [50, 'Electric', 'Special', 0],
    'Petal Blizzard': [90, 'Grass', 'Physical', 0],
    'Phantom Force': [90, 'Ghost', 'Physical', 0],
    'Play Rough': [90, 'Fairy', 'Physical', 0],
    'Power-Up Punch': [40, 'Fighting', 'Physical', 0],
    'Water Shuriken': [20, 'Water', 'Physical', 0],

    // Begin NL exclusives
    'Acid Rain': [120, 'Poison', 'Special', 0],
    'Aftershock': [60, 'Ground', 'Physical', 0],
    'Aquos Whip': [70, 'Water', 'Physical', 0],
    'Arclightning': [18, 'Electric', 'Special', 0],
    'Blindside': [120, 'Dark', 'Physical', 0],
    'Boulder Crash': [130, 'Rock', 'Physical', 0],
    'Coal Scatter': [40, 'Fire', 'Physical', 0],
    'Cut': [65, 'Steel', 'Physical', 0],
    'Daring Dash': [80, 'Normal', 'Physical', 0],
    'Draft Swipe': [60, 'Flying', 'Physical', 0],
    'Dragon Aura': [40, 'Dragon', 'Special', 0],
    'Dragon Dive': [120, 'Dragon', 'Physical', 0],
    'Dragon Flare': [50, 'Dragon', 'Special', 0],
    'Drag Under': [60, 'Water', 'Special', 0],
    'Drain Life': [50, 'Bug', 'Physical', 0],
    'Ear Splitter': [130, 'Normal', 'Special', 0],
    'Ectoplasm': [75, 'Ghost', 'Special', 0],
    'Feather Rain': [25, 'Flying', 'Physical', 0],
    'Flash Shot': [70, 'Steel', 'Special', 0],
    'Frenzy Strike': [20, 'Normal', 'Physical', 0],
    'Frostbite': [65, 'Ice', 'Physical', 0],
    'Frozen Blade': [70, 'Ice', 'Physical', 0],
    'Flutter Jump': [50, 'Fairy', 'Physical', 0],
    'Gamble': [40, 'Dark', 'Special', 0],
    'Golden Flame': [75, 'Dragon', 'Special', 0],
    'Hellfire': [65, 'Dark', 'Special', 0],
    'Hydrofire': [75, 'Water', 'Special', 0],
    'Hydro Lash': [60, 'Water', 'Physical', 0],
    'Ice Drill': [80, 'Ice', 'Physical', 0],
    'Imperial Tackle': [80, 'Dragon', 'Physical', 0],
    'Lariat': [60, 'Fighting', 'Physical', 0],
    'Lightning Burst': [80, 'Electric', 'Physical', 0],
    'Luck Blast': [40, 'Normal', 'Special', 0],
    'Luminous Wave': [65, 'Water', 'Special', 0],
    'Maelstrom': [120, 'Water', 'Physical', 0],
    'Mind Crush': [95, 'Psychic', 'Physical', 0],
    'Mirror Spiral': [60, 'Steel', 'Special', 0],
    'Mow Down': [150, 'Steel', 'Physical', 0],
    'Ore Wave': [85, 'Rock', 'Special', 0],
    'Phantasmata': [120, 'Psychic', 'Special', 0],
    'Phoenix Fire': [70, 'Fire', 'Special', 0],
    'Piston Kick': [75, 'Steel', 'Physical', 0],
    'Plague': [30, 'Poison', 'Physical', 0],
    'Prism Shower': [120, 'Ice', 'Special', 0],
    'Radiant Claw': [80, 'Dragon', 'Physical', 0],
    'Ring of Fire': [100, 'Fire', 'Special', 0],
    'Rocket Kick': [30, 'Fire', 'Physical', 0],
    'Rose Spear': [95, 'Grass', 'Physical', 0],
    'Sandblast': [55, 'Ground', 'Physical', 0],
    'Scythe Rip': [80, 'Dark', 'Physical', 0],
    'Shimmer Shot': [40, 'Fairy', 'Special', 0],
    'Slumber Fang': [60, 'Poison', 'Physical', 0],
    'Snow Cannon': [75, 'Ice', 'Special', 0],
    'Super Sting': [55, 'Bug', 'Physical', 0],
    'Terra Force': [70, 'Ground', 'Special', 0],
    'Thorn Slice': [80, 'Grass', 'Physical', 0],
    'Thundercrash': [140, 'Electric', 'Physical', 0],
    'Void Burst': [80, 'Psychic', 'Special', 0],
    'Wave Dance': [120, 'Water', 'Special', 0],
    'Weld': [60, 'Steel', 'Physical', 0],
    'Wild Maul': [120, 'Fighting', 'Physical', 0]
};

var ALL_POKEMON = {
	'bulbasaur': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 6.9,
		'hp': 45,
		'at': 49,
		'df': 49,
		'sa': 65,
		'sd': 65,
		'spd': 45
		},

	'ivysaur': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 13.0,
		'hp': 60,
		'at': 62,
		'df': 63,
		'sa': 80,
		'sd': 80,
		'spd': 60
		},

	'venusaur': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 100.0,
		'hp': 80,
		'at': 82,
		'df': 83,
		'sa': 100,
		'sd': 100,
		'spd': 80
		},

	'charmander': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 8.5,
		'hp': 39,
		'at': 52,
		'df': 43,
		'sa': 60,
		'sd': 50,
		'spd': 65
		},

	'charmeleon': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 19.0,
		'hp': 58,
		'at': 64,
		'df': 58,
		'sa': 80,
		'sd': 65,
		'spd': 80
		},

	'charizard': {
		'type1': 'Flying',
		'type2': 'Fire',
		'weight': 90.5,
		'hp': 78,
		'at': 84,
		'df': 78,
		'sa': 109,
		'sd': 85,
		'spd': 100
		},

	'squirtle': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 9.0,
		'hp': 44,
		'at': 48,
		'df': 65,
		'sa': 50,
		'sd': 64,
		'spd': 43
		},

	'wartortle': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 22.5,
		'hp': 59,
		'at': 63,
		'df': 80,
		'sa': 65,
		'sd': 80,
		'spd': 58
		},

	'blastoise': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 85.5,
		'hp': 79,
		'at': 83,
		'df': 100,
		'sa': 85,
		'sd': 105,
		'spd': 78
		},

	'caterpie': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 2.9,
		'hp': 45,
		'at': 30,
		'df': 35,
		'sa': 20,
		'sd': 20,
		'spd': 45
		},

	'metapod': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 9.9,
		'hp': 50,
		'at': 20,
		'df': 55,
		'sa': 25,
		'sd': 25,
		'spd': 30
		},

	'butterfree': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 32.0,
		'hp': 60,
		'at': 45,
		'df': 50,
		'sa': 90,
		'sd': 80,
		'spd': 70
		},

	'weedle': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 3.2,
		'hp': 40,
		'at': 35,
		'df': 30,
		'sa': 20,
		'sd': 20,
		'spd': 50
		},

	'kakuna': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 10.0,
		'hp': 45,
		'at': 25,
		'df': 50,
		'sa': 25,
		'sd': 25,
		'spd': 35
		},

	'beedrill': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 29.5,
		'hp': 65,
		'at': 90,
		'df': 40,
		'sa': 45,
		'sd': 80,
		'spd': 75
		},

	'pidgey': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 1.8,
		'hp': 40,
		'at': 45,
		'df': 40,
		'sa': 35,
		'sd': 35,
		'spd': 56
		},

	'pidgeotto': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 30.0,
		'hp': 63,
		'at': 60,
		'df': 55,
		'sa': 50,
		'sd': 50,
		'spd': 71
		},

	'pidgeot': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 39.5,
		'hp': 83,
		'at': 80,
		'df': 75,
		'sa': 70,
		'sd': 70,
		'spd': 101
		},

	'rattata': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 3.5,
		'hp': 30,
		'at': 56,
		'df': 35,
		'sa': 25,
		'sd': 35,
		'spd': 72
		},

	'raticate': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 18.5,
		'hp': 55,
		'at': 81,
		'df': 60,
		'sa': 50,
		'sd': 70,
		'spd': 97
		},

	'spearow': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 2.0,
		'hp': 40,
		'at': 60,
		'df': 30,
		'sa': 31,
		'sd': 31,
		'spd': 70
		},

	'fearow': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 38.0,
		'hp': 65,
		'at': 90,
		'df': 65,
		'sa': 61,
		'sd': 61,
		'spd': 100
		},

	'ekans': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 6.9,
		'hp': 35,
		'at': 60,
		'df': 44,
		'sa': 40,
		'sd': 54,
		'spd': 55
		},

	'arbok': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 65.0,
		'hp': 60,
		'at': 85,
		'df': 69,
		'sa': 65,
		'sd': 79,
		'spd': 80
		},

	'pikachu': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 6.0,
		'hp': 35,
		'at': 55,
		'df': 40,
		'sa': 50,
		'sd': 50,
		'spd': 90
		},

	'raichu': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 30.0,
		'hp': 60,
		'at': 90,
		'df': 55,
		'sa': 90,
		'sd': 80,
		'spd': 110
		},

	'sandshrew': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 12.0,
		'hp': 50,
		'at': 75,
		'df': 85,
		'sa': 20,
		'sd': 30,
		'spd': 40
		},

	'sandslash': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 29.5,
		'hp': 75,
		'at': 100,
		'df': 110,
		'sa': 45,
		'sd': 55,
		'spd': 65
		},

	'nidoranf': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 7.0,
		'hp': 55,
		'at': 47,
		'df': 52,
		'sa': 40,
		'sd': 40,
		'spd': 41
		},

	'nidorina': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 20.0,
		'hp': 70,
		'at': 62,
		'df': 67,
		'sa': 55,
		'sd': 55,
		'spd': 56
		},

	'nidoqueen': {
		'type1': 'Poison',
		'type2': 'Ground',
		'weight': 60.0,
		'hp': 90,
		'at': 92,
		'df': 87,
		'sa': 75,
		'sd': 85,
		'spd': 76
		},

	'nidoranf': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 9.0,
		'hp': 46,
		'at': 57,
		'df': 40,
		'sa': 40,
		'sd': 40,
		'spd': 50
		},

	'nidorino': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 19.5,
		'hp': 61,
		'at': 72,
		'df': 57,
		'sa': 55,
		'sd': 55,
		'spd': 65
		},

	'nidoking': {
		'type1': 'Poison',
		'type2': 'Ground',
		'weight': 62.0,
		'hp': 81,
		'at': 102,
		'df': 77,
		'sa': 85,
		'sd': 75,
		'spd': 85
		},

	'clefairy': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 7.5,
		'hp': 70,
		'at': 45,
		'df': 48,
		'sa': 60,
		'sd': 65,
		'spd': 35
		},

	'clefable': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 40.0,
		'hp': 95,
		'at': 70,
		'df': 73,
		'sa': 95,
		'sd': 90,
		'spd': 60
		},

	'vulpix': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 9.9,
		'hp': 38,
		'at': 41,
		'df': 40,
		'sa': 50,
		'sd': 65,
		'spd': 65
		},

	'ninetales': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 19.9,
		'hp': 73,
		'at': 76,
		'df': 75,
		'sa': 81,
		'sd': 100,
		'spd': 100
		},

	'jigglypuff': {
		'type1': 'Normal',
		'type2': 'Fairy',
		'weight': 5.5,
		'hp': 115,
		'at': 45,
		'df': 20,
		'sa': 45,
		'sd': 25,
		'spd': 20
		},

	'wigglytuff': {
		'type1': 'Normal',
		'type2': 'Fairy',
		'weight': 12.0,
		'hp': 140,
		'at': 70,
		'df': 45,
		'sa': 85,
		'sd': 50,
		'spd': 45
		},

	'zubat': {
		'type1': 'Flying',
		'type2': 'Poison',
		'weight': 7.5,
		'hp': 40,
		'at': 45,
		'df': 35,
		'sa': 30,
		'sd': 40,
		'spd': 55
		},

	'golbat': {
		'type1': 'Flying',
		'type2': 'Poison',
		'weight': 55.0,
		'hp': 75,
		'at': 80,
		'df': 70,
		'sa': 65,
		'sd': 75,
		'spd': 90
		},

	'oddish': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 5.4,
		'hp': 45,
		'at': 50,
		'df': 55,
		'sa': 75,
		'sd': 65,
		'spd': 30
		},

	'gloom': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 8.6,
		'hp': 60,
		'at': 65,
		'df': 70,
		'sa': 85,
		'sd': 75,
		'spd': 40
		},

	'vileplume': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 18.6,
		'hp': 75,
		'at': 80,
		'df': 85,
		'sa': 110,
		'sd': 90,
		'spd': 50
		},

	'paras': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 5.4,
		'hp': 35,
		'at': 70,
		'df': 55,
		'sa': 45,
		'sd': 55,
		'spd': 25
		},

	'parasect': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 29.5,
		'hp': 60,
		'at': 95,
		'df': 80,
		'sa': 60,
		'sd': 80,
		'spd': 30
		},

	'venonat': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 30.0,
		'hp': 60,
		'at': 55,
		'df': 50,
		'sa': 40,
		'sd': 55,
		'spd': 45
		},

	'venomoth': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 12.5,
		'hp': 70,
		'at': 65,
		'df': 60,
		'sa': 90,
		'sd': 75,
		'spd': 90
		},

	'diglett': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 0.8,
		'hp': 10,
		'at': 55,
		'df': 25,
		'sa': 35,
		'sd': 45,
		'spd': 95
		},

	'dugtrio': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 33.3,
		'hp': 35,
		'at': 80,
		'df': 50,
		'sa': 50,
		'sd': 70,
		'spd': 120
		},

	'meowth': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 4.2,
		'hp': 40,
		'at': 45,
		'df': 35,
		'sa': 40,
		'sd': 40,
		'spd': 90
		},

	'persian': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 32.0,
		'hp': 65,
		'at': 70,
		'df': 60,
		'sa': 65,
		'sd': 65,
		'spd': 115
		},

	'psyduck': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 19.6,
		'hp': 50,
		'at': 52,
		'df': 48,
		'sa': 65,
		'sd': 50,
		'spd': 55
		},

	'golduck': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 76.6,
		'hp': 80,
		'at': 82,
		'df': 78,
		'sa': 95,
		'sd': 80,
		'spd': 85
		},

	'mankey': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 28.0,
		'hp': 40,
		'at': 80,
		'df': 35,
		'sa': 35,
		'sd': 45,
		'spd': 70
		},

	'primeape': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 32.0,
		'hp': 65,
		'at': 105,
		'df': 60,
		'sa': 60,
		'sd': 70,
		'spd': 95
		},

	'growlithe': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 19.0,
		'hp': 55,
		'at': 70,
		'df': 45,
		'sa': 70,
		'sd': 50,
		'spd': 60
		},

	'arcanine': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 155.0,
		'hp': 90,
		'at': 110,
		'df': 80,
		'sa': 100,
		'sd': 80,
		'spd': 95
		},

	'poliwag': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 12.4,
		'hp': 40,
		'at': 50,
		'df': 40,
		'sa': 40,
		'sd': 40,
		'spd': 90
		},

	'poliwhirl': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 20.0,
		'hp': 65,
		'at': 65,
		'df': 65,
		'sa': 50,
		'sd': 50,
		'spd': 90
		},

	'poliwrath': {
		'type1': 'Fighting',
		'type2': 'Water',
		'weight': 54.0,
		'hp': 90,
		'at': 95,
		'df': 95,
		'sa': 70,
		'sd': 90,
		'spd': 70
		},

	'abra': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 19.5,
		'hp': 25,
		'at': 20,
		'df': 15,
		'sa': 105,
		'sd': 55,
		'spd': 90
		},

	'kadabra': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 56.5,
		'hp': 40,
		'at': 35,
		'df': 30,
		'sa': 120,
		'sd': 70,
		'spd': 105
		},

	'alakazam': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 48.0,
		'hp': 55,
		'at': 50,
		'df': 45,
		'sa': 135,
		'sd': 95,
		'spd': 120
		},

	'machop': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 19.5,
		'hp': 70,
		'at': 80,
		'df': 50,
		'sa': 35,
		'sd': 35,
		'spd': 35
		},

	'machoke': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 70.5,
		'hp': 80,
		'at': 100,
		'df': 70,
		'sa': 50,
		'sd': 60,
		'spd': 45
		},

	'machamp': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 130.0,
		'hp': 90,
		'at': 130,
		'df': 80,
		'sa': 65,
		'sd': 85,
		'spd': 55
		},

	'bellsprout': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 4.0,
		'hp': 50,
		'at': 75,
		'df': 35,
		'sa': 70,
		'sd': 30,
		'spd': 40
		},

	'weepinbell': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 6.4,
		'hp': 65,
		'at': 90,
		'df': 50,
		'sa': 85,
		'sd': 45,
		'spd': 55
		},

	'victreebel': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 15.5,
		'hp': 80,
		'at': 105,
		'df': 65,
		'sa': 100,
		'sd': 70,
		'spd': 70
		},

	'tentacool': {
		'type1': 'Poison',
		'type2': 'Water',
		'weight': 45.5,
		'hp': 40,
		'at': 40,
		'df': 35,
		'sa': 50,
		'sd': 100,
		'spd': 70
		},

	'tentacruel': {
		'type1': 'Poison',
		'type2': 'Water',
		'weight': 55.0,
		'hp': 80,
		'at': 70,
		'df': 65,
		'sa': 80,
		'sd': 120,
		'spd': 100
		},

	'geodude': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 20.0,
		'hp': 40,
		'at': 80,
		'df': 100,
		'sa': 30,
		'sd': 30,
		'spd': 20
		},

	'graveler': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 105.0,
		'hp': 55,
		'at': 95,
		'df': 115,
		'sa': 45,
		'sd': 45,
		'spd': 35
		},

	'golem': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 300.0,
		'hp': 80,
		'at': 120,
		'df': 130,
		'sa': 55,
		'sd': 65,
		'spd': 45
		},

	'ponyta': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 30.0,
		'hp': 50,
		'at': 85,
		'df': 55,
		'sa': 65,
		'sd': 65,
		'spd': 90
		},

	'rapidash': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 95.0,
		'hp': 65,
		'at': 100,
		'df': 70,
		'sa': 80,
		'sd': 80,
		'spd': 105
		},

	'slowpoke': {
		'type1': 'Water',
		'type2': 'Psychic',
		'weight': 36.0,
		'hp': 90,
		'at': 65,
		'df': 65,
		'sa': 40,
		'sd': 40,
		'spd': 15
		},

	'slowbro': {
		'type1': 'Water',
		'type2': 'Psychic',
		'weight': 78.5,
		'hp': 95,
		'at': 75,
		'df': 110,
		'sa': 100,
		'sd': 80,
		'spd': 30
		},

	'magnemite': {
		'type1': 'Steel',
		'type2': 'Electric',
		'weight': 6.0,
		'hp': 25,
		'at': 35,
		'df': 70,
		'sa': 95,
		'sd': 55,
		'spd': 45
		},

	'magneton': {
		'type1': 'Steel',
		'type2': 'Electric',
		'weight': 60.0,
		'hp': 50,
		'at': 60,
		'df': 95,
		'sa': 120,
		'sd': 70,
		'spd': 70
		},

	'farfetchd': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 15.0,
		'hp': 52,
		'at': 65,
		'df': 55,
		'sa': 58,
		'sd': 62,
		'spd': 60
		},

	'doduo': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 39.2,
		'hp': 35,
		'at': 85,
		'df': 45,
		'sa': 35,
		'sd': 35,
		'spd': 75
		},

	'dodrio': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 85.2,
		'hp': 60,
		'at': 110,
		'df': 70,
		'sa': 60,
		'sd': 60,
		'spd': 100
		},

	'seel': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 90.0,
		'hp': 65,
		'at': 45,
		'df': 55,
		'sa': 45,
		'sd': 70,
		'spd': 45
		},

	'dewgong': {
		'type1': 'Water',
		'type2': 'Ice',
		'weight': 120.0,
		'hp': 90,
		'at': 70,
		'df': 80,
		'sa': 70,
		'sd': 95,
		'spd': 70
		},

	'grimer': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 30.0,
		'hp': 80,
		'at': 80,
		'df': 50,
		'sa': 40,
		'sd': 50,
		'spd': 25
		},

	'muk': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 30.0,
		'hp': 105,
		'at': 105,
		'df': 75,
		'sa': 65,
		'sd': 100,
		'spd': 50
		},

	'shellder': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 4.0,
		'hp': 30,
		'at': 65,
		'df': 100,
		'sa': 45,
		'sd': 25,
		'spd': 40
		},

	'cloyster': {
		'type1': 'Water',
		'type2': 'Ice',
		'weight': 132.5,
		'hp': 50,
		'at': 95,
		'df': 180,
		'sa': 85,
		'sd': 45,
		'spd': 70
		},

	'gastly': {
		'type1': 'Poison',
		'type2': 'Ghost',
		'weight': 0.1,
		'hp': 30,
		'at': 35,
		'df': 30,
		'sa': 100,
		'sd': 35,
		'spd': 80
		},

	'haunter': {
		'type1': 'Poison',
		'type2': 'Ghost',
		'weight': 0.1,
		'hp': 45,
		'at': 50,
		'df': 45,
		'sa': 115,
		'sd': 55,
		'spd': 95
		},

	'gengar': {
		'type1': 'Poison',
		'type2': 'Ghost',
		'weight': 40.5,
		'hp': 60,
		'at': 65,
		'df': 60,
		'sa': 130,
		'sd': 75,
		'spd': 110
		},

	'onix': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 210.0,
		'hp': 35,
		'at': 45,
		'df': 160,
		'sa': 30,
		'sd': 45,
		'spd': 70
		},

	'drowzee': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 32.4,
		'hp': 60,
		'at': 48,
		'df': 45,
		'sa': 43,
		'sd': 90,
		'spd': 42
		},

	'hypno': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 75.6,
		'hp': 85,
		'at': 73,
		'df': 70,
		'sa': 73,
		'sd': 115,
		'spd': 67
		},

	'krabby': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 6.5,
		'hp': 30,
		'at': 105,
		'df': 90,
		'sa': 25,
		'sd': 25,
		'spd': 50
		},

	'kingler': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 60.0,
		'hp': 55,
		'at': 130,
		'df': 115,
		'sa': 50,
		'sd': 50,
		'spd': 75
		},

	'voltorb': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 10.4,
		'hp': 40,
		'at': 30,
		'df': 50,
		'sa': 55,
		'sd': 55,
		'spd': 100
		},

	'electrode': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 66.6,
		'hp': 60,
		'at': 50,
		'df': 70,
		'sa': 80,
		'sd': 80,
		'spd': 140
		},

	'exeggcute': {
		'type1': 'Grass',
		'type2': 'Psychic',
		'weight': 2.5,
		'hp': 60,
		'at': 40,
		'df': 80,
		'sa': 60,
		'sd': 45,
		'spd': 40
		},

	'exeggutor': {
		'type1': 'Grass',
		'type2': 'Psychic',
		'weight': 120.0,
		'hp': 95,
		'at': 95,
		'df': 85,
		'sa': 125,
		'sd': 65,
		'spd': 55
		},

	'cubone': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 6.5,
		'hp': 50,
		'at': 50,
		'df': 95,
		'sa': 40,
		'sd': 50,
		'spd': 35
		},

	'marowak': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 45.0,
		'hp': 60,
		'at': 80,
		'df': 110,
		'sa': 50,
		'sd': 80,
		'spd': 45
		},

	'hitmonlee': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 49.8,
		'hp': 50,
		'at': 120,
		'df': 53,
		'sa': 35,
		'sd': 110,
		'spd': 87
		},

	'hitmonchan': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 50.2,
		'hp': 50,
		'at': 105,
		'df': 79,
		'sa': 35,
		'sd': 110,
		'spd': 76
		},

	'lickitung': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 65.5,
		'hp': 90,
		'at': 55,
		'df': 75,
		'sa': 60,
		'sd': 75,
		'spd': 30
		},

	'koffing': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 1.0,
		'hp': 40,
		'at': 65,
		'df': 95,
		'sa': 60,
		'sd': 45,
		'spd': 35
		},

	'weezing': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 9.5,
		'hp': 65,
		'at': 90,
		'df': 120,
		'sa': 85,
		'sd': 70,
		'spd': 60
		},

	'rhyhorn': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 115.0,
		'hp': 80,
		'at': 85,
		'df': 95,
		'sa': 30,
		'sd': 30,
		'spd': 25
		},

	'rhydon': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 120.0,
		'hp': 105,
		'at': 130,
		'df': 120,
		'sa': 45,
		'sd': 45,
		'spd': 40
		},

	'chansey': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 34.6,
		'hp': 250,
		'at': 5,
		'df': 5,
		'sa': 35,
		'sd': 105,
		'spd': 50
		},

	'tangela': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 35.0,
		'hp': 65,
		'at': 55,
		'df': 115,
		'sa': 100,
		'sd': 40,
		'spd': 60
		},

	'kangaskhan': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 80.0,
		'hp': 105,
		'at': 95,
		'df': 80,
		'sa': 40,
		'sd': 80,
		'spd': 90
		},

	'horsea': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 8.0,
		'hp': 30,
		'at': 40,
		'df': 70,
		'sa': 70,
		'sd': 25,
		'spd': 60
		},

	'seadra': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 25.0,
		'hp': 55,
		'at': 65,
		'df': 95,
		'sa': 95,
		'sd': 45,
		'spd': 85
		},

	'goldeen': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 15.0,
		'hp': 45,
		'at': 67,
		'df': 60,
		'sa': 35,
		'sd': 50,
		'spd': 63
		},

	'seaking': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 39.0,
		'hp': 80,
		'at': 92,
		'df': 65,
		'sa': 65,
		'sd': 80,
		'spd': 68
		},

	'staryu': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 34.5,
		'hp': 30,
		'at': 45,
		'df': 55,
		'sa': 70,
		'sd': 55,
		'spd': 85
		},

	'starmie': {
		'type1': 'Water',
		'type2': 'Psychic',
		'weight': 80.0,
		'hp': 60,
		'at': 75,
		'df': 85,
		'sa': 100,
		'sd': 85,
		'spd': 115
		},

	'mr-mime': {
		'type1': 'Psychic',
		'type2': 'Fairy',
		'weight': 54.5,
		'hp': 40,
		'at': 45,
		'df': 65,
		'sa': 100,
		'sd': 120,
		'spd': 90
		},

	'scyther': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 56.0,
		'hp': 70,
		'at': 110,
		'df': 80,
		'sa': 55,
		'sd': 80,
		'spd': 105
		},

	'jynx': {
		'type1': 'Psychic',
		'type2': 'Ice',
		'weight': 40.6,
		'hp': 65,
		'at': 50,
		'df': 35,
		'sa': 115,
		'sd': 95,
		'spd': 95
		},

	'electabuzz': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 30.0,
		'hp': 65,
		'at': 83,
		'df': 57,
		'sa': 95,
		'sd': 85,
		'spd': 105
		},

	'magmar': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 44.5,
		'hp': 65,
		'at': 95,
		'df': 57,
		'sa': 100,
		'sd': 85,
		'spd': 93
		},

	'pinsir': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 55.0,
		'hp': 65,
		'at': 125,
		'df': 100,
		'sa': 55,
		'sd': 70,
		'spd': 85
		},

	'tauros': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 88.4,
		'hp': 75,
		'at': 100,
		'df': 95,
		'sa': 40,
		'sd': 70,
		'spd': 110
		},

	'magikarp': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 10.0,
		'hp': 20,
		'at': 10,
		'df': 55,
		'sa': 15,
		'sd': 20,
		'spd': 80
		},

	'gyarados': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 235.0,
		'hp': 95,
		'at': 125,
		'df': 79,
		'sa': 60,
		'sd': 100,
		'spd': 81
		},

	'lapras': {
		'type1': 'Water',
		'type2': 'Ice',
		'weight': 220.0,
		'hp': 130,
		'at': 85,
		'df': 80,
		'sa': 85,
		'sd': 95,
		'spd': 60
		},

	'ditto': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 4.0,
		'hp': 48,
		'at': 48,
		'df': 48,
		'sa': 48,
		'sd': 48,
		'spd': 48
		},

	'eevee': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 6.5,
		'hp': 55,
		'at': 55,
		'df': 50,
		'sa': 45,
		'sd': 65,
		'spd': 55
		},

	'vaporeon': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 29.0,
		'hp': 130,
		'at': 65,
		'df': 60,
		'sa': 110,
		'sd': 95,
		'spd': 65
		},

	'jolteon': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 24.5,
		'hp': 65,
		'at': 65,
		'df': 60,
		'sa': 110,
		'sd': 95,
		'spd': 130
		},

	'flareon': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 25.0,
		'hp': 65,
		'at': 130,
		'df': 60,
		'sa': 95,
		'sd': 110,
		'spd': 65
		},

	'porygon': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 36.5,
		'hp': 65,
		'at': 60,
		'df': 70,
		'sa': 85,
		'sd': 75,
		'spd': 40
		},

	'omanyte': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 7.5,
		'hp': 35,
		'at': 40,
		'df': 100,
		'sa': 90,
		'sd': 55,
		'spd': 35
		},

	'omastar': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 35.0,
		'hp': 70,
		'at': 60,
		'df': 125,
		'sa': 115,
		'sd': 70,
		'spd': 55
		},

	'kabuto': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 11.5,
		'hp': 30,
		'at': 80,
		'df': 90,
		'sa': 55,
		'sd': 45,
		'spd': 55
		},

	'kabutops': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 40.5,
		'hp': 60,
		'at': 115,
		'df': 105,
		'sa': 65,
		'sd': 70,
		'spd': 80
		},

	'aerodactyl': {
		'type1': 'Flying',
		'type2': 'Rock',
		'weight': 59.0,
		'hp': 80,
		'at': 105,
		'df': 65,
		'sa': 60,
		'sd': 75,
		'spd': 130
		},

	'snorlax': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 460.0,
		'hp': 160,
		'at': 110,
		'df': 65,
		'sa': 65,
		'sd': 110,
		'spd': 30
		},

	'articuno': {
		'type1': 'Flying',
		'type2': 'Ice',
		'weight': 55.4,
		'hp': 90,
		'at': 85,
		'df': 100,
		'sa': 95,
		'sd': 125,
		'spd': 85
		},

	'zapdos': {
		'type1': 'Flying',
		'type2': 'Electric',
		'weight': 52.6,
		'hp': 90,
		'at': 90,
		'df': 85,
		'sa': 125,
		'sd': 90,
		'spd': 100
		},

	'moltres': {
		'type1': 'Flying',
		'type2': 'Fire',
		'weight': 60.0,
		'hp': 90,
		'at': 100,
		'df': 90,
		'sa': 125,
		'sd': 85,
		'spd': 90
		},

	'dratini': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 3.3,
		'hp': 41,
		'at': 64,
		'df': 45,
		'sa': 50,
		'sd': 50,
		'spd': 50
		},

	'dragonair': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 16.5,
		'hp': 61,
		'at': 84,
		'df': 65,
		'sa': 70,
		'sd': 70,
		'spd': 70
		},

	'dragonite': {
		'type1': 'Flying',
		'type2': 'Dragon',
		'weight': 210.0,
		'hp': 91,
		'at': 134,
		'df': 95,
		'sa': 100,
		'sd': 100,
		'spd': 80
		},

	'mewtwo': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 122.0,
		'hp': 106,
		'at': 110,
		'df': 90,
		'sa': 154,
		'sd': 90,
		'spd': 130
		},

	'mew': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 4.0,
		'hp': 100,
		'at': 100,
		'df': 100,
		'sa': 100,
		'sd': 100,
		'spd': 100
		},

	'chikorita': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 6.4,
		'hp': 45,
		'at': 49,
		'df': 65,
		'sa': 49,
		'sd': 65,
		'spd': 45
		},

	'bayleef': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 15.8,
		'hp': 60,
		'at': 62,
		'df': 80,
		'sa': 63,
		'sd': 80,
		'spd': 60
		},

	'meganium': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 100.5,
		'hp': 80,
		'at': 82,
		'df': 100,
		'sa': 83,
		'sd': 100,
		'spd': 80
		},

	'cyndaquil': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 7.9,
		'hp': 39,
		'at': 52,
		'df': 43,
		'sa': 60,
		'sd': 50,
		'spd': 65
		},

	'quilava': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 19.0,
		'hp': 58,
		'at': 64,
		'df': 58,
		'sa': 80,
		'sd': 65,
		'spd': 80
		},

	'typhlosion': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 79.5,
		'hp': 78,
		'at': 84,
		'df': 78,
		'sa': 109,
		'sd': 85,
		'spd': 100
		},

	'totodile': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 9.5,
		'hp': 50,
		'at': 65,
		'df': 64,
		'sa': 44,
		'sd': 48,
		'spd': 43
		},

	'croconaw': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 25.0,
		'hp': 65,
		'at': 80,
		'df': 80,
		'sa': 59,
		'sd': 63,
		'spd': 58
		},

	'feraligatr': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 88.8,
		'hp': 85,
		'at': 105,
		'df': 100,
		'sa': 79,
		'sd': 83,
		'spd': 78
		},

	'sentret': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 6.0,
		'hp': 35,
		'at': 46,
		'df': 34,
		'sa': 35,
		'sd': 45,
		'spd': 20
		},

	'furret': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 32.5,
		'hp': 85,
		'at': 76,
		'df': 64,
		'sa': 45,
		'sd': 55,
		'spd': 90
		},

	'hoothoot': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 21.2,
		'hp': 60,
		'at': 30,
		'df': 30,
		'sa': 36,
		'sd': 56,
		'spd': 50
		},

	'noctowl': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 40.8,
		'hp': 100,
		'at': 50,
		'df': 50,
		'sa': 76,
		'sd': 96,
		'spd': 70
		},

	'ledyba': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 10.8,
		'hp': 40,
		'at': 20,
		'df': 30,
		'sa': 40,
		'sd': 80,
		'spd': 55
		},

	'ledian': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 35.6,
		'hp': 55,
		'at': 35,
		'df': 50,
		'sa': 55,
		'sd': 110,
		'spd': 85
		},

	'spinarak': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 8.5,
		'hp': 40,
		'at': 60,
		'df': 40,
		'sa': 40,
		'sd': 40,
		'spd': 30
		},

	'ariados': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 33.5,
		'hp': 70,
		'at': 90,
		'df': 70,
		'sa': 60,
		'sd': 60,
		'spd': 40
		},

	'crobat': {
		'type1': 'Flying',
		'type2': 'Poison',
		'weight': 75.0,
		'hp': 85,
		'at': 90,
		'df': 80,
		'sa': 70,
		'sd': 80,
		'spd': 130
		},

	'chinchou': {
		'type1': 'Water',
		'type2': 'Electric',
		'weight': 12.0,
		'hp': 75,
		'at': 38,
		'df': 38,
		'sa': 56,
		'sd': 56,
		'spd': 67
		},

	'lanturn': {
		'type1': 'Water',
		'type2': 'Electric',
		'weight': 22.5,
		'hp': 125,
		'at': 58,
		'df': 58,
		'sa': 76,
		'sd': 76,
		'spd': 67
		},

	'pichu': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 2.0,
		'hp': 20,
		'at': 40,
		'df': 15,
		'sa': 35,
		'sd': 35,
		'spd': 60
		},

	'cleffa': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 3.0,
		'hp': 50,
		'at': 25,
		'df': 28,
		'sa': 45,
		'sd': 55,
		'spd': 15
		},

	'igglybuff': {
		'type1': 'Normal',
		'type2': 'Fairy',
		'weight': 1.0,
		'hp': 90,
		'at': 30,
		'df': 15,
		'sa': 40,
		'sd': 20,
		'spd': 15
		},

	'togepi': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 1.5,
		'hp': 35,
		'at': 20,
		'df': 65,
		'sa': 40,
		'sd': 65,
		'spd': 20
		},

	'togetic': {
		'type1': 'Fairy',
		'type2': 'Flying',
		'weight': 3.2,
		'hp': 55,
		'at': 40,
		'df': 85,
		'sa': 80,
		'sd': 105,
		'spd': 40
		},

	'natu': {
		'type1': 'Flying',
		'type2': 'Psychic',
		'weight': 2.0,
		'hp': 40,
		'at': 50,
		'df': 45,
		'sa': 70,
		'sd': 45,
		'spd': 70
		},

	'xatu': {
		'type1': 'Flying',
		'type2': 'Psychic',
		'weight': 15.0,
		'hp': 65,
		'at': 75,
		'df': 70,
		'sa': 95,
		'sd': 70,
		'spd': 95
		},

	'mareep': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 7.8,
		'hp': 55,
		'at': 40,
		'df': 40,
		'sa': 65,
		'sd': 45,
		'spd': 35
		},

	'flaaffy': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 13.3,
		'hp': 70,
		'at': 55,
		'df': 55,
		'sa': 80,
		'sd': 60,
		'spd': 45
		},

	'ampharos': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 61.5,
		'hp': 90,
		'at': 75,
		'df': 85,
		'sa': 115,
		'sd': 90,
		'spd': 55
		},

	'bellossom': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 5.8,
		'hp': 75,
		'at': 80,
		'df': 95,
		'sa': 90,
		'sd': 100,
		'spd': 50
		},

	'marill': {
		'type1': 'Water',
		'type2': 'Fairy',
		'weight': 8.5,
		'hp': 70,
		'at': 20,
		'df': 50,
		'sa': 20,
		'sd': 50,
		'spd': 40
		},

	'azumarill': {
		'type1': 'Water',
		'type2': 'Fairy',
		'weight': 28.5,
		'hp': 100,
		'at': 50,
		'df': 80,
		'sa': 60,
		'sd': 80,
		'spd': 50
		},

	'sudowoodo': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 38.0,
		'hp': 70,
		'at': 100,
		'df': 115,
		'sa': 30,
		'sd': 65,
		'spd': 30
		},

	'politoed': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 33.9,
		'hp': 90,
		'at': 75,
		'df': 75,
		'sa': 90,
		'sd': 100,
		'spd': 70
		},

	'hoppip': {
		'type1': 'Flying',
		'type2': 'Grass',
		'weight': 0.5,
		'hp': 35,
		'at': 35,
		'df': 40,
		'sa': 35,
		'sd': 55,
		'spd': 50
		},

	'skiploom': {
		'type1': 'Flying',
		'type2': 'Grass',
		'weight': 1.0,
		'hp': 55,
		'at': 45,
		'df': 50,
		'sa': 45,
		'sd': 65,
		'spd': 80
		},

	'jumpluff': {
		'type1': 'Flying',
		'type2': 'Grass',
		'weight': 3.0,
		'hp': 75,
		'at': 55,
		'df': 70,
		'sa': 55,
		'sd': 95,
		'spd': 110
		},

	'aipom': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 11.5,
		'hp': 55,
		'at': 70,
		'df': 55,
		'sa': 40,
		'sd': 55,
		'spd': 85
		},

	'sunkern': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 1.8,
		'hp': 30,
		'at': 30,
		'df': 30,
		'sa': 30,
		'sd': 30,
		'spd': 30
		},

	'sunflora': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 8.5,
		'hp': 75,
		'at': 75,
		'df': 55,
		'sa': 105,
		'sd': 85,
		'spd': 30
		},

	'yanma': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 38.0,
		'hp': 65,
		'at': 65,
		'df': 45,
		'sa': 75,
		'sd': 45,
		'spd': 95
		},

	'wooper': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 8.5,
		'hp': 55,
		'at': 45,
		'df': 45,
		'sa': 25,
		'sd': 25,
		'spd': 15
		},

	'quagsire': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 75.0,
		'hp': 95,
		'at': 85,
		'df': 85,
		'sa': 65,
		'sd': 65,
		'spd': 35
		},

	'espeon': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 26.5,
		'hp': 65,
		'at': 65,
		'df': 60,
		'sa': 130,
		'sd': 95,
		'spd': 110
		},

	'umbreon': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 27.0,
		'hp': 95,
		'at': 65,
		'df': 110,
		'sa': 60,
		'sd': 130,
		'spd': 65
		},

	'murkrow': {
		'type1': 'Flying',
		'type2': 'Dark',
		'weight': 2.1,
		'hp': 60,
		'at': 85,
		'df': 42,
		'sa': 85,
		'sd': 42,
		'spd': 91
		},

	'slowking': {
		'type1': 'Water',
		'type2': 'Psychic',
		'weight': 79.5,
		'hp': 95,
		'at': 75,
		'df': 80,
		'sa': 100,
		'sd': 110,
		'spd': 30
		},

	'misdreavus': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 1.0,
		'hp': 60,
		'at': 60,
		'df': 60,
		'sa': 85,
		'sd': 85,
		'spd': 85
		},

	'unown': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 5.0,
		'hp': 48,
		'at': 72,
		'df': 48,
		'sa': 72,
		'sd': 48,
		'spd': 48
		},

	'wobbuffet': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 28.5,
		'hp': 190,
		'at': 33,
		'df': 58,
		'sa': 33,
		'sd': 58,
		'spd': 33
		},

	'girafarig': {
		'type1': 'Normal',
		'type2': 'Psychic',
		'weight': 41.5,
		'hp': 70,
		'at': 80,
		'df': 65,
		'sa': 90,
		'sd': 65,
		'spd': 85
		},

	'pineco': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 7.2,
		'hp': 50,
		'at': 65,
		'df': 90,
		'sa': 35,
		'sd': 35,
		'spd': 15
		},

	'forretress': {
		'type1': 'Bug',
		'type2': 'Steel',
		'weight': 125.8,
		'hp': 75,
		'at': 90,
		'df': 140,
		'sa': 60,
		'sd': 60,
		'spd': 40
		},

	'dunsparce': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 14.0,
		'hp': 100,
		'at': 70,
		'df': 70,
		'sa': 65,
		'sd': 65,
		'spd': 45
		},

	'gligar': {
		'type1': 'Flying',
		'type2': 'Ground',
		'weight': 64.8,
		'hp': 65,
		'at': 75,
		'df': 105,
		'sa': 35,
		'sd': 65,
		'spd': 85
		},

	'steelix': {
		'type1': 'Ground',
		'type2': 'Steel',
		'weight': 400.0,
		'hp': 75,
		'at': 85,
		'df': 200,
		'sa': 55,
		'sd': 65,
		'spd': 30
		},

	'snubbull': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 7.8,
		'hp': 60,
		'at': 80,
		'df': 50,
		'sa': 40,
		'sd': 40,
		'spd': 30
		},

	'granbull': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 48.7,
		'hp': 90,
		'at': 120,
		'df': 75,
		'sa': 60,
		'sd': 60,
		'spd': 45
		},

	'qwilfish': {
		'type1': 'Poison',
		'type2': 'Water',
		'weight': 3.9,
		'hp': 65,
		'at': 95,
		'df': 75,
		'sa': 55,
		'sd': 55,
		'spd': 85
		},

	'scizor': {
		'type1': 'Bug',
		'type2': 'Steel',
		'weight': 118.0,
		'hp': 70,
		'at': 130,
		'df': 100,
		'sa': 55,
		'sd': 80,
		'spd': 65
		},

	'shuckle': {
		'type1': 'Rock',
		'type2': 'Bug',
		'weight': 20.5,
		'hp': 20,
		'at': 10,
		'df': 230,
		'sa': 10,
		'sd': 230,
		'spd': 5
		},

	'heracross': {
		'type1': 'Fighting',
		'type2': 'Bug',
		'weight': 54.0,
		'hp': 80,
		'at': 125,
		'df': 75,
		'sa': 40,
		'sd': 95,
		'spd': 85
		},

	'sneasel': {
		'type1': 'Ice',
		'type2': 'Dark',
		'weight': 28.0,
		'hp': 55,
		'at': 95,
		'df': 55,
		'sa': 35,
		'sd': 75,
		'spd': 115
		},

	'teddiursa': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 8.8,
		'hp': 60,
		'at': 80,
		'df': 50,
		'sa': 50,
		'sd': 50,
		'spd': 40
		},

	'ursaring': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 125.8,
		'hp': 90,
		'at': 130,
		'df': 75,
		'sa': 75,
		'sd': 75,
		'spd': 55
		},

	'slugma': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 35.0,
		'hp': 40,
		'at': 40,
		'df': 40,
		'sa': 70,
		'sd': 40,
		'spd': 20
		},

	'magcargo': {
		'type1': 'Rock',
		'type2': 'Fire',
		'weight': 55.0,
		'hp': 50,
		'at': 50,
		'df': 120,
		'sa': 80,
		'sd': 80,
		'spd': 30
		},

	'swinub': {
		'type1': 'Ground',
		'type2': 'Ice',
		'weight': 6.5,
		'hp': 50,
		'at': 50,
		'df': 40,
		'sa': 30,
		'sd': 30,
		'spd': 50
		},

	'piloswine': {
		'type1': 'Ground',
		'type2': 'Ice',
		'weight': 55.8,
		'hp': 100,
		'at': 100,
		'df': 80,
		'sa': 60,
		'sd': 60,
		'spd': 50
		},

	'corsola': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 5.0,
		'hp': 55,
		'at': 55,
		'df': 85,
		'sa': 65,
		'sd': 85,
		'spd': 35
		},

	'remoraid': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 12.0,
		'hp': 35,
		'at': 65,
		'df': 35,
		'sa': 65,
		'sd': 35,
		'spd': 65
		},

	'octillery': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 28.5,
		'hp': 75,
		'at': 105,
		'df': 75,
		'sa': 105,
		'sd': 75,
		'spd': 45
		},

	'delibird': {
		'type1': 'Flying',
		'type2': 'Ice',
		'weight': 16.0,
		'hp': 45,
		'at': 55,
		'df': 45,
		'sa': 65,
		'sd': 45,
		'spd': 75
		},

	'mantine': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 220.0,
		'hp': 65,
		'at': 40,
		'df': 70,
		'sa': 80,
		'sd': 140,
		'spd': 70
		},

	'skarmory': {
		'type1': 'Flying',
		'type2': 'Steel',
		'weight': 50.5,
		'hp': 65,
		'at': 80,
		'df': 140,
		'sa': 40,
		'sd': 70,
		'spd': 70
		},

	'houndour': {
		'type1': 'Fire',
		'type2': 'Dark',
		'weight': 10.8,
		'hp': 45,
		'at': 60,
		'df': 30,
		'sa': 80,
		'sd': 50,
		'spd': 65
		},

	'houndoom': {
		'type1': 'Fire',
		'type2': 'Dark',
		'weight': 35.0,
		'hp': 75,
		'at': 90,
		'df': 50,
		'sa': 110,
		'sd': 80,
		'spd': 95
		},

	'kingdra': {
		'type1': 'Water',
		'type2': 'Dragon',
		'weight': 152.0,
		'hp': 75,
		'at': 95,
		'df': 95,
		'sa': 95,
		'sd': 95,
		'spd': 85
		},

	'phanpy': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 33.5,
		'hp': 90,
		'at': 60,
		'df': 60,
		'sa': 40,
		'sd': 40,
		'spd': 40
		},

	'donphan': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 120.0,
		'hp': 90,
		'at': 120,
		'df': 120,
		'sa': 60,
		'sd': 60,
		'spd': 50
		},

	'porygon2': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 32.5,
		'hp': 85,
		'at': 80,
		'df': 90,
		'sa': 105,
		'sd': 95,
		'spd': 60
		},

	'stantler': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 71.2,
		'hp': 73,
		'at': 95,
		'df': 62,
		'sa': 85,
		'sd': 65,
		'spd': 85
		},

	'smeargle': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 58.0,
		'hp': 55,
		'at': 20,
		'df': 35,
		'sa': 20,
		'sd': 45,
		'spd': 75
		},

	'tyrogue': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 21.0,
		'hp': 35,
		'at': 35,
		'df': 35,
		'sa': 35,
		'sd': 35,
		'spd': 35
		},

	'hitmontop': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 48.0,
		'hp': 50,
		'at': 95,
		'df': 95,
		'sa': 35,
		'sd': 110,
		'spd': 70
		},

	'smoochum': {
		'type1': 'Psychic',
		'type2': 'Ice',
		'weight': 6.0,
		'hp': 45,
		'at': 30,
		'df': 15,
		'sa': 85,
		'sd': 65,
		'spd': 65
		},

	'elekid': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 23.5,
		'hp': 45,
		'at': 63,
		'df': 37,
		'sa': 65,
		'sd': 55,
		'spd': 95
		},

	'magby': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 21.4,
		'hp': 45,
		'at': 75,
		'df': 37,
		'sa': 70,
		'sd': 55,
		'spd': 83
		},

	'miltank': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 75.5,
		'hp': 95,
		'at': 80,
		'df': 105,
		'sa': 40,
		'sd': 70,
		'spd': 100
		},

	'blissey': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 46.8,
		'hp': 255,
		'at': 10,
		'df': 10,
		'sa': 75,
		'sd': 135,
		'spd': 55
		},

	'raikou': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 178.0,
		'hp': 90,
		'at': 85,
		'df': 75,
		'sa': 115,
		'sd': 100,
		'spd': 115
		},

	'entei': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 198.0,
		'hp': 115,
		'at': 115,
		'df': 85,
		'sa': 90,
		'sd': 75,
		'spd': 100
		},

	'suicune': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 187.0,
		'hp': 100,
		'at': 75,
		'df': 115,
		'sa': 90,
		'sd': 115,
		'spd': 85
		},

	'larvitar': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 72.0,
		'hp': 50,
		'at': 64,
		'df': 50,
		'sa': 45,
		'sd': 50,
		'spd': 41
		},

	'pupitar': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 152.0,
		'hp': 70,
		'at': 84,
		'df': 70,
		'sa': 65,
		'sd': 70,
		'spd': 51
		},

	'tyranitar': {
		'type1': 'Rock',
		'type2': 'Dark',
		'weight': 202.0,
		'hp': 100,
		'at': 134,
		'df': 110,
		'sa': 95,
		'sd': 100,
		'spd': 61
		},

	'lugia': {
		'type1': 'Flying',
		'type2': 'Psychic',
		'weight': 216.0,
		'hp': 106,
		'at': 90,
		'df': 130,
		'sa': 90,
		'sd': 154,
		'spd': 110
		},

	'ho-oh': {
		'type1': 'Flying',
		'type2': 'Fire',
		'weight': 199.0,
		'hp': 106,
		'at': 130,
		'df': 90,
		'sa': 110,
		'sd': 154,
		'spd': 90
		},

	'celebi': {
		'type1': 'Grass',
		'type2': 'Psychic',
		'weight': 5.0,
		'hp': 100,
		'at': 100,
		'df': 100,
		'sa': 100,
		'sd': 100,
		'spd': 100
		},

	'treecko': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 5.0,
		'hp': 40,
		'at': 45,
		'df': 35,
		'sa': 65,
		'sd': 55,
		'spd': 70
		},

	'grovyle': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 21.6,
		'hp': 50,
		'at': 65,
		'df': 45,
		'sa': 85,
		'sd': 65,
		'spd': 95
		},

	'sceptile': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 52.2,
		'hp': 70,
		'at': 85,
		'df': 65,
		'sa': 105,
		'sd': 85,
		'spd': 120
		},

	'torchic': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 2.5,
		'hp': 45,
		'at': 60,
		'df': 40,
		'sa': 70,
		'sd': 50,
		'spd': 45
		},

	'combusken': {
		'type1': 'Fighting',
		'type2': 'Fire',
		'weight': 19.5,
		'hp': 60,
		'at': 85,
		'df': 60,
		'sa': 85,
		'sd': 60,
		'spd': 55
		},

	'blaziken': {
		'type1': 'Fighting',
		'type2': 'Fire',
		'weight': 52.0,
		'hp': 80,
		'at': 120,
		'df': 70,
		'sa': 110,
		'sd': 70,
		'spd': 80
		},

	'mudkip': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 7.6,
		'hp': 50,
		'at': 70,
		'df': 50,
		'sa': 50,
		'sd': 50,
		'spd': 40
		},

	'marshtomp': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 28.0,
		'hp': 70,
		'at': 85,
		'df': 70,
		'sa': 60,
		'sd': 70,
		'spd': 50
		},

	'swampert': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 81.9,
		'hp': 100,
		'at': 110,
		'df': 90,
		'sa': 85,
		'sd': 90,
		'spd': 60
		},

	'poochyena': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 13.6,
		'hp': 35,
		'at': 55,
		'df': 35,
		'sa': 30,
		'sd': 30,
		'spd': 35
		},

	'mightyena': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 37.0,
		'hp': 70,
		'at': 90,
		'df': 70,
		'sa': 60,
		'sd': 60,
		'spd': 70
		},

	'zigzagoon': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 17.5,
		'hp': 38,
		'at': 30,
		'df': 41,
		'sa': 30,
		'sd': 41,
		'spd': 60
		},

	'linoone': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 32.5,
		'hp': 78,
		'at': 70,
		'df': 61,
		'sa': 50,
		'sd': 61,
		'spd': 100
		},

	'wurmple': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 3.6,
		'hp': 45,
		'at': 45,
		'df': 35,
		'sa': 20,
		'sd': 30,
		'spd': 20
		},

	'silcoon': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 10.0,
		'hp': 50,
		'at': 35,
		'df': 55,
		'sa': 25,
		'sd': 25,
		'spd': 15
		},

	'beautifly': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 28.4,
		'hp': 60,
		'at': 70,
		'df': 50,
		'sa': 100,
		'sd': 50,
		'spd': 65
		},

	'cascoon': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 11.5,
		'hp': 50,
		'at': 35,
		'df': 55,
		'sa': 25,
		'sd': 25,
		'spd': 15
		},

	'dustox': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 31.6,
		'hp': 60,
		'at': 50,
		'df': 70,
		'sa': 50,
		'sd': 90,
		'spd': 65
		},

	'lotad': {
		'type1': 'Water',
		'type2': 'Grass',
		'weight': 2.6,
		'hp': 40,
		'at': 30,
		'df': 30,
		'sa': 40,
		'sd': 50,
		'spd': 30
		},

	'lombre': {
		'type1': 'Water',
		'type2': 'Grass',
		'weight': 32.5,
		'hp': 60,
		'at': 50,
		'df': 50,
		'sa': 60,
		'sd': 70,
		'spd': 50
		},

	'ludicolo': {
		'type1': 'Water',
		'type2': 'Grass',
		'weight': 55.0,
		'hp': 80,
		'at': 70,
		'df': 70,
		'sa': 90,
		'sd': 100,
		'spd': 70
		},

	'seedot': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 4.0,
		'hp': 40,
		'at': 40,
		'df': 50,
		'sa': 30,
		'sd': 30,
		'spd': 30
		},

	'nuzleaf': {
		'type1': 'Grass',
		'type2': 'Dark',
		'weight': 28.0,
		'hp': 70,
		'at': 70,
		'df': 40,
		'sa': 60,
		'sd': 40,
		'spd': 60
		},

	'shiftry': {
		'type1': 'Grass',
		'type2': 'Dark',
		'weight': 59.6,
		'hp': 90,
		'at': 100,
		'df': 60,
		'sa': 90,
		'sd': 60,
		'spd': 80
		},

	'taillow': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 2.3,
		'hp': 40,
		'at': 55,
		'df': 30,
		'sa': 30,
		'sd': 30,
		'spd': 85
		},

	'swellow': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 19.8,
		'hp': 60,
		'at': 85,
		'df': 60,
		'sa': 50,
		'sd': 50,
		'spd': 125
		},

	'wingull': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 9.5,
		'hp': 40,
		'at': 30,
		'df': 30,
		'sa': 55,
		'sd': 30,
		'spd': 85
		},

	'pelipper': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 28.0,
		'hp': 60,
		'at': 50,
		'df': 100,
		'sa': 85,
		'sd': 70,
		'spd': 65
		},

	'ralts': {
		'type1': 'Psychic',
		'type2': 'Fairy',
		'weight': 6.6,
		'hp': 28,
		'at': 25,
		'df': 25,
		'sa': 45,
		'sd': 35,
		'spd': 40
		},

	'kirlia': {
		'type1': 'Psychic',
		'type2': 'Fairy',
		'weight': 20.2,
		'hp': 38,
		'at': 35,
		'df': 35,
		'sa': 65,
		'sd': 55,
		'spd': 50
		},

	'gardevoir': {
		'type1': 'Psychic',
		'type2': 'Fairy',
		'weight': 48.4,
		'hp': 68,
		'at': 65,
		'df': 65,
		'sa': 125,
		'sd': 115,
		'spd': 80
		},

	'surskit': {
		'type1': 'Bug',
		'type2': 'Water',
		'weight': 1.7,
		'hp': 40,
		'at': 30,
		'df': 32,
		'sa': 50,
		'sd': 52,
		'spd': 65
		},

	'masquerain': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 3.6,
		'hp': 70,
		'at': 60,
		'df': 62,
		'sa': 80,
		'sd': 82,
		'spd': 60
		},

	'shroomish': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 4.5,
		'hp': 60,
		'at': 40,
		'df': 60,
		'sa': 40,
		'sd': 60,
		'spd': 35
		},

	'breloom': {
		'type1': 'Fighting',
		'type2': 'Grass',
		'weight': 39.2,
		'hp': 60,
		'at': 130,
		'df': 80,
		'sa': 60,
		'sd': 60,
		'spd': 70
		},

	'slakoth': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 24.0,
		'hp': 60,
		'at': 60,
		'df': 60,
		'sa': 35,
		'sd': 35,
		'spd': 30
		},

	'vigoroth': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 46.5,
		'hp': 80,
		'at': 80,
		'df': 80,
		'sa': 55,
		'sd': 55,
		'spd': 90
		},

	'slaking': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 130.5,
		'hp': 150,
		'at': 160,
		'df': 100,
		'sa': 95,
		'sd': 65,
		'spd': 100
		},

	'nincada': {
		'type1': 'Ground',
		'type2': 'Bug',
		'weight': 5.5,
		'hp': 31,
		'at': 45,
		'df': 90,
		'sa': 30,
		'sd': 30,
		'spd': 40
		},

	'ninjask': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 12.0,
		'hp': 61,
		'at': 90,
		'df': 45,
		'sa': 50,
		'sd': 50,
		'spd': 160
		},

	'shedinja': {
		'type1': 'Bug',
		'type2': 'Ghost',
		'weight': 1.2,
		'hp': 1,
		'at': 90,
		'df': 45,
		'sa': 30,
		'sd': 30,
		'spd': 40
		},

	'whismur': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 16.3,
		'hp': 64,
		'at': 51,
		'df': 23,
		'sa': 51,
		'sd': 23,
		'spd': 28
		},

	'loudred': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 40.5,
		'hp': 84,
		'at': 71,
		'df': 43,
		'sa': 71,
		'sd': 43,
		'spd': 48
		},

	'exploud': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 84.0,
		'hp': 104,
		'at': 91,
		'df': 63,
		'sa': 91,
		'sd': 73,
		'spd': 68
		},

	'makuhita': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 86.4,
		'hp': 72,
		'at': 60,
		'df': 30,
		'sa': 20,
		'sd': 30,
		'spd': 25
		},

	'hariyama': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 253.8,
		'hp': 144,
		'at': 120,
		'df': 60,
		'sa': 40,
		'sd': 60,
		'spd': 50
		},

	'azurill': {
		'type1': 'Normal',
		'type2': 'Fairy',
		'weight': 2.0,
		'hp': 50,
		'at': 20,
		'df': 40,
		'sa': 20,
		'sd': 40,
		'spd': 20
		},

	'nosepass': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 97.0,
		'hp': 30,
		'at': 45,
		'df': 135,
		'sa': 45,
		'sd': 90,
		'spd': 30
		},

	'skitty': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 11.0,
		'hp': 50,
		'at': 45,
		'df': 45,
		'sa': 35,
		'sd': 35,
		'spd': 50
		},

	'delcatty': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 32.6,
		'hp': 70,
		'at': 65,
		'df': 65,
		'sa': 55,
		'sd': 55,
		'spd': 70
		},

	'sableye': {
		'type1': 'Ghost',
		'type2': 'Dark',
		'weight': 11.0,
		'hp': 50,
		'at': 75,
		'df': 75,
		'sa': 65,
		'sd': 65,
		'spd': 50
		},

	'mawile': {
		'type1': 'Steel',
		'type2': 'Fairy',
		'weight': 11.5,
		'hp': 50,
		'at': 85,
		'df': 85,
		'sa': 55,
		'sd': 55,
		'spd': 50
		},

	'aron': {
		'type1': 'Rock',
		'type2': 'Steel',
		'weight': 60.0,
		'hp': 50,
		'at': 70,
		'df': 100,
		'sa': 40,
		'sd': 40,
		'spd': 30
		},

	'lairon': {
		'type1': 'Rock',
		'type2': 'Steel',
		'weight': 120.0,
		'hp': 60,
		'at': 90,
		'df': 140,
		'sa': 50,
		'sd': 50,
		'spd': 40
		},

	'aggron': {
		'type1': 'Rock',
		'type2': 'Steel',
		'weight': 360.0,
		'hp': 70,
		'at': 110,
		'df': 180,
		'sa': 60,
		'sd': 60,
		'spd': 50
		},

	'meditite': {
		'type1': 'Fighting',
		'type2': 'Psychic',
		'weight': 11.2,
		'hp': 30,
		'at': 40,
		'df': 55,
		'sa': 40,
		'sd': 55,
		'spd': 60
		},

	'medicham': {
		'type1': 'Fighting',
		'type2': 'Psychic',
		'weight': 31.5,
		'hp': 60,
		'at': 60,
		'df': 75,
		'sa': 60,
		'sd': 75,
		'spd': 80
		},

	'electrike': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 15.2,
		'hp': 40,
		'at': 45,
		'df': 40,
		'sa': 65,
		'sd': 40,
		'spd': 65
		},

	'manectric': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 40.2,
		'hp': 70,
		'at': 75,
		'df': 60,
		'sa': 105,
		'sd': 60,
		'spd': 105
		},

	'plusle': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 4.2,
		'hp': 60,
		'at': 50,
		'df': 40,
		'sa': 85,
		'sd': 75,
		'spd': 95
		},

	'minun': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 4.2,
		'hp': 60,
		'at': 40,
		'df': 50,
		'sa': 75,
		'sd': 85,
		'spd': 95
		},

	'volbeat': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 17.7,
		'hp': 65,
		'at': 73,
		'df': 55,
		'sa': 47,
		'sd': 75,
		'spd': 85
		},

	'illumise': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 17.7,
		'hp': 65,
		'at': 47,
		'df': 55,
		'sa': 73,
		'sd': 75,
		'spd': 85
		},

	'roselia': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 2.0,
		'hp': 50,
		'at': 60,
		'df': 45,
		'sa': 100,
		'sd': 80,
		'spd': 65
		},

	'gulpin': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 10.3,
		'hp': 70,
		'at': 43,
		'df': 53,
		'sa': 43,
		'sd': 53,
		'spd': 40
		},

	'swalot': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 80.0,
		'hp': 100,
		'at': 73,
		'df': 83,
		'sa': 73,
		'sd': 83,
		'spd': 55
		},

	'carvanha': {
		'type1': 'Water',
		'type2': 'Dark',
		'weight': 20.8,
		'hp': 45,
		'at': 90,
		'df': 20,
		'sa': 65,
		'sd': 20,
		'spd': 65
		},

	'sharpedo': {
		'type1': 'Water',
		'type2': 'Dark',
		'weight': 88.8,
		'hp': 70,
		'at': 120,
		'df': 40,
		'sa': 95,
		'sd': 40,
		'spd': 95
		},

	'wailmer': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 130.0,
		'hp': 130,
		'at': 70,
		'df': 35,
		'sa': 70,
		'sd': 35,
		'spd': 60
		},

	'wailord': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 398.0,
		'hp': 170,
		'at': 90,
		'df': 45,
		'sa': 90,
		'sd': 45,
		'spd': 60
		},

	'numel': {
		'type1': 'Ground',
		'type2': 'Fire',
		'weight': 24.0,
		'hp': 60,
		'at': 60,
		'df': 40,
		'sa': 65,
		'sd': 45,
		'spd': 35
		},

	'camerupt': {
		'type1': 'Ground',
		'type2': 'Fire',
		'weight': 220.0,
		'hp': 70,
		'at': 100,
		'df': 70,
		'sa': 105,
		'sd': 75,
		'spd': 40
		},

	'torkoal': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 80.4,
		'hp': 70,
		'at': 85,
		'df': 140,
		'sa': 85,
		'sd': 70,
		'spd': 20
		},

	'spoink': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 30.6,
		'hp': 60,
		'at': 25,
		'df': 35,
		'sa': 70,
		'sd': 80,
		'spd': 60
		},

	'grumpig': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 71.5,
		'hp': 80,
		'at': 45,
		'df': 65,
		'sa': 90,
		'sd': 110,
		'spd': 80
		},

	'spinda': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 5.0,
		'hp': 60,
		'at': 60,
		'df': 60,
		'sa': 60,
		'sd': 60,
		'spd': 60
		},

	'trapinch': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 15.0,
		'hp': 45,
		'at': 100,
		'df': 45,
		'sa': 45,
		'sd': 45,
		'spd': 10
		},

	'vibrava': {
		'type1': 'Ground',
		'type2': 'Dragon',
		'weight': 15.3,
		'hp': 50,
		'at': 70,
		'df': 50,
		'sa': 50,
		'sd': 50,
		'spd': 70
		},

	'flygon': {
		'type1': 'Ground',
		'type2': 'Dragon',
		'weight': 82.0,
		'hp': 80,
		'at': 100,
		'df': 80,
		'sa': 80,
		'sd': 80,
		'spd': 100
		},

	'cacnea': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 51.3,
		'hp': 50,
		'at': 85,
		'df': 40,
		'sa': 85,
		'sd': 40,
		'spd': 35
		},

	'cacturne': {
		'type1': 'Grass',
		'type2': 'Dark',
		'weight': 77.4,
		'hp': 70,
		'at': 115,
		'df': 60,
		'sa': 115,
		'sd': 60,
		'spd': 55
		},

	'swablu': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 1.2,
		'hp': 45,
		'at': 40,
		'df': 60,
		'sa': 40,
		'sd': 75,
		'spd': 50
		},

	'altaria': {
		'type1': 'Flying',
		'type2': 'Dragon',
		'weight': 20.6,
		'hp': 75,
		'at': 70,
		'df': 90,
		'sa': 70,
		'sd': 105,
		'spd': 80
		},

	'zangoose': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 40.3,
		'hp': 73,
		'at': 115,
		'df': 60,
		'sa': 60,
		'sd': 60,
		'spd': 90
		},

	'seviper': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 52.5,
		'hp': 73,
		'at': 100,
		'df': 60,
		'sa': 100,
		'sd': 60,
		'spd': 65
		},

	'lunatone': {
		'type1': 'Rock',
		'type2': 'Psychic',
		'weight': 168.0,
		'hp': 70,
		'at': 55,
		'df': 65,
		'sa': 95,
		'sd': 85,
		'spd': 70
		},

	'solrock': {
		'type1': 'Rock',
		'type2': 'Psychic',
		'weight': 154.0,
		'hp': 70,
		'at': 95,
		'df': 85,
		'sa': 55,
		'sd': 65,
		'spd': 70
		},

	'barboach': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 1.9,
		'hp': 50,
		'at': 48,
		'df': 43,
		'sa': 46,
		'sd': 41,
		'spd': 60
		},

	'whiscash': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 23.6,
		'hp': 110,
		'at': 78,
		'df': 73,
		'sa': 76,
		'sd': 71,
		'spd': 60
		},

	'corphish': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 11.5,
		'hp': 43,
		'at': 80,
		'df': 65,
		'sa': 50,
		'sd': 35,
		'spd': 35
		},

	'crawdaunt': {
		'type1': 'Water',
		'type2': 'Dark',
		'weight': 32.8,
		'hp': 63,
		'at': 120,
		'df': 85,
		'sa': 90,
		'sd': 55,
		'spd': 55
		},

	'baltoy': {
		'type1': 'Ground',
		'type2': 'Psychic',
		'weight': 21.5,
		'hp': 40,
		'at': 40,
		'df': 55,
		'sa': 40,
		'sd': 70,
		'spd': 55
		},

	'claydol': {
		'type1': 'Ground',
		'type2': 'Psychic',
		'weight': 108.0,
		'hp': 60,
		'at': 70,
		'df': 105,
		'sa': 70,
		'sd': 120,
		'spd': 75
		},

	'lileep': {
		'type1': 'Rock',
		'type2': 'Grass',
		'weight': 23.8,
		'hp': 66,
		'at': 41,
		'df': 77,
		'sa': 61,
		'sd': 87,
		'spd': 23
		},

	'cradily': {
		'type1': 'Rock',
		'type2': 'Grass',
		'weight': 60.4,
		'hp': 86,
		'at': 81,
		'df': 97,
		'sa': 81,
		'sd': 107,
		'spd': 43
		},

	'anorith': {
		'type1': 'Rock',
		'type2': 'Bug',
		'weight': 12.5,
		'hp': 45,
		'at': 95,
		'df': 50,
		'sa': 40,
		'sd': 50,
		'spd': 75
		},

	'armaldo': {
		'type1': 'Rock',
		'type2': 'Bug',
		'weight': 68.2,
		'hp': 75,
		'at': 125,
		'df': 100,
		'sa': 70,
		'sd': 80,
		'spd': 45
		},

	'feebas': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 7.4,
		'hp': 20,
		'at': 15,
		'df': 20,
		'sa': 10,
		'sd': 55,
		'spd': 80
		},

	'milotic': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 162.0,
		'hp': 95,
		'at': 60,
		'df': 79,
		'sa': 100,
		'sd': 125,
		'spd': 81
		},

	'castform': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 0.8,
		'hp': 70,
		'at': 70,
		'df': 70,
		'sa': 70,
		'sd': 70,
		'spd': 70
		},

	'kecleon': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 22.0,
		'hp': 60,
		'at': 90,
		'df': 70,
		'sa': 60,
		'sd': 120,
		'spd': 40
		},

	'shuppet': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 2.3,
		'hp': 44,
		'at': 75,
		'df': 35,
		'sa': 63,
		'sd': 33,
		'spd': 45
		},

	'banette': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 12.5,
		'hp': 64,
		'at': 115,
		'df': 65,
		'sa': 83,
		'sd': 63,
		'spd': 65
		},

	'duskull': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 15.0,
		'hp': 20,
		'at': 40,
		'df': 90,
		'sa': 30,
		'sd': 90,
		'spd': 25
		},

	'dusclops': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 30.6,
		'hp': 40,
		'at': 70,
		'df': 130,
		'sa': 60,
		'sd': 130,
		'spd': 25
		},

	'tropius': {
		'type1': 'Flying',
		'type2': 'Grass',
		'weight': 100.0,
		'hp': 99,
		'at': 68,
		'df': 83,
		'sa': 72,
		'sd': 87,
		'spd': 51
		},

	'chimecho': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 1.0,
		'hp': 65,
		'at': 50,
		'df': 70,
		'sa': 95,
		'sd': 80,
		'spd': 65
		},

	'absol': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 47.0,
		'hp': 65,
		'at': 130,
		'df': 60,
		'sa': 75,
		'sd': 60,
		'spd': 75
		},

	'wynaut': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 14.0,
		'hp': 95,
		'at': 23,
		'df': 48,
		'sa': 23,
		'sd': 48,
		'spd': 23
		},

	'snorunt': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 16.8,
		'hp': 50,
		'at': 50,
		'df': 50,
		'sa': 50,
		'sd': 50,
		'spd': 50
		},

	'glalie': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 256.5,
		'hp': 80,
		'at': 80,
		'df': 80,
		'sa': 80,
		'sd': 80,
		'spd': 80
		},

	'spheal': {
		'type1': 'Water',
		'type2': 'Ice',
		'weight': 39.5,
		'hp': 70,
		'at': 40,
		'df': 50,
		'sa': 55,
		'sd': 50,
		'spd': 25
		},

	'sealeo': {
		'type1': 'Water',
		'type2': 'Ice',
		'weight': 87.6,
		'hp': 90,
		'at': 60,
		'df': 70,
		'sa': 75,
		'sd': 70,
		'spd': 45
		},

	'walrein': {
		'type1': 'Water',
		'type2': 'Ice',
		'weight': 150.6,
		'hp': 110,
		'at': 80,
		'df': 90,
		'sa': 95,
		'sd': 90,
		'spd': 65
		},

	'clamperl': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 52.5,
		'hp': 35,
		'at': 64,
		'df': 85,
		'sa': 74,
		'sd': 55,
		'spd': 32
		},

	'huntail': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 27.0,
		'hp': 55,
		'at': 104,
		'df': 105,
		'sa': 94,
		'sd': 75,
		'spd': 52
		},

	'gorebyss': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 22.6,
		'hp': 55,
		'at': 84,
		'df': 105,
		'sa': 114,
		'sd': 75,
		'spd': 52
		},

	'relicanth': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 23.4,
		'hp': 100,
		'at': 90,
		'df': 130,
		'sa': 45,
		'sd': 65,
		'spd': 55
		},

	'luvdisc': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 8.7,
		'hp': 43,
		'at': 30,
		'df': 55,
		'sa': 40,
		'sd': 65,
		'spd': 97
		},

	'bagon': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 42.1,
		'hp': 45,
		'at': 75,
		'df': 60,
		'sa': 40,
		'sd': 30,
		'spd': 50
		},

	'shelgon': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 110.5,
		'hp': 65,
		'at': 95,
		'df': 100,
		'sa': 60,
		'sd': 50,
		'spd': 50
		},

	'salamence': {
		'type1': 'Flying',
		'type2': 'Dragon',
		'weight': 102.6,
		'hp': 95,
		'at': 135,
		'df': 80,
		'sa': 110,
		'sd': 80,
		'spd': 100
		},

	'beldum': {
		'type1': 'Steel',
		'type2': 'Psychic',
		'weight': 95.2,
		'hp': 40,
		'at': 55,
		'df': 80,
		'sa': 35,
		'sd': 60,
		'spd': 30
		},

	'metang': {
		'type1': 'Steel',
		'type2': 'Psychic',
		'weight': 202.5,
		'hp': 60,
		'at': 75,
		'df': 100,
		'sa': 55,
		'sd': 80,
		'spd': 50
		},

	'metagross': {
		'type1': 'Steel',
		'type2': 'Psychic',
		'weight': 550.0,
		'hp': 80,
		'at': 135,
		'df': 130,
		'sa': 95,
		'sd': 90,
		'spd': 70
		},

	'regirock': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 230.0,
		'hp': 80,
		'at': 100,
		'df': 200,
		'sa': 50,
		'sd': 100,
		'spd': 50
		},

	'regice': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 175.0,
		'hp': 80,
		'at': 50,
		'df': 100,
		'sa': 100,
		'sd': 200,
		'spd': 50
		},

	'registeel': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 205.0,
		'hp': 80,
		'at': 75,
		'df': 150,
		'sa': 75,
		'sd': 150,
		'spd': 50
		},

	'latias': {
		'type1': 'Psychic',
		'type2': 'Dragon',
		'weight': 40.0,
		'hp': 80,
		'at': 80,
		'df': 90,
		'sa': 110,
		'sd': 130,
		'spd': 110
		},

	'latios': {
		'type1': 'Psychic',
		'type2': 'Dragon',
		'weight': 60.0,
		'hp': 80,
		'at': 90,
		'df': 80,
		'sa': 130,
		'sd': 110,
		'spd': 110
		},

	'kyogre': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 352.0,
		'hp': 100,
		'at': 100,
		'df': 90,
		'sa': 150,
		'sd': 140,
		'spd': 90
		},

	'groudon': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 950.0,
		'hp': 100,
		'at': 150,
		'df': 140,
		'sa': 100,
		'sd': 90,
		'spd': 90
		},

	'rayquaza': {
		'type1': 'Flying',
		'type2': 'Dragon',
		'weight': 206.5,
		'hp': 105,
		'at': 150,
		'df': 90,
		'sa': 150,
		'sd': 90,
		'spd': 95
		},

	'jirachi': {
		'type1': 'Steel',
		'type2': 'Psychic',
		'weight': 1.1,
		'hp': 100,
		'at': 100,
		'df': 100,
		'sa': 100,
		'sd': 100,
		'spd': 100
		},

	'deoxys': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 60.8,
		'hp': 50,
		'at': 150,
		'df': 50,
		'sa': 150,
		'sd': 50,
		'spd': 150
		},

	'turtwig': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 10.2,
		'hp': 55,
		'at': 68,
		'df': 64,
		'sa': 45,
		'sd': 55,
		'spd': 31
		},

	'grotle': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 97.0,
		'hp': 75,
		'at': 89,
		'df': 85,
		'sa': 55,
		'sd': 65,
		'spd': 36
		},

	'torterra': {
		'type1': 'Ground',
		'type2': 'Grass',
		'weight': 310.0,
		'hp': 95,
		'at': 109,
		'df': 105,
		'sa': 75,
		'sd': 85,
		'spd': 56
		},

	'chimchar': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 6.2,
		'hp': 44,
		'at': 58,
		'df': 44,
		'sa': 58,
		'sd': 44,
		'spd': 61
		},

	'monferno': {
		'type1': 'Fighting',
		'type2': 'Fire',
		'weight': 22.0,
		'hp': 64,
		'at': 78,
		'df': 52,
		'sa': 78,
		'sd': 52,
		'spd': 81
		},

	'infernape': {
		'type1': 'Fighting',
		'type2': 'Fire',
		'weight': 55.0,
		'hp': 76,
		'at': 104,
		'df': 71,
		'sa': 104,
		'sd': 71,
		'spd': 108
		},

	'piplup': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 5.2,
		'hp': 53,
		'at': 51,
		'df': 53,
		'sa': 61,
		'sd': 56,
		'spd': 40
		},

	'prinplup': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 23.0,
		'hp': 64,
		'at': 66,
		'df': 68,
		'sa': 81,
		'sd': 76,
		'spd': 50
		},

	'empoleon': {
		'type1': 'Steel',
		'type2': 'Water',
		'weight': 84.5,
		'hp': 84,
		'at': 86,
		'df': 88,
		'sa': 111,
		'sd': 101,
		'spd': 60
		},

	'starly': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 2.0,
		'hp': 40,
		'at': 55,
		'df': 30,
		'sa': 30,
		'sd': 30,
		'spd': 60
		},

	'staravia': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 15.5,
		'hp': 55,
		'at': 75,
		'df': 50,
		'sa': 40,
		'sd': 40,
		'spd': 80
		},

	'staraptor': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 24.9,
		'hp': 85,
		'at': 120,
		'df': 70,
		'sa': 50,
		'sd': 60,
		'spd': 100
		},

	'bidoof': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 20.0,
		'hp': 59,
		'at': 45,
		'df': 40,
		'sa': 35,
		'sd': 40,
		'spd': 31
		},

	'bibarel': {
		'type1': 'Normal',
		'type2': 'Water',
		'weight': 31.5,
		'hp': 79,
		'at': 85,
		'df': 60,
		'sa': 55,
		'sd': 60,
		'spd': 71
		},

	'kricketot': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 2.2,
		'hp': 37,
		'at': 25,
		'df': 41,
		'sa': 25,
		'sd': 41,
		'spd': 25
		},

	'kricketune': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 25.5,
		'hp': 77,
		'at': 85,
		'df': 51,
		'sa': 55,
		'sd': 51,
		'spd': 65
		},

	'shinx': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 9.5,
		'hp': 45,
		'at': 65,
		'df': 34,
		'sa': 40,
		'sd': 34,
		'spd': 45
		},

	'luxio': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 30.5,
		'hp': 60,
		'at': 85,
		'df': 49,
		'sa': 60,
		'sd': 49,
		'spd': 60
		},

	'luxray': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 42.0,
		'hp': 80,
		'at': 120,
		'df': 79,
		'sa': 95,
		'sd': 79,
		'spd': 70
		},

	'budew': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 1.2,
		'hp': 40,
		'at': 30,
		'df': 35,
		'sa': 50,
		'sd': 70,
		'spd': 55
		},

	'roserade': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 14.5,
		'hp': 60,
		'at': 70,
		'df': 65,
		'sa': 125,
		'sd': 105,
		'spd': 90
		},

	'cranidos': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 31.5,
		'hp': 67,
		'at': 125,
		'df': 40,
		'sa': 30,
		'sd': 30,
		'spd': 58
		},

	'rampardos': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 102.5,
		'hp': 97,
		'at': 165,
		'df': 60,
		'sa': 65,
		'sd': 50,
		'spd': 58
		},

	'shieldon': {
		'type1': 'Rock',
		'type2': 'Steel',
		'weight': 57.0,
		'hp': 30,
		'at': 42,
		'df': 118,
		'sa': 42,
		'sd': 88,
		'spd': 30
		},

	'bastiodon': {
		'type1': 'Rock',
		'type2': 'Steel',
		'weight': 149.5,
		'hp': 60,
		'at': 52,
		'df': 168,
		'sa': 47,
		'sd': 138,
		'spd': 30
		},

	'burmy': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 3.4,
		'hp': 40,
		'at': 29,
		'df': 45,
		'sa': 29,
		'sd': 45,
		'spd': 36
		},

	'wormadam': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 6.5,
		'hp': 60,
		'at': 59,
		'df': 85,
		'sa': 79,
		'sd': 105,
		'spd': 36
		},

	'mothim': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 23.3,
		'hp': 70,
		'at': 94,
		'df': 50,
		'sa': 94,
		'sd': 50,
		'spd': 66
		},

	'combee': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 5.5,
		'hp': 30,
		'at': 30,
		'df': 42,
		'sa': 30,
		'sd': 42,
		'spd': 70
		},

	'vespiquen': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 38.5,
		'hp': 70,
		'at': 80,
		'df': 102,
		'sa': 80,
		'sd': 102,
		'spd': 40
		},

	'pachirisu': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 3.9,
		'hp': 60,
		'at': 45,
		'df': 70,
		'sa': 45,
		'sd': 90,
		'spd': 95
		},

	'buizel': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 29.5,
		'hp': 55,
		'at': 65,
		'df': 35,
		'sa': 60,
		'sd': 30,
		'spd': 85
		},

	'floatzel': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 33.5,
		'hp': 85,
		'at': 105,
		'df': 55,
		'sa': 85,
		'sd': 50,
		'spd': 115
		},

	'cherubi': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 3.3,
		'hp': 45,
		'at': 35,
		'df': 45,
		'sa': 62,
		'sd': 53,
		'spd': 35
		},

	'cherrim': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 9.3,
		'hp': 70,
		'at': 60,
		'df': 70,
		'sa': 87,
		'sd': 78,
		'spd': 85
		},

	'shellos': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 6.3,
		'hp': 76,
		'at': 48,
		'df': 48,
		'sa': 57,
		'sd': 62,
		'spd': 34
		},

	'gastrodon': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 29.9,
		'hp': 111,
		'at': 83,
		'df': 68,
		'sa': 92,
		'sd': 82,
		'spd': 39
		},

	'ambipom': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 20.3,
		'hp': 75,
		'at': 100,
		'df': 66,
		'sa': 60,
		'sd': 66,
		'spd': 115
		},

	'drifloon': {
		'type1': 'Flying',
		'type2': 'Ghost',
		'weight': 1.2,
		'hp': 90,
		'at': 50,
		'df': 34,
		'sa': 60,
		'sd': 44,
		'spd': 70
		},

	'drifblim': {
		'type1': 'Flying',
		'type2': 'Ghost',
		'weight': 15.0,
		'hp': 150,
		'at': 80,
		'df': 44,
		'sa': 90,
		'sd': 54,
		'spd': 80
		},

	'buneary': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 5.5,
		'hp': 55,
		'at': 66,
		'df': 44,
		'sa': 44,
		'sd': 56,
		'spd': 85
		},

	'lopunny': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 33.3,
		'hp': 65,
		'at': 76,
		'df': 84,
		'sa': 54,
		'sd': 96,
		'spd': 105
		},

	'mismagius': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 4.4,
		'hp': 60,
		'at': 60,
		'df': 60,
		'sa': 105,
		'sd': 105,
		'spd': 105
		},

	'honchkrow': {
		'type1': 'Flying',
		'type2': 'Dark',
		'weight': 27.3,
		'hp': 100,
		'at': 125,
		'df': 52,
		'sa': 105,
		'sd': 52,
		'spd': 71
		},

	'glameow': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 3.9,
		'hp': 49,
		'at': 55,
		'df': 42,
		'sa': 42,
		'sd': 37,
		'spd': 85
		},

	'purugly': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 43.8,
		'hp': 71,
		'at': 82,
		'df': 64,
		'sa': 64,
		'sd': 59,
		'spd': 112
		},

	'chingling': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 0.6,
		'hp': 45,
		'at': 30,
		'df': 50,
		'sa': 65,
		'sd': 50,
		'spd': 45
		},

	'stunky': {
		'type1': 'Poison',
		'type2': 'Dark',
		'weight': 19.2,
		'hp': 63,
		'at': 63,
		'df': 47,
		'sa': 41,
		'sd': 41,
		'spd': 74
		},

	'skuntank': {
		'type1': 'Poison',
		'type2': 'Dark',
		'weight': 38.0,
		'hp': 103,
		'at': 93,
		'df': 67,
		'sa': 71,
		'sd': 61,
		'spd': 84
		},

	'bronzor': {
		'type1': 'Steel',
		'type2': 'Psychic',
		'weight': 60.5,
		'hp': 57,
		'at': 24,
		'df': 86,
		'sa': 24,
		'sd': 86,
		'spd': 23
		},

	'bronzong': {
		'type1': 'Steel',
		'type2': 'Psychic',
		'weight': 187.0,
		'hp': 67,
		'at': 89,
		'df': 116,
		'sa': 79,
		'sd': 116,
		'spd': 33
		},

	'bonsly': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 15.0,
		'hp': 50,
		'at': 80,
		'df': 95,
		'sa': 10,
		'sd': 45,
		'spd': 10
		},

	'mime-jr': {
		'type1': 'Psychic',
		'type2': 'Fairy',
		'weight': 13.0,
		'hp': 20,
		'at': 25,
		'df': 45,
		'sa': 70,
		'sd': 90,
		'spd': 60
		},

	'happiny': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 24.4,
		'hp': 100,
		'at': 5,
		'df': 5,
		'sa': 15,
		'sd': 65,
		'spd': 30
		},

	'chatot': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 1.9,
		'hp': 76,
		'at': 65,
		'df': 45,
		'sa': 92,
		'sd': 42,
		'spd': 91
		},

	'spiritomb': {
		'type1': 'Ghost',
		'type2': 'Dark',
		'weight': 108.0,
		'hp': 50,
		'at': 92,
		'df': 108,
		'sa': 92,
		'sd': 108,
		'spd': 35
		},

	'gible': {
		'type1': 'Ground',
		'type2': 'Dragon',
		'weight': 20.5,
		'hp': 58,
		'at': 70,
		'df': 45,
		'sa': 40,
		'sd': 45,
		'spd': 42
		},

	'gabite': {
		'type1': 'Ground',
		'type2': 'Dragon',
		'weight': 56.0,
		'hp': 68,
		'at': 90,
		'df': 65,
		'sa': 50,
		'sd': 55,
		'spd': 82
		},

	'garchomp': {
		'type1': 'Ground',
		'type2': 'Dragon',
		'weight': 95.0,
		'hp': 108,
		'at': 130,
		'df': 95,
		'sa': 80,
		'sd': 85,
		'spd': 102
		},

	'munchlax': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 105.0,
		'hp': 135,
		'at': 85,
		'df': 40,
		'sa': 40,
		'sd': 85,
		'spd': 5
		},

	'riolu': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 20.2,
		'hp': 40,
		'at': 70,
		'df': 40,
		'sa': 35,
		'sd': 40,
		'spd': 60
		},

	'lucario': {
		'type1': 'Fighting',
		'type2': 'Steel',
		'weight': 54.0,
		'hp': 70,
		'at': 110,
		'df': 70,
		'sa': 115,
		'sd': 70,
		'spd': 90
		},

	'hippopotas': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 49.5,
		'hp': 68,
		'at': 72,
		'df': 78,
		'sa': 38,
		'sd': 42,
		'spd': 32
		},

	'hippowdon': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 300.0,
		'hp': 108,
		'at': 112,
		'df': 118,
		'sa': 68,
		'sd': 72,
		'spd': 47
		},

	'skorupi': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 12.0,
		'hp': 40,
		'at': 50,
		'df': 90,
		'sa': 30,
		'sd': 55,
		'spd': 65
		},

	'drapion': {
		'type1': 'Poison',
		'type2': 'Dark',
		'weight': 61.5,
		'hp': 70,
		'at': 90,
		'df': 110,
		'sa': 60,
		'sd': 75,
		'spd': 95
		},

	'croagunk': {
		'type1': 'Fighting',
		'type2': 'Poison',
		'weight': 23.0,
		'hp': 48,
		'at': 61,
		'df': 40,
		'sa': 61,
		'sd': 40,
		'spd': 50
		},

	'toxicroak': {
		'type1': 'Fighting',
		'type2': 'Poison',
		'weight': 44.4,
		'hp': 83,
		'at': 106,
		'df': 65,
		'sa': 86,
		'sd': 65,
		'spd': 85
		},

	'carnivine': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 27.0,
		'hp': 74,
		'at': 100,
		'df': 72,
		'sa': 90,
		'sd': 72,
		'spd': 46
		},

	'finneon': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 7.0,
		'hp': 49,
		'at': 49,
		'df': 56,
		'sa': 49,
		'sd': 61,
		'spd': 66
		},

	'lumineon': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 24.0,
		'hp': 69,
		'at': 69,
		'df': 76,
		'sa': 69,
		'sd': 86,
		'spd': 91
		},

	'mantyke': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 65.0,
		'hp': 45,
		'at': 20,
		'df': 50,
		'sa': 60,
		'sd': 120,
		'spd': 50
		},

	'snover': {
		'type1': 'Grass',
		'type2': 'Ice',
		'weight': 50.5,
		'hp': 60,
		'at': 62,
		'df': 50,
		'sa': 62,
		'sd': 60,
		'spd': 40
		},

	'abomasnow': {
		'type1': 'Grass',
		'type2': 'Ice',
		'weight': 135.5,
		'hp': 90,
		'at': 92,
		'df': 75,
		'sa': 92,
		'sd': 85,
		'spd': 60
		},

	'weavile': {
		'type1': 'Ice',
		'type2': 'Dark',
		'weight': 34.0,
		'hp': 70,
		'at': 120,
		'df': 65,
		'sa': 45,
		'sd': 85,
		'spd': 125
		},

	'magnezone': {
		'type1': 'Steel',
		'type2': 'Electric',
		'weight': 180.0,
		'hp': 70,
		'at': 70,
		'df': 115,
		'sa': 130,
		'sd': 90,
		'spd': 60
		},

	'lickilicky': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 140.0,
		'hp': 110,
		'at': 85,
		'df': 95,
		'sa': 80,
		'sd': 95,
		'spd': 50
		},

	'rhyperior': {
		'type1': 'Ground',
		'type2': 'Rock',
		'weight': 282.8,
		'hp': 115,
		'at': 140,
		'df': 130,
		'sa': 55,
		'sd': 55,
		'spd': 40
		},

	'tangrowth': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 128.6,
		'hp': 100,
		'at': 100,
		'df': 125,
		'sa': 110,
		'sd': 50,
		'spd': 50
		},

	'electivire': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 138.6,
		'hp': 75,
		'at': 123,
		'df': 67,
		'sa': 95,
		'sd': 85,
		'spd': 95
		},

	'magmortar': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 68.0,
		'hp': 75,
		'at': 95,
		'df': 67,
		'sa': 125,
		'sd': 95,
		'spd': 83
		},

	'togekiss': {
		'type1': 'Fairy',
		'type2': 'Flying',
		'weight': 38.0,
		'hp': 85,
		'at': 50,
		'df': 95,
		'sa': 120,
		'sd': 115,
		'spd': 80
		},

	'yanmega': {
		'type1': 'Flying',
		'type2': 'Bug',
		'weight': 51.5,
		'hp': 86,
		'at': 76,
		'df': 86,
		'sa': 116,
		'sd': 56,
		'spd': 95
		},

	'leafeon': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 25.5,
		'hp': 65,
		'at': 110,
		'df': 130,
		'sa': 60,
		'sd': 65,
		'spd': 95
		},

	'glaceon': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 25.9,
		'hp': 65,
		'at': 60,
		'df': 110,
		'sa': 130,
		'sd': 95,
		'spd': 65
		},

	'gliscor': {
		'type1': 'Flying',
		'type2': 'Ground',
		'weight': 42.5,
		'hp': 75,
		'at': 95,
		'df': 125,
		'sa': 45,
		'sd': 75,
		'spd': 95
		},

	'mamoswine': {
		'type1': 'Ground',
		'type2': 'Ice',
		'weight': 291.0,
		'hp': 110,
		'at': 130,
		'df': 80,
		'sa': 70,
		'sd': 60,
		'spd': 80
		},

	'porygon-z': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 34.0,
		'hp': 85,
		'at': 80,
		'df': 70,
		'sa': 135,
		'sd': 75,
		'spd': 90
		},

	'gallade': {
		'type1': 'Fighting',
		'type2': 'Psychic',
		'weight': 52.0,
		'hp': 68,
		'at': 125,
		'df': 65,
		'sa': 65,
		'sd': 115,
		'spd': 80
		},

	'probopass': {
		'type1': 'Rock',
		'type2': 'Steel',
		'weight': 340.0,
		'hp': 60,
		'at': 55,
		'df': 145,
		'sa': 75,
		'sd': 150,
		'spd': 40
		},

	'dusknoir': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 106.6,
		'hp': 45,
		'at': 100,
		'df': 135,
		'sa': 65,
		'sd': 135,
		'spd': 45
		},

	'froslass': {
		'type1': 'Ghost',
		'type2': 'Ice',
		'weight': 26.6,
		'hp': 70,
		'at': 80,
		'df': 70,
		'sa': 80,
		'sd': 70,
		'spd': 110
		},

	'rotom': {
		'type1': 'Ghost',
		'type2': 'Electric',
		'weight': 0.3,
		'hp': 50,
		'at': 50,
		'df': 77,
		'sa': 95,
		'sd': 77,
		'spd': 91
		},

	'uxie': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 0.3,
		'hp': 75,
		'at': 75,
		'df': 130,
		'sa': 75,
		'sd': 130,
		'spd': 95
		},

	'mesprit': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 0.3,
		'hp': 80,
		'at': 105,
		'df': 105,
		'sa': 105,
		'sd': 105,
		'spd': 80
		},

	'azelf': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 0.3,
		'hp': 75,
		'at': 125,
		'df': 70,
		'sa': 125,
		'sd': 70,
		'spd': 115
		},

	'dialga': {
		'type1': 'Steel',
		'type2': 'Dragon',
		'weight': 683.0,
		'hp': 100,
		'at': 120,
		'df': 120,
		'sa': 150,
		'sd': 100,
		'spd': 90
		},

	'palkia': {
		'type1': 'Water',
		'type2': 'Dragon',
		'weight': 336.0,
		'hp': 90,
		'at': 120,
		'df': 100,
		'sa': 150,
		'sd': 120,
		'spd': 100
		},

	'heatran': {
		'type1': 'Steel',
		'type2': 'Fire',
		'weight': 430.0,
		'hp': 91,
		'at': 90,
		'df': 106,
		'sa': 130,
		'sd': 106,
		'spd': 77
		},

	'regigigas': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 420.0,
		'hp': 110,
		'at': 160,
		'df': 110,
		'sa': 80,
		'sd': 110,
		'spd': 100
		},

	'giratina': {
		'type1': 'Ghost',
		'type2': 'Dragon',
		'weight': 750.0,
		'hp': 150,
		'at': 100,
		'df': 120,
		'sa': 100,
		'sd': 120,
		'spd': 90
		},

	'cresselia': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 85.6,
		'hp': 120,
		'at': 70,
		'df': 120,
		'sa': 75,
		'sd': 130,
		'spd': 85
		},

	'phione': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 3.1,
		'hp': 80,
		'at': 80,
		'df': 80,
		'sa': 80,
		'sd': 80,
		'spd': 80
		},

	'manaphy': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 1.4,
		'hp': 100,
		'at': 100,
		'df': 100,
		'sa': 100,
		'sd': 100,
		'spd': 100
		},

	'darkrai': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 50.5,
		'hp': 70,
		'at': 90,
		'df': 90,
		'sa': 135,
		'sd': 90,
		'spd': 125
		},

	'shaymin': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 2.1,
		'hp': 100,
		'at': 100,
		'df': 100,
		'sa': 100,
		'sd': 100,
		'spd': 100
		},

	'arceus': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 320.0,
		'hp': 120,
		'at': 120,
		'df': 120,
		'sa': 120,
		'sd': 120,
		'spd': 120
		},

	'victini': {
		'type1': 'Fire',
		'type2': 'Psychic',
		'weight': 4.0,
		'hp': 100,
		'at': 100,
		'df': 100,
		'sa': 100,
		'sd': 100,
		'spd': 100
		},

	'snivy': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 8.1,
		'hp': 45,
		'at': 45,
		'df': 55,
		'sa': 45,
		'sd': 55,
		'spd': 63
		},

	'servine': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 16.0,
		'hp': 60,
		'at': 60,
		'df': 75,
		'sa': 60,
		'sd': 75,
		'spd': 83
		},

	'serperior': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 63.0,
		'hp': 75,
		'at': 75,
		'df': 95,
		'sa': 75,
		'sd': 95,
		'spd': 113
		},

	'tepig': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 9.9,
		'hp': 65,
		'at': 63,
		'df': 45,
		'sa': 45,
		'sd': 45,
		'spd': 45
		},

	'pignite': {
		'type1': 'Fighting',
		'type2': 'Fire',
		'weight': 55.5,
		'hp': 90,
		'at': 93,
		'df': 55,
		'sa': 70,
		'sd': 55,
		'spd': 55
		},

	'emboar': {
		'type1': 'Fighting',
		'type2': 'Fire',
		'weight': 150.0,
		'hp': 110,
		'at': 123,
		'df': 65,
		'sa': 100,
		'sd': 65,
		'spd': 65
		},

	'oshawott': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 5.9,
		'hp': 55,
		'at': 55,
		'df': 45,
		'sa': 63,
		'sd': 45,
		'spd': 45
		},

	'dewott': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 24.5,
		'hp': 75,
		'at': 75,
		'df': 60,
		'sa': 83,
		'sd': 60,
		'spd': 60
		},

	'samurott': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 94.6,
		'hp': 95,
		'at': 100,
		'df': 85,
		'sa': 108,
		'sd': 70,
		'spd': 70
		},

	'patrat': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 11.6,
		'hp': 45,
		'at': 55,
		'df': 39,
		'sa': 35,
		'sd': 39,
		'spd': 42
		},

	'watchog': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 27.0,
		'hp': 60,
		'at': 85,
		'df': 69,
		'sa': 60,
		'sd': 69,
		'spd': 77
		},

	'lillipup': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 4.1,
		'hp': 45,
		'at': 60,
		'df': 45,
		'sa': 25,
		'sd': 45,
		'spd': 55
		},

	'herdier': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 14.7,
		'hp': 65,
		'at': 80,
		'df': 65,
		'sa': 35,
		'sd': 65,
		'spd': 60
		},

	'stoutland': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 61.0,
		'hp': 85,
		'at': 110,
		'df': 90,
		'sa': 45,
		'sd': 90,
		'spd': 80
		},

	'purrloin': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 10.1,
		'hp': 41,
		'at': 50,
		'df': 37,
		'sa': 50,
		'sd': 37,
		'spd': 66
		},

	'liepard': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 37.5,
		'hp': 64,
		'at': 88,
		'df': 50,
		'sa': 88,
		'sd': 50,
		'spd': 106
		},

	'pansage': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 10.5,
		'hp': 50,
		'at': 53,
		'df': 48,
		'sa': 53,
		'sd': 48,
		'spd': 64
		},

	'simisage': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 30.5,
		'hp': 75,
		'at': 98,
		'df': 63,
		'sa': 98,
		'sd': 63,
		'spd': 101
		},

	'pansear': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 11.0,
		'hp': 50,
		'at': 53,
		'df': 48,
		'sa': 53,
		'sd': 48,
		'spd': 64
		},

	'simisear': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 28.0,
		'hp': 75,
		'at': 98,
		'df': 63,
		'sa': 98,
		'sd': 63,
		'spd': 101
		},

	'panpour': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 13.5,
		'hp': 50,
		'at': 53,
		'df': 48,
		'sa': 53,
		'sd': 48,
		'spd': 64
		},

	'simipour': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 29.0,
		'hp': 75,
		'at': 98,
		'df': 63,
		'sa': 98,
		'sd': 63,
		'spd': 101
		},

	'munna': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 23.3,
		'hp': 76,
		'at': 25,
		'df': 45,
		'sa': 67,
		'sd': 55,
		'spd': 24
		},

	'musharna': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 60.5,
		'hp': 116,
		'at': 55,
		'df': 85,
		'sa': 107,
		'sd': 95,
		'spd': 29
		},

	'pidove': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 2.1,
		'hp': 50,
		'at': 55,
		'df': 50,
		'sa': 36,
		'sd': 30,
		'spd': 43
		},

	'tranquill': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 15.0,
		'hp': 62,
		'at': 77,
		'df': 62,
		'sa': 50,
		'sd': 42,
		'spd': 65
		},

	'unfezant': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 29.0,
		'hp': 80,
		'at': 115,
		'df': 80,
		'sa': 65,
		'sd': 55,
		'spd': 93
		},

	'blitzle': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 29.8,
		'hp': 45,
		'at': 60,
		'df': 32,
		'sa': 50,
		'sd': 32,
		'spd': 76
		},

	'zebstrika': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 79.5,
		'hp': 75,
		'at': 100,
		'df': 63,
		'sa': 80,
		'sd': 63,
		'spd': 116
		},

	'roggenrola': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 18.0,
		'hp': 55,
		'at': 75,
		'df': 85,
		'sa': 25,
		'sd': 25,
		'spd': 15
		},

	'boldore': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 102.0,
		'hp': 70,
		'at': 105,
		'df': 105,
		'sa': 50,
		'sd': 40,
		'spd': 20
		},

	'gigalith': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 260.0,
		'hp': 85,
		'at': 135,
		'df': 130,
		'sa': 60,
		'sd': 80,
		'spd': 25
		},

	'woobat': {
		'type1': 'Flying',
		'type2': 'Psychic',
		'weight': 2.1,
		'hp': 55,
		'at': 45,
		'df': 43,
		'sa': 55,
		'sd': 43,
		'spd': 72
		},

	'swoobat': {
		'type1': 'Flying',
		'type2': 'Psychic',
		'weight': 10.5,
		'hp': 67,
		'at': 57,
		'df': 55,
		'sa': 77,
		'sd': 55,
		'spd': 114
		},

	'drilbur': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 8.5,
		'hp': 60,
		'at': 85,
		'df': 40,
		'sa': 30,
		'sd': 45,
		'spd': 68
		},

	'excadrill': {
		'type1': 'Ground',
		'type2': 'Steel',
		'weight': 40.4,
		'hp': 110,
		'at': 135,
		'df': 60,
		'sa': 50,
		'sd': 65,
		'spd': 88
		},

	'audino': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 31.0,
		'hp': 103,
		'at': 60,
		'df': 86,
		'sa': 60,
		'sd': 86,
		'spd': 50
		},

	'timburr': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 12.5,
		'hp': 75,
		'at': 80,
		'df': 55,
		'sa': 25,
		'sd': 35,
		'spd': 35
		},

	'gurdurr': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 40.0,
		'hp': 85,
		'at': 105,
		'df': 85,
		'sa': 40,
		'sd': 50,
		'spd': 40
		},

	'conkeldurr': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 87.0,
		'hp': 105,
		'at': 140,
		'df': 95,
		'sa': 55,
		'sd': 65,
		'spd': 45
		},

	'tympole': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 4.5,
		'hp': 50,
		'at': 50,
		'df': 40,
		'sa': 50,
		'sd': 40,
		'spd': 64
		},

	'palpitoad': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 17.0,
		'hp': 75,
		'at': 65,
		'df': 55,
		'sa': 65,
		'sd': 55,
		'spd': 69
		},

	'seismitoad': {
		'type1': 'Ground',
		'type2': 'Water',
		'weight': 62.0,
		'hp': 105,
		'at': 95,
		'df': 75,
		'sa': 85,
		'sd': 75,
		'spd': 74
		},

	'throh': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 55.5,
		'hp': 120,
		'at': 100,
		'df': 85,
		'sa': 30,
		'sd': 85,
		'spd': 45
		},

	'sawk': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 51.0,
		'hp': 75,
		'at': 125,
		'df': 75,
		'sa': 30,
		'sd': 75,
		'spd': 85
		},

	'sewaddle': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 2.5,
		'hp': 45,
		'at': 53,
		'df': 70,
		'sa': 40,
		'sd': 60,
		'spd': 42
		},

	'swadloon': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 7.3,
		'hp': 55,
		'at': 63,
		'df': 90,
		'sa': 50,
		'sd': 80,
		'spd': 42
		},

	'leavanny': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 20.5,
		'hp': 75,
		'at': 103,
		'df': 80,
		'sa': 70,
		'sd': 80,
		'spd': 92
		},

	'venipede': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 5.3,
		'hp': 30,
		'at': 45,
		'df': 59,
		'sa': 30,
		'sd': 39,
		'spd': 57
		},

	'whirlipede': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 58.5,
		'hp': 40,
		'at': 55,
		'df': 99,
		'sa': 40,
		'sd': 79,
		'spd': 47
		},

	'scolipede': {
		'type1': 'Poison',
		'type2': 'Bug',
		'weight': 200.5,
		'hp': 60,
		'at': 100,
		'df': 89,
		'sa': 55,
		'sd': 69,
		'spd': 112
		},

	'cottonee': {
		'type1': 'Grass',
		'type2': 'Fairy',
		'weight': 0.6,
		'hp': 40,
		'at': 27,
		'df': 60,
		'sa': 37,
		'sd': 50,
		'spd': 66
		},

	'whimsicott': {
		'type1': 'Grass',
		'type2': 'Fairy',
		'weight': 6.6,
		'hp': 60,
		'at': 67,
		'df': 85,
		'sa': 77,
		'sd': 75,
		'spd': 116
		},

	'petilil': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 6.6,
		'hp': 45,
		'at': 35,
		'df': 50,
		'sa': 70,
		'sd': 50,
		'spd': 30
		},

	'lilligant': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 16.3,
		'hp': 70,
		'at': 60,
		'df': 75,
		'sa': 110,
		'sd': 75,
		'spd': 90
		},

	'basculin': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 18.0,
		'hp': 70,
		'at': 92,
		'df': 65,
		'sa': 80,
		'sd': 55,
		'spd': 98
		},

	'sandile': {
		'type1': 'Ground',
		'type2': 'Dark',
		'weight': 15.2,
		'hp': 50,
		'at': 72,
		'df': 35,
		'sa': 35,
		'sd': 35,
		'spd': 65
		},

	'krokorok': {
		'type1': 'Ground',
		'type2': 'Dark',
		'weight': 33.4,
		'hp': 60,
		'at': 82,
		'df': 45,
		'sa': 45,
		'sd': 45,
		'spd': 74
		},

	'krookodile': {
		'type1': 'Ground',
		'type2': 'Dark',
		'weight': 96.3,
		'hp': 95,
		'at': 117,
		'df': 80,
		'sa': 65,
		'sd': 70,
		'spd': 92
		},

	'darumaka': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 37.5,
		'hp': 70,
		'at': 90,
		'df': 45,
		'sa': 15,
		'sd': 45,
		'spd': 50
		},

	'darmanitan': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 92.9,
		'hp': 105,
		'at': 140,
		'df': 55,
		'sa': 30,
		'sd': 55,
		'spd': 95
		},

	'maractus': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 28.0,
		'hp': 75,
		'at': 86,
		'df': 67,
		'sa': 106,
		'sd': 67,
		'spd': 60
		},

	'dwebble': {
		'type1': 'Rock',
		'type2': 'Bug',
		'weight': 14.5,
		'hp': 50,
		'at': 65,
		'df': 85,
		'sa': 35,
		'sd': 35,
		'spd': 55
		},

	'crustle': {
		'type1': 'Rock',
		'type2': 'Bug',
		'weight': 200.0,
		'hp': 70,
		'at': 95,
		'df': 125,
		'sa': 65,
		'sd': 75,
		'spd': 45
		},

	'scraggy': {
		'type1': 'Fighting',
		'type2': 'Dark',
		'weight': 11.8,
		'hp': 50,
		'at': 75,
		'df': 70,
		'sa': 35,
		'sd': 70,
		'spd': 48
		},

	'scrafty': {
		'type1': 'Fighting',
		'type2': 'Dark',
		'weight': 30.0,
		'hp': 65,
		'at': 90,
		'df': 115,
		'sa': 45,
		'sd': 115,
		'spd': 58
		},

	'sigilyph': {
		'type1': 'Flying',
		'type2': 'Psychic',
		'weight': 14.0,
		'hp': 72,
		'at': 58,
		'df': 80,
		'sa': 103,
		'sd': 80,
		'spd': 97
		},

	'yamask': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 1.5,
		'hp': 38,
		'at': 30,
		'df': 85,
		'sa': 55,
		'sd': 65,
		'spd': 30
		},

	'cofagrigus': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 76.5,
		'hp': 58,
		'at': 50,
		'df': 145,
		'sa': 95,
		'sd': 105,
		'spd': 30
		},

	'tirtouga': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 16.5,
		'hp': 54,
		'at': 78,
		'df': 103,
		'sa': 53,
		'sd': 45,
		'spd': 22
		},

	'carracosta': {
		'type1': 'Rock',
		'type2': 'Water',
		'weight': 81.0,
		'hp': 74,
		'at': 108,
		'df': 133,
		'sa': 83,
		'sd': 65,
		'spd': 32
		},

	'archen': {
		'type1': 'Flying',
		'type2': 'Rock',
		'weight': 9.5,
		'hp': 55,
		'at': 112,
		'df': 45,
		'sa': 74,
		'sd': 45,
		'spd': 70
		},

	'archeops': {
		'type1': 'Flying',
		'type2': 'Rock',
		'weight': 32.0,
		'hp': 75,
		'at': 140,
		'df': 65,
		'sa': 112,
		'sd': 65,
		'spd': 110
		},

	'trubbish': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 31.0,
		'hp': 50,
		'at': 50,
		'df': 62,
		'sa': 40,
		'sd': 62,
		'spd': 65
		},

	'garbodor': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 107.3,
		'hp': 80,
		'at': 95,
		'df': 82,
		'sa': 60,
		'sd': 82,
		'spd': 75
		},

	'zorua': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 12.5,
		'hp': 40,
		'at': 65,
		'df': 40,
		'sa': 80,
		'sd': 40,
		'spd': 65
		},

	'zoroark': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 81.1,
		'hp': 60,
		'at': 105,
		'df': 60,
		'sa': 120,
		'sd': 60,
		'spd': 105
		},

	'minccino': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 5.8,
		'hp': 55,
		'at': 50,
		'df': 40,
		'sa': 40,
		'sd': 40,
		'spd': 75
		},

	'cinccino': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 7.5,
		'hp': 75,
		'at': 95,
		'df': 60,
		'sa': 65,
		'sd': 60,
		'spd': 115
		},

	'gothita': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 5.8,
		'hp': 45,
		'at': 30,
		'df': 50,
		'sa': 55,
		'sd': 65,
		'spd': 45
		},

	'gothorita': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 18.0,
		'hp': 60,
		'at': 45,
		'df': 70,
		'sa': 75,
		'sd': 85,
		'spd': 55
		},

	'gothitelle': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 44.0,
		'hp': 70,
		'at': 55,
		'df': 95,
		'sa': 95,
		'sd': 110,
		'spd': 65
		},

	'solosis': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 1.0,
		'hp': 45,
		'at': 30,
		'df': 40,
		'sa': 105,
		'sd': 50,
		'spd': 20
		},

	'duosion': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 8.0,
		'hp': 65,
		'at': 40,
		'df': 50,
		'sa': 125,
		'sd': 60,
		'spd': 30
		},

	'reuniclus': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 20.1,
		'hp': 110,
		'at': 65,
		'df': 75,
		'sa': 125,
		'sd': 85,
		'spd': 30
		},

	'ducklett': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 5.5,
		'hp': 62,
		'at': 44,
		'df': 50,
		'sa': 44,
		'sd': 50,
		'spd': 55
		},

	'swanna': {
		'type1': 'Flying',
		'type2': 'Water',
		'weight': 24.2,
		'hp': 75,
		'at': 87,
		'df': 63,
		'sa': 87,
		'sd': 63,
		'spd': 98
		},

	'vanillite': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 5.7,
		'hp': 36,
		'at': 50,
		'df': 50,
		'sa': 65,
		'sd': 60,
		'spd': 44
		},

	'vanillish': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 41.0,
		'hp': 51,
		'at': 65,
		'df': 65,
		'sa': 80,
		'sd': 75,
		'spd': 59
		},

	'vanilluxe': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 57.5,
		'hp': 71,
		'at': 95,
		'df': 85,
		'sa': 110,
		'sd': 95,
		'spd': 79
		},

	'deerling': {
		'type1': 'Normal',
		'type2': 'Grass',
		'weight': 19.5,
		'hp': 60,
		'at': 60,
		'df': 50,
		'sa': 40,
		'sd': 50,
		'spd': 75
		},

	'sawsbuck': {
		'type1': 'Normal',
		'type2': 'Grass',
		'weight': 92.5,
		'hp': 80,
		'at': 100,
		'df': 70,
		'sa': 60,
		'sd': 70,
		'spd': 95
		},

	'emolga': {
		'type1': 'Flying',
		'type2': 'Electric',
		'weight': 5.0,
		'hp': 55,
		'at': 75,
		'df': 60,
		'sa': 75,
		'sd': 60,
		'spd': 103
		},

	'karrablast': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 5.9,
		'hp': 50,
		'at': 75,
		'df': 45,
		'sa': 40,
		'sd': 45,
		'spd': 60
		},

	'escavalier': {
		'type1': 'Bug',
		'type2': 'Steel',
		'weight': 33.0,
		'hp': 70,
		'at': 135,
		'df': 105,
		'sa': 60,
		'sd': 105,
		'spd': 20
		},

	'foongus': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 1.0,
		'hp': 69,
		'at': 55,
		'df': 45,
		'sa': 55,
		'sd': 55,
		'spd': 15
		},

	'amoonguss': {
		'type1': 'Poison',
		'type2': 'Grass',
		'weight': 10.5,
		'hp': 114,
		'at': 85,
		'df': 70,
		'sa': 85,
		'sd': 80,
		'spd': 30
		},

	'frillish': {
		'type1': 'Ghost',
		'type2': 'Water',
		'weight': 33.0,
		'hp': 55,
		'at': 40,
		'df': 50,
		'sa': 65,
		'sd': 85,
		'spd': 40
		},

	'jellicent': {
		'type1': 'Ghost',
		'type2': 'Water',
		'weight': 135.0,
		'hp': 100,
		'at': 60,
		'df': 70,
		'sa': 85,
		'sd': 105,
		'spd': 60
		},

	'alomomola': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 31.6,
		'hp': 165,
		'at': 75,
		'df': 80,
		'sa': 40,
		'sd': 45,
		'spd': 65
		},

	'joltik': {
		'type1': 'Bug',
		'type2': 'Electric',
		'weight': 0.6,
		'hp': 50,
		'at': 47,
		'df': 50,
		'sa': 57,
		'sd': 50,
		'spd': 65
		},

	'galvantula': {
		'type1': 'Bug',
		'type2': 'Electric',
		'weight': 14.3,
		'hp': 70,
		'at': 77,
		'df': 60,
		'sa': 97,
		'sd': 60,
		'spd': 108
		},

	'ferroseed': {
		'type1': 'Steel',
		'type2': 'Grass',
		'weight': 18.8,
		'hp': 44,
		'at': 50,
		'df': 91,
		'sa': 24,
		'sd': 86,
		'spd': 10
		},

	'ferrothorn': {
		'type1': 'Steel',
		'type2': 'Grass',
		'weight': 110.0,
		'hp': 74,
		'at': 94,
		'df': 131,
		'sa': 54,
		'sd': 116,
		'spd': 20
		},

	'klink': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 21.0,
		'hp': 40,
		'at': 55,
		'df': 70,
		'sa': 45,
		'sd': 60,
		'spd': 30
		},

	'klang': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 51.0,
		'hp': 60,
		'at': 80,
		'df': 95,
		'sa': 70,
		'sd': 85,
		'spd': 50
		},

	'klinklang': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 81.0,
		'hp': 60,
		'at': 100,
		'df': 115,
		'sa': 70,
		'sd': 85,
		'spd': 90
		},

	'tynamo': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 0.3,
		'hp': 35,
		'at': 55,
		'df': 40,
		'sa': 45,
		'sd': 40,
		'spd': 60
		},

	'eelektrik': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 22.0,
		'hp': 65,
		'at': 85,
		'df': 70,
		'sa': 75,
		'sd': 70,
		'spd': 40
		},

	'eelektross': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 80.5,
		'hp': 85,
		'at': 115,
		'df': 80,
		'sa': 105,
		'sd': 80,
		'spd': 50
		},

	'elgyem': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 9.0,
		'hp': 55,
		'at': 55,
		'df': 55,
		'sa': 85,
		'sd': 55,
		'spd': 30
		},

	'beheeyem': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 34.5,
		'hp': 75,
		'at': 75,
		'df': 75,
		'sa': 125,
		'sd': 95,
		'spd': 40
		},

	'litwick': {
		'type1': 'Ghost',
		'type2': 'Fire',
		'weight': 3.1,
		'hp': 50,
		'at': 30,
		'df': 55,
		'sa': 65,
		'sd': 55,
		'spd': 20
		},

	'lampent': {
		'type1': 'Ghost',
		'type2': 'Fire',
		'weight': 13.0,
		'hp': 60,
		'at': 40,
		'df': 60,
		'sa': 95,
		'sd': 60,
		'spd': 55
		},

	'chandelure': {
		'type1': 'Ghost',
		'type2': 'Fire',
		'weight': 34.3,
		'hp': 60,
		'at': 55,
		'df': 90,
		'sa': 145,
		'sd': 90,
		'spd': 80
		},

	'axew': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 18.0,
		'hp': 46,
		'at': 87,
		'df': 60,
		'sa': 30,
		'sd': 40,
		'spd': 57
		},

	'fraxure': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 36.0,
		'hp': 66,
		'at': 117,
		'df': 70,
		'sa': 40,
		'sd': 50,
		'spd': 67
		},

	'haxorus': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 105.5,
		'hp': 76,
		'at': 147,
		'df': 90,
		'sa': 60,
		'sd': 70,
		'spd': 97
		},

	'cubchoo': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 8.5,
		'hp': 55,
		'at': 70,
		'df': 40,
		'sa': 60,
		'sd': 40,
		'spd': 40
		},

	'beartic': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 260.0,
		'hp': 95,
		'at': 110,
		'df': 80,
		'sa': 70,
		'sd': 80,
		'spd': 50
		},

	'cryogonal': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 148.0,
		'hp': 70,
		'at': 50,
		'df': 30,
		'sa': 95,
		'sd': 135,
		'spd': 105
		},

	'shelmet': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 7.7,
		'hp': 50,
		'at': 40,
		'df': 85,
		'sa': 40,
		'sd': 65,
		'spd': 25
		},

	'accelgor': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 25.3,
		'hp': 80,
		'at': 70,
		'df': 40,
		'sa': 100,
		'sd': 60,
		'spd': 145
		},

	'stunfisk': {
		'type1': 'Ground',
		'type2': 'Electric',
		'weight': 11.0,
		'hp': 109,
		'at': 66,
		'df': 84,
		'sa': 81,
		'sd': 99,
		'spd': 32
		},

	'mienfoo': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 20.0,
		'hp': 45,
		'at': 85,
		'df': 50,
		'sa': 55,
		'sd': 50,
		'spd': 65
		},

	'mienshao': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 35.5,
		'hp': 65,
		'at': 125,
		'df': 60,
		'sa': 95,
		'sd': 60,
		'spd': 105
		},

	'druddigon': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 139.0,
		'hp': 77,
		'at': 120,
		'df': 90,
		'sa': 60,
		'sd': 90,
		'spd': 48
		},

	'golett': {
		'type1': 'Ground',
		'type2': 'Ghost',
		'weight': 92.0,
		'hp': 59,
		'at': 74,
		'df': 50,
		'sa': 35,
		'sd': 50,
		'spd': 35
		},

	'golurk': {
		'type1': 'Ground',
		'type2': 'Ghost',
		'weight': 330.0,
		'hp': 89,
		'at': 124,
		'df': 80,
		'sa': 55,
		'sd': 80,
		'spd': 55
		},

	'pawniard': {
		'type1': 'Steel',
		'type2': 'Dark',
		'weight': 10.2,
		'hp': 45,
		'at': 85,
		'df': 70,
		'sa': 40,
		'sd': 40,
		'spd': 60
		},

	'bisharp': {
		'type1': 'Steel',
		'type2': 'Dark',
		'weight': 70.0,
		'hp': 65,
		'at': 125,
		'df': 100,
		'sa': 60,
		'sd': 70,
		'spd': 70
		},

	'bouffalant': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 94.6,
		'hp': 95,
		'at': 110,
		'df': 95,
		'sa': 40,
		'sd': 95,
		'spd': 55
		},

	'rufflet': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 10.5,
		'hp': 70,
		'at': 83,
		'df': 50,
		'sa': 37,
		'sd': 50,
		'spd': 60
		},

	'braviary': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 41.0,
		'hp': 100,
		'at': 123,
		'df': 75,
		'sa': 57,
		'sd': 75,
		'spd': 80
		},

	'vullaby': {
		'type1': 'Flying',
		'type2': 'Dark',
		'weight': 9.0,
		'hp': 70,
		'at': 55,
		'df': 75,
		'sa': 45,
		'sd': 65,
		'spd': 60
		},

	'mandibuzz': {
		'type1': 'Flying',
		'type2': 'Dark',
		'weight': 39.5,
		'hp': 110,
		'at': 65,
		'df': 105,
		'sa': 55,
		'sd': 95,
		'spd': 80
		},

	'heatmor': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 58.0,
		'hp': 85,
		'at': 97,
		'df': 66,
		'sa': 105,
		'sd': 66,
		'spd': 65
		},

	'durant': {
		'type1': 'Bug',
		'type2': 'Steel',
		'weight': 33.0,
		'hp': 58,
		'at': 109,
		'df': 112,
		'sa': 48,
		'sd': 48,
		'spd': 109
		},

	'deino': {
		'type1': 'Dragon',
		'type2': 'Dark',
		'weight': 17.3,
		'hp': 52,
		'at': 65,
		'df': 50,
		'sa': 45,
		'sd': 50,
		'spd': 38
		},

	'zweilous': {
		'type1': 'Dragon',
		'type2': 'Dark',
		'weight': 50.0,
		'hp': 72,
		'at': 85,
		'df': 70,
		'sa': 65,
		'sd': 70,
		'spd': 58
		},

	'hydreigon': {
		'type1': 'Dragon',
		'type2': 'Dark',
		'weight': 160.0,
		'hp': 92,
		'at': 105,
		'df': 90,
		'sa': 125,
		'sd': 90,
		'spd': 98
		},

	'larvesta': {
		'type1': 'Bug',
		'type2': 'Fire',
		'weight': 28.8,
		'hp': 55,
		'at': 85,
		'df': 55,
		'sa': 50,
		'sd': 55,
		'spd': 60
		},

	'volcarona': {
		'type1': 'Bug',
		'type2': 'Fire',
		'weight': 46.0,
		'hp': 85,
		'at': 60,
		'df': 65,
		'sa': 135,
		'sd': 105,
		'spd': 100
		},

	'cobalion': {
		'type1': 'Fighting',
		'type2': 'Steel',
		'weight': 250.0,
		'hp': 91,
		'at': 90,
		'df': 129,
		'sa': 90,
		'sd': 72,
		'spd': 108
		},

	'terrakion': {
		'type1': 'Fighting',
		'type2': 'Rock',
		'weight': 260.0,
		'hp': 91,
		'at': 129,
		'df': 90,
		'sa': 72,
		'sd': 90,
		'spd': 108
		},

	'virizion': {
		'type1': 'Fighting',
		'type2': 'Grass',
		'weight': 200.0,
		'hp': 91,
		'at': 90,
		'df': 72,
		'sa': 90,
		'sd': 129,
		'spd': 108
		},

	'tornadus': {
		'type1': 'Flying',
		'type2': '(none)',
		'weight': 63.0,
		'hp': 79,
		'at': 115,
		'df': 70,
		'sa': 125,
		'sd': 80,
		'spd': 111
		},

	'thundurus': {
		'type1': 'Flying',
		'type2': 'Electric',
		'weight': 61.0,
		'hp': 79,
		'at': 115,
		'df': 70,
		'sa': 125,
		'sd': 80,
		'spd': 111
		},

	'reshiram': {
		'type1': 'Fire',
		'type2': 'Dragon',
		'weight': 330.0,
		'hp': 100,
		'at': 120,
		'df': 100,
		'sa': 150,
		'sd': 120,
		'spd': 90
		},

	'zekrom': {
		'type1': 'Electric',
		'type2': 'Dragon',
		'weight': 345.0,
		'hp': 100,
		'at': 150,
		'df': 120,
		'sa': 120,
		'sd': 100,
		'spd': 90
		},

	'landorus': {
		'type1': 'Flying',
		'type2': 'Ground',
		'weight': 68.0,
		'hp': 89,
		'at': 125,
		'df': 90,
		'sa': 115,
		'sd': 80,
		'spd': 101
		},

	'kyurem': {
		'type1': 'Ice',
		'type2': 'Dragon',
		'weight': 325.0,
		'hp': 125,
		'at': 130,
		'df': 90,
		'sa': 130,
		'sd': 90,
		'spd': 95
		},

	'keldeo': {
		'type1': 'Fighting',
		'type2': 'Water',
		'weight': 48.5,
		'hp': 91,
		'at': 72,
		'df': 90,
		'sa': 129,
		'sd': 90,
		'spd': 108
		},

	'meloetta': {
		'type1': 'Normal',
		'type2': 'Psychic',
		'weight': 6.5,
		'hp': 100,
		'at': 77,
		'df': 77,
		'sa': 128,
		'sd': 128,
		'spd': 90
		},

	'genesect': {
		'type1': 'Bug',
		'type2': 'Steel',
		'weight': 82.5,
		'hp': 71,
		'at': 120,
		'df': 95,
		'sa': 120,
		'sd': 95,
		'spd': 99
		},

	'deoxys-f': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 60.8,
		'hp': 50,
		'at': 180,
		'df': 20,
		'sa': 180,
		'sd': 20,
		'spd': 150
		},

	'deoxys-l': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 60.8,
		'hp': 50,
		'at': 70,
		'df': 160,
		'sa': 70,
		'sd': 160,
		'spd': 90
		},

	'deoxys-e': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 60.8,
		'hp': 50,
		'at': 95,
		'df': 90,
		'sa': 95,
		'sd': 90,
		'spd': 180
		},

	'wormadam-ground': {
		'type1': 'Ground',
		'type2': 'Bug',
		'weight': 6.5,
		'hp': 60,
		'at': 79,
		'df': 105,
		'sa': 59,
		'sd': 85,
		'spd': 36
		},

	'wormadam-steel': {
		'type1': 'Bug',
		'type2': 'Steel',
		'weight': 6.5,
		'hp': 60,
		'at': 69,
		'df': 95,
		'sa': 69,
		'sd': 95,
		'spd': 36
		},

	'shaymin-sky': {
		'type1': 'Flying',
		'type2': 'Grass',
		'weight': 5.2,
		'hp': 100,
		'at': 103,
		'df': 75,
		'sa': 120,
		'sd': 75,
		'spd': 127
		},

	'giratina-origin': {
		'type1': 'Ghost',
		'type2': 'Dragon',
		'weight': 650.0,
		'hp': 150,
		'at': 120,
		'df': 100,
		'sa': 120,
		'sd': 100,
		'spd': 90
		},

	'rotom-heat': {
		'type1': 'Fire',
		'type2': 'Electric',
		'weight': 0.3,
		'hp': 50,
		'at': 65,
		'df': 107,
		'sa': 105,
		'sd': 107,
		'spd': 86
		},

	'rotom-wash': {
		'type1': 'Water',
		'type2': 'Electric',
		'weight': 0.3,
		'hp': 50,
		'at': 65,
		'df': 107,
		'sa': 105,
		'sd': 107,
		'spd': 86
		},

	'rotom-fridge': {
		'type1': 'Electric',
		'type2': 'Ice',
		'weight': 0.3,
		'hp': 50,
		'at': 65,
		'df': 107,
		'sa': 105,
		'sd': 107,
		'spd': 86
		},

	'rotom-fan': {
		'type1': 'Flying',
		'type2': 'Electric',
		'weight': 0.3,
		'hp': 50,
		'at': 65,
		'df': 107,
		'sa': 105,
		'sd': 107,
		'spd': 86
		},

	'rotom-cut': {
		'type1': 'Grass',
		'type2': 'Electric',
		'weight': 0.3,
		'hp': 50,
		'at': 65,
		'df': 107,
		'sa': 105,
		'sd': 107,
		'spd': 86
		},

	'castform-sunny': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 0.8,
		'hp': 70,
		'at': 70,
		'df': 70,
		'sa': 70,
		'sd': 70,
		'spd': 70
		},

	'castform-rainy': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 0.8,
		'hp': 70,
		'at': 70,
		'df': 70,
		'sa': 70,
		'sd': 70,
		'spd': 70
		},

	'castform-icy': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 0.8,
		'hp': 70,
		'at': 70,
		'df': 70,
		'sa': 70,
		'sd': 70,
		'spd': 70
		},

	'darmanitan-zen': {
		'type1': 'Fire',
		'type2': 'Psychic',
		'weight': 92.9,
		'hp': 105,
		'at': 30,
		'df': 105,
		'sa': 140,
		'sd': 105,
		'spd': 55
		},

	'meloetta-pirouette': {
		'type1': 'Normal',
		'type2': 'Fighting',
		'weight': 6.5,
		'hp': 100,
		'at': 128,
		'df': 90,
		'sa': 77,
		'sd': 77,
		'spd': 128
		},

	'clauncher': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 8.3,
		'hp': 50,
		'at': 53,
		'df': 62,
		'sa': 58,
		'sd': 63,
		'spd': 44
		},

	'clawitzer': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 35.3,
		'hp': 71,
		'at': 73,
		'df': 88,
		'sa': 120,
		'sd': 89,
		'spd': 59
		},

	'skrelp': {
		'type1': 'Poison',
		'type2': 'Water',
		'weight': 7.3,
		'hp': 50,
		'at': 60,
		'df': 60,
		'sa': 60,
		'sd': 60,
		'spd': 30
		},

	'dragalge': {
		'type1': 'Poison',
		'type2': 'Dragon',
		'weight': 81.5,
		'hp': 65,
		'at': 75,
		'df': 90,
		'sa': 97,
		'sd': 123,
		'spd': 44
		},

	'helioptile': {
		'type1': 'Electric',
		'type2': 'Normal',
		'weight': 6,
		'hp': 44,
		'at': 38,
		'df': 33,
		'sa': 61,
		'sd': 43,
		'spd': 70
		},

	'heliolisk': {
		'type1': 'Electric',
		'type2': 'Normal',
		'weight': 21,
		'hp': 62,
		'at': 55,
		'df': 52,
		'sa': 109,
		'sd': 94,
		'spd': 109
		},

	'hawlucha': {
		'type1': 'Fighting',
		'type2': 'Flying',
		'weight': 21.5,
		'hp': 78,
		'at': 92,
		'df': 77,
		'sa': 74,
		'sd': 63,
		'spd': 118
		},

	'acafia': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 12.8,
		'hp': 62,
		'at': 68,
		'df': 56,
		'sa': 41,
		'sd': 48,
		'spd': 43
		},

	'aculago': {
		'type1': 'Normal',
		'type2': 'Ground',
		'weight': 35.7,
		'hp': 90,
		'at': 70,
		'df': 95,
		'sa': 50,
		'sd': 55,
		'spd': 60
		},

	'aeolagio': {
		'type1': 'Water',
		'type2': 'Poison',
		'weight': 30.9,
		'hp': 120,
		'at': 40,
		'df': 65,
		'sa': 110,
		'sd': 110,
		'spd': 70
		},

	'altavault': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 20.5,
		'hp': 65,
		'at': 110,
		'df': 70,
		'sa': 100,
		'sd': 50,
		'spd': 80
		},

	'arctangel': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 23.6,
		'hp': 75,
		'at': 60,
		'df': 85,
		'sa': 109,
		'sd': 91,
		'spd': 120
		},

	'auriole': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 1.7,
		'hp': 40,
		'at': 60,
		'df': 45,
		'sa': 20,
		'sd': 45,
		'spd': 60
		},

	'beddybite': {
		'type1': 'Bug',
		'type2': 'Ghost',
		'weight': 1.4,
		'hp': 70,
		'at': 40,
		'df': 45,
		'sa': 60,
		'sd': 65,
		'spd': 30
		},

	'belmarine': {
		'type1': 'Bug',
		'type2': 'Water',
		'weight': 24,
		'hp': 110,
		'at': 70,
		'df': 50,
		'sa': 100,
		'sd': 80,
		'spd': 65
		},

	'bitemare': {
		'type1': 'Bug',
		'type2': 'Ghost',
		'weight': 111.4,
		'hp': 110,
		'at': 70,
		'df': 80,
		'sa': 95,
		'sd': 100,
		'spd': 60
		},

	'bojina': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 35.4,
		'hp': 80,
		'at': 90,
		'df': 60,
		'sa': 40,
		'sd': 65,
		'spd': 65
		},

	'bossorna': {
		'type1': 'Grass',
		'type2': 'Poison',
		'weight': 115.8,
		'hp': 105,
		'at': 115,
		'df': 90,
		'sa': 70,
		'sd': 80,
		'spd': 75
		},

	'bucarat': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 3,
		'hp': 35,
		'at': 60,
		'df': 30,
		'sa': 60,
		'sd': 30,
		'spd': 60
		},

	'burungin': {
		'type1': 'Fire',
		'type2': 'Dark',
		'weight': 63.9,
		'hp': 84,
		'at': 85,
		'df': 74,
		'sa': 121,
		'sd': 70,
		'spd': 101
		},

	'caslot': {
		'type1': 'Dark',
		'type2': 'Fairy',
		'weight': 35.2,
		'hp': 70,
		'at': 75,
		'df': 80,
		'sa': 75,
		'sd': 80,
		'spd': 115
		},

	'catalcia': {
		'type1': 'Grass',
		'type2': '(none)',
		'weight': 42,
		'hp': 80,
		'at': 85,
		'df': 70,
		'sa': 54,
		'sd': 60,
		'spd': 56
		},

	'cerisol': {
		'type1': 'Grass',
		'type2': 'Fire',
		'weight': 26.7,
		'hp': 75,
		'at': 80,
		'df': 80,
		'sa': 117,
		'sd': 80,
		'spd': 93
		},

	'chantirrus': {
		'type1': 'Dragon',
		'type2': 'Flying',
		'weight': 40.9,
		'hp': 80,
		'at': 60,
		'df': 125,
		'sa': 70,
		'sd': 130,
		'spd': 70
		},

	'charandillo': {
		'type1': 'Fairy',
		'type2': 'Ground',
		'weight': 13.8,
		'hp': 62,
		'at': 103,
		'df': 107,
		'sa': 80,
		'sd': 68,
		'spd': 70
		},

	'chaszin': {
		'type1': 'Bug',
		'type2': 'Poison',
		'weight': 19.1,
		'hp': 60,
		'at': 85,
		'df': 60,
		'sa': 55,
		'sd': 55,
		'spd': 100
		},

	'coiffaire': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 39.9,
		'hp': 75,
		'at': 75,
		'df': 70,
		'sa': 60,
		'sd': 60,
		'spd': 50
		},

	'cragendou': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 78.9,
		'hp': 68,
		'at': 75,
		'df': 100,
		'sa': 35,
		'sd': 62,
		'spd': 35
		},

	'crocoal': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 7.4,
		'hp': 52,
		'at': 55,
		'df': 43,
		'sa': 70,
		'sd': 40,
		'spd': 58
		},

	'cubly': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 6.8,
		'hp': 10,
		'at': 10,
		'df': 20,
		'sa': 45,
		'sd': 35,
		'spd': 80
		},

	'curlsa': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 4.7,
		'hp': 60,
		'at': 60,
		'df': 50,
		'sa': 40,
		'sd': 40,
		'spd': 35
		},

	'dartizel-r': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 55.2,
		'hp': 70,
		'at': 80,
		'df': 60,
		'sa': 80,
		'sd': 60,
		'spd': 130
		},

	'dasfix': {
		'type1': 'Steel',
		'type2': 'Poison',
		'weight': 82.2,
		'hp': 45,
		'at': 55,
		'df': 40,
		'sa': 60,
		'sd': 70,
		'spd': 30
		},

	'derfin': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 11,
		'hp': 45,
		'at': 10,
		'df': 10,
		'sa': 80,
		'sd': 20,
		'spd': 35
		},

	'distrike': {
		'type1': 'Electric',
		'type2': 'Steel',
		'weight': 92.7,
		'hp': 85,
		'at': 100,
		'df': 80,
		'sa': 135,
		'sd': 80,
		'spd': 120
		},

	'donarith': {
		'type1': 'Bug',
		'type2': 'Electric',
		'weight': 33.9,
		'hp': 75,
		'at': 108,
		'df': 70,
		'sa': 85,
		'sd': 60,
		'spd': 102
		},

	'drakella': {
		'type1': 'Water',
		'type2': 'Grass',
		'weight': 39.5,
		'hp': 81,
		'at': 64,
		'df': 60,
		'sa': 92,
		'sd': 93,
		'spd': 80
		},

	'drasarkr': {
		'type1': 'Water',
		'type2': 'Dragon',
		'weight': 410.4,
		'hp': 115,
		'at': 110,
		'df': 75,
		'sa': 50,
		'sd': 70,
		'spd': 100
		},

	'dustley': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 4.2,
		'hp': 50,
		'at': 45,
		'df': 55,
		'sa': 25,
		'sd': 35,
		'spd': 40
		},

	'dybelial': {
		'type1': 'Rock',
		'type2': 'Dragon',
		'weight': 167.2,
		'hp': 90,
		'at': 95,
		'df': 80,
		'sa': 130,
		'sd': 110,
		'spd': 95
		},

	'dyferal': {
		'type1': 'Rock',
		'type2': 'Dragon',
		'weight': 62.1,
		'hp': 65,
		'at': 55,
		'df': 50,
		'sa': 95,
		'sd': 80,
		'spd': 75
		},

	'dyrascal': {
		'type1': 'Rock',
		'type2': 'Dragon',
		'weight': 28.3,
		'hp': 45,
		'at': 40,
		'df': 40,
		'sa': 65,
		'sd': 60,
		'spd': 50
		},

	'emperobe': {
		'type1': 'Bug',
		'type2': 'Fairy',
		'weight': 19.1,
		'hp': 70,
		'at': 50,
		'df': 65,
		'sa': 100,
		'sd': 110,
		'spd': 80
		},

	'encanoto': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 58,
		'hp': 105,
		'at': 80,
		'df': 70,
		'sa': 125,
		'sd': 85,
		'spd': 75
		},

	'espurr': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 3.5,
		'hp': 62,
		'at': 48,
		'df': 54,
		'sa': 63,
		'sd': 60,
		'spd': 68
		},

	'fallorite': {
		'type1': 'Rock',
		'type2': 'Fire',
		'weight': 111.6,
		'hp': 45,
		'at': 70,
		'df': 85,
		'sa': 55,
		'sd': 50,
		'spd': 30
		},

	'feucrota': {
		'type1': 'Fire',
		'type2': '(none)',
		'weight': 30.8,
		'hp': 66,
		'at': 69,
		'df': 54,
		'sa': 90,
		'sd': 50,
		'spd': 76
		},

	'finnial': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 9.2,
		'hp': 50,
		'at': 50,
		'df': 35,
		'sa': 75,
		'sd': 35,
		'spd': 55
		},

	'floundirt': {
		'type1': 'Water',
		'type2': 'Ground',
		'weight': 101.1,
		'hp': 105,
		'at': 50,
		'df': 80,
		'sa': 65,
		'sd': 60,
		'spd': 35
		},

	'freye': {
		'type1': 'Water',
		'type2': 'Ground',
		'weight': 15.5,
		'hp': 85,
		'at': 30,
		'df': 60,
		'sa': 45,
		'sd': 40,
		'spd': 15
		},

	'galaraud': {
		'type1': 'Ghost',
		'type2': 'Dark',
		'weight': 17.6,
		'hp': 60,
		'at': 85,
		'df': 70,
		'sa': 95,
		'sd': 70,
		'spd': 105
		},

	'galorindle': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 2.9,
		'hp': 50,
		'at': 50,
		'df': 50,
		'sa': 60,
		'sd': 50,
		'spd': 50
		},

	'galoryph': {
		'type1': 'Ghost',
		'type2': 'Psychic',
		'weight': 29.8,
		'hp': 105,
		'at': 70,
		'df': 70,
		'sa': 95,
		'sd': 85,
		'spd': 60
		},

	'garapaima': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 218.9,
		'hp': 85,
		'at': 120,
		'df': 90,
		'sa': 60,
		'sd': 55,
		'spd': 80
		},

	'gasvirlich': {
		'type1': 'Fire',
		'type2': 'Fairy',
		'weight': 4.8,
		'hp': 55,
		'at': 70,
		'df': 55,
		'sa': 130,
		'sd': 100,
		'spd': 105
		},

	'gowatu': {
		'type1': 'Grass',
		'type2': 'Flying',
		'weight': 3.8,
		'hp': 40,
		'at': 55,
		'df': 35,
		'sa': 60,
		'sd': 35,
		'spd': 50
		},

	'gravendou': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 20.4,
		'hp': 48,
		'at': 60,
		'df': 85,
		'sa': 15,
		'sd': 42,
		'spd': 15
		},

	'halberax-r': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 87.8,
		'hp': 70,
		'at': 140,
		'df': 70,
		'sa': 40,
		'sd': 90,
		'spd': 70
		},

	'halirth': {
		'type1': 'Water',
		'type2': 'Ground',
		'weight': 320.3,
		'hp': 125,
		'at': 70,
		'df': 100,
		'sa': 85,
		'sd': 80,
		'spd': 55
		},

	'heladalca': {
		'type1': 'Ice',
		'type2': 'Fighting',
		'weight': 105.9,
		'hp': 95,
		'at': 115,
		'df': 100,
		'sa': 50,
		'sd': 100,
		'spd': 60
		},

	'humbuzz': {
		'type1': 'Electric',
		'type2': 'Flying',
		'weight': 0.4,
		'hp': 30,
		'at': 10,
		'df': 10,
		'sa': 60,
		'sd': 50,
		'spd': 120
		},

	'ibazel': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 49.2,
		'hp': 55,
		'at': 80,
		'df': 50,
		'sa': 122,
		'sd': 113,
		'spd': 80
		},

	'ignelix': {
		'type1': 'Fire',
		'type2': 'Rock',
		'weight': 367,
		'hp': 70,
		'at': 75,
		'df': 130,
		'sa': 105,
		'sd': 105,
		'spd': 30
		},

	'icauriole': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 29.4,
		'hp': 60,
		'at': 110,
		'df': 60,
		'sa': 50,
		'sd': 60,
		'spd': 110
		},

	'invicunya': {
		'type1': 'Ice',
		'type2': '(none)',
		'weight': 36.5,
		'hp': 65,
		'at': 60,
		'df': 70,
		'sa': 60,
		'sd': 70,
		'spd': 40
		},

	'jackravage': {
		'type1': 'Normal',
		'type2': 'Fighting',
		'weight': 44.5,
		'hp': 65,
		'at': 96,
		'df': 84,
		'sa': 54,
		'sd': 76,
		'spd': 105
		},

	'josuche': {
		'type1': 'Fighting',
		'type2': 'Flying',
		'weight': 56.7,
		'hp': 65,
		'at': 95,
		'df': 60,
		'sa': 95,
		'sd': 65,
		'spd': 110
		},

	'kelfee': {
		'type1': 'Water',
		'type2': 'Grass',
		'weight': 9.8,
		'hp': 51,
		'at': 34,
		'df': 30,
		'sa': 62,
		'sd': 63,
		'spd': 50
		},

	'khargonaut': {
		'type1': 'Water',
		'type2': 'Dark',
		'weight': 576.2,
		'hp': 85,
		'at': 130,
		'df': 100,
		'sa': 80,
		'sd': 90,
		'spd': 70
		},

	'kiblis': {
		'type1': 'Dark',
		'type2': '(none)',
		'weight': 18.1,
		'hp': 35,
		'at': 50,
		'df': 30,
		'sa': 82,
		'sd': 73,
		'spd': 50
		},

	'kizziff': {
		'type1': 'Bug',
		'type2': 'Poison',
		'weight': 2.2,
		'hp': 25,
		'at': 45,
		'df': 30,
		'sa': 20,
		'sd': 20,
		'spd': 50
		},

	'klaitning': {
		'type1': 'Electric',
		'type2': 'Flying',
		'weight': 3,
		'hp': 50,
		'at': 30,
		'df': 30,
		'sa': 100,
		'sd': 90,
		'spd': 160
		},

	'kraitra': {
		'type1': 'Water',
		'type2': 'Poison',
		'weight': 87.1,
		'hp': 80,
		'at': 95,
		'df': 85,
		'sa': 85,
		'sd': 95,
		'spd': 85
		},

	'lamanda': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 14.2,
		'hp': 10,
		'at': 70,
		'df': 10,
		'sa': 45,
		'sd': 35,
		'spd': 30
		},

	'lamlie': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 10,
		'hp': 45,
		'at': 70,
		'df': 50,
		'sa': 30,
		'sd': 35,
		'spd': 50
		},

	'latikrai': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 30.1,
		'hp': 45,
		'at': 60,
		'df': 55,
		'sa': 55,
		'sd': 65,
		'spd': 50
		},

	'lobovo': {
		'type1': 'Ground',
		'type2': '(none)',
		'weight': 31.7,
		'hp': 60,
		'at': 80,
		'df': 65,
		'sa': 45,
		'sd': 50,
		'spd': 65
		},

	'loftitan-r': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 725.9,
		'hp': 80,
		'at': 100,
		'df': 105,
		'sa': 50,
		'sd': 105,
		'spd': 40
		},

	'luvaris': {
		'type1': 'Ground',
		'type2': 'Fighting',
		'weight': 72.8,
		'hp': 80,
		'at': 100,
		'df': 90,
		'sa': 65,
		'sd': 75,
		'spd': 90
		},

	'lyrissimo': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 28,
		'hp': 81,
		'at': 70,
		'df': 67,
		'sa': 122,
		'sd': 65,
		'spd': 121
		},

	'makima': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 1,
		'hp': 40,
		'at': 10,
		'df': 55,
		'sa': 55,
		'sd': 55,
		'spd': 40
		},

	'makitaku': {
		'type1': 'Normal',
		'type2': 'Psychic',
		'weight': 3.2,
		'hp': 70,
		'at': 30,
		'df': 105,
		'sa': 105,
		'sd': 105,
		'spd': 70
		},

	'malraja': {
		'type1': 'Steel',
		'type2': 'Poison',
		'weight': 184.2,
		'hp': 80,
		'at': 90,
		'df': 75,
		'sa': 95,
		'sd': 120,
		'spd': 65
		},

	'mandicore': {
		'type1': 'Bug',
		'type2': 'Dark',
		'weight': 70.4,
		'hp': 75,
		'at': 100,
		'df': 100,
		'sa': 40,
		'sd': 75,
		'spd': 110
		},

	'marazuma': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 31.3,
		'hp': 85,
		'at': 55,
		'df': 105,
		'sa': 75,
		'sd': 120,
		'spd': 75
		},

	'marvelisk': {
		'type1': 'Dragon',
		'type2': '(none)',
		'weight': 70.4,
		'hp': 71,
		'at': 115,
		'df': 70,
		'sa': 105,
		'sd': 99,
		'spd': 80
		},

	'mefflora': {
		'type1': 'Grass',
		'type2': 'Fairy',
		'weight': 5.4,
		'hp': 50,
		'at': 20,
		'df': 40,
		'sa': 50,
		'sd': 65,
		'spd': 35
		},

	'meowstic': {
		'type1': 'Psychic',
		'type2': '(none)',
		'weight': 8.5,
		'hp': 74,
		'at': 48,
		'df': 76,
		'sa': 83,
		'sd': 81,
		'spd': 104
		},

	'mephodil': {
		'type1': 'Grass',
		'type2': 'Fairy',
		'weight': 17.9,
		'hp': 70,
		'at': 40,
		'df': 60,
		'sa': 70,
		'sd': 100,
		'spd': 55
		},

	'minijina': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 13.2,
		'hp': 60,
		'at': 70,
		'df': 40,
		'sa': 20,
		'sd': 45,
		'spd': 45
		},

	'moirexe': {
		'type1': 'Ghost',
		'type2': 'Bug',
		'weight': 34.1,
		'hp': 85,
		'at': 126,
		'df': 71,
		'sa': 103,
		'sd': 92,
		'spd': 103
		},

	'mortarat': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 13.7,
		'hp': 55,
		'at': 95,
		'df': 50,
		'sa': 95,
		'sd': 50,
		'spd': 95
		},

	'murgaz': {
		'type1': 'Bug',
		'type2': 'Poison',
		'weight': 8.9,
		'hp': 40,
		'at': 55,
		'df': 40,
		'sa': 35,
		'sd': 35,
		'spd': 65
		},

	'nahualatu': {
		'type1': 'Psychic',
		'type2': 'Dragon',
		'weight': 41.1,
		'hp': 70,
		'at': 100,
		'df': 80,
		'sa': 125,
		'sd': 80,
		'spd': 95
		},

	'nekhetura': {
		'type1': 'Fire',
		'type2': 'Flying',
		'weight': 35.8,
		'hp': 97,
		'at': 106,
		'df': 70,
		'sa': 85,
		'sd': 65,
		'spd': 86
		},

	'noperajina': {
		'type1': 'Ghost',
		'type2': '(none)',
		'weight': 96.5,
		'hp': 100,
		'at': 120,
		'df': 80,
		'sa': 60,
		'sd': 85,
		'spd': 80
		},

	'nulohm': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 89.5,
		'hp': 80,
		'at': 118,
		'df': 86,
		'sa': 65,
		'sd': 74,
		'spd': 82
		},

	'nuwill': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 32.6,
		'hp': 60,
		'at': 78,
		'df': 66,
		'sa': 45,
		'sd': 54,
		'spd': 62
		},

	'onzarudo': {
		'type1': 'Fighting',
		'type2': 'Dark',
		'weight': 136.4,
		'hp': 78,
		'at': 118,
		'df': 92,
		'sa': 63,
		'sd': 75,
		'spd': 74
		},

	'osgrave': {
		'type1': 'Water',
		'type2': 'Flying',
		'weight': 65.5,
		'hp': 80,
		'at': 112,
		'df': 70,
		'sa': 80,
		'sd': 81,
		'spd': 112
		},

	'ostento': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 215.4,
		'hp': 115,
		'at': 115,
		'df': 90,
		'sa': 70,
		'sd': 70,
		'spd': 65
		},

	'pandive': {
		'type1': 'Water',
		'type2': 'Flying',
		'weight': 26.9,
		'hp': 62,
		'at': 82,
		'df': 50,
		'sa': 63,
		'sd': 66,
		'spd': 82
		},

	'paracordis': {
		'type1': 'Bug',
		'type2': 'Grass',
		'weight': 45.1,
		'hp': 70,
		'at': 115,
		'df': 90,
		'sa': 75,
		'sd': 90,
		'spd': 65
		},

	'petrocka': {
		'type1': 'Fighting',
		'type2': 'Rock',
		'weight': 33.1,
		'hp': 60,
		'at': 120,
		'df': 85,
		'sa': 40,
		'sd': 75,
		'spd': 95
		},

	'pindillo': {
		'type1': 'Fairy',
		'type2': '(none)',
		'weight': 4.1,
		'hp': 32,
		'at': 68,
		'df': 72,
		'sa': 45,
		'sd': 38,
		'spd': 40
		},

	'quarendou': {
		'type1': 'Rock',
		'type2': '(none)',
		'weight': 256.2,
		'hp': 98,
		'at': 105,
		'df': 150,
		'sa': 45,
		'sd': 82,
		'spd': 45
		},

	'quimpy': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 9,
		'hp': 25,
		'at': 55,
		'df': 20,
		'sa': 10,
		'sd': 10,
		'spd': 80
		},

	'raidnarr': {
		'type1': 'Water',
		'type2': 'Dragon',
		'weight': 46.4,
		'hp': 95,
		'at': 90,
		'df': 55,
		'sa': 40,
		'sd': 50,
		'spd': 85
		},

	'rakateis': {
		'type1': 'Ice',
		'type2': 'Flying',
		'weight': 14.9,
		'hp': 70,
		'at': 114,
		'df': 52,
		'sa': 80,
		'sd': 78,
		'spd': 91
		},

	'ramfere': {
		'type1': 'Electric',
		'type2': 'Dark',
		'weight': 72.9,
		'hp': 90,
		'at': 115,
		'df': 90,
		'sa': 55,
		'sd': 75,
		'spd': 75
		},

	'rapscalion': {
		'type1': 'Normal',
		'type2': 'Flying',
		'weight': 34.5,
		'hp': 72,
		'at': 115,
		'df': 70,
		'sa': 78,
		'sd': 85,
		'spd': 102
		},

	'rasqueon': {
		'type1': 'Dragon',
		'type2': 'Steel',
		'weight': 133.2,
		'hp': 100,
		'at': 90,
		'df': 115,
		'sa': 60,
		'sd': 80,
		'spd': 60
		},

	'razelodon': {
		'type1': 'Steel',
		'type2': '(none)',
		'weight': 140.5,
		'hp': 80,
		'at': 130,
		'df': 130,
		'sa': 40,
		'sd': 60,
		'spd': 65
		},

	'seviron': {
		'type1': 'Poison',
		'type2': '(none)',
		'weight': 95.1,
		'hp': 83,
		'at': 85,
		'df': 105,
		'sa': 85,
		'sd': 105,
		'spd': 75
		},

	'siamacho': {
		'type1': 'Water',
		'type2': 'Fighting',
		'weight': 54.4,
		'hp': 75,
		'at': 104,
		'df': 70,
		'sa': 89,
		'sd': 60,
		'spd': 112
		},

	'sparcoil': {
		'type1': 'Fire',
		'type2': 'Fighting',
		'weight': 29,
		'hp': 75,
		'at': 105,
		'df': 60,
		'sa': 75,
		'sd': 80,
		'spd': 85
		},

	'spilotalis': {
		'type1': 'Grass',
		'type2': 'Fairy',
		'weight': 44.6,
		'hp': 90,
		'at': 55,
		'df': 75,
		'sa': 90,
		'sd': 130,
		'spd': 75
		},

	'spraylet': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 4.8,
		'hp': 49,
		'at': 64,
		'df': 41,
		'sa': 49,
		'sd': 51,
		'spd': 64
		},

	'termelc': {
		'type1': 'Electric',
		'type2': '(none)',
		'weight': 50.7,
		'hp': 60,
		'at': 70,
		'df': 60,
		'sa': 90,
		'sd': 60,
		'spd': 80
		},

	'thunderma': {
		'type1': 'Electric',
		'type2': 'Poison',
		'weight': 28.5,
		'hp': 105,
		'at': 90,
		'df': 70,
		'sa': 45,
		'sd': 65,
		'spd': 50
		},

	'tianglis': {
		'type1': 'Dark',
		'type2': 'Flying',
		'weight': 14.2,
		'hp': 55,
		'at': 90,
		'df': 75,
		'sa': 75,
		'sd': 60,
		'spd': 80
		},

	'tinimer': {
		'type1': 'Bug',
		'type2': '(none)',
		'weight': 1.3,
		'hp': 45,
		'at': 45,
		'df': 45,
		'sa': 50,
		'sd': 45,
		'spd': 45
		},

	'torranel': {
		'type1': 'Fire',
		'type2': 'Dragon',
		'weight': 177.6,
		'hp': 70,
		'at': 80,
		'df': 90,
		'sa': 120,
		'sd': 75,
		'spd': 80
		},

	'transmite': {
		'type1': 'Electric',
		'type2': 'Flying',
		'weight': 1.5,
		'hp': 55,
		'at': 75,
		'df': 60,
		'sa': 80,
		'sd': 75,
		'spd': 90
		},

	'turatal': {
		'type1': 'Grass',
		'type2': 'Flying',
		'weight': 18.1,
		'hp': 70,
		'at': 85,
		'df': 60,
		'sa': 95,
		'sd': 60,
		'spd': 80
		},

	'turistar': {
		'type1': 'Water',
		'type2': '(none)',
		'weight': 5,
		'hp': 30,
		'at': 70,
		'df': 50,
		'sa': 35,
		'sd': 50,
		'spd': 85
		},

	'turumaken': {
		'type1': 'Water',
		'type2': 'Steel',
		'weight': 18,
		'hp': 60,
		'at': 100,
		'df': 90,
		'sa': 65,
		'sd': 85,
		'spd': 115
		},

	'vaering': {
		'type1': 'Water',
		'type2': 'Dragon',
		'weight': 25.8,
		'hp': 75,
		'at': 65,
		'df': 40,
		'sa': 30,
		'sd': 35,
		'spd': 60
		},

	'valazman': {
		'type1': 'Fighting',
		'type2': '(none)',
		'weight': 40.6,
		'hp': 80,
		'at': 120,
		'df': 101,
		'sa': 60,
		'sd': 80,
		'spd': 99
		},

	'vaquerado': {
		'type1': 'Bug',
		'type2': 'Ground',
		'weight': 22.9,
		'hp': 70,
		'at': 80,
		'df': 65,
		'sa': 100,
		'sd': 50,
		'spd': 110 
		},

	'ventorm': {
		'type1': 'Water',
		'type2': 'Fire',
		'weight': 167.7,
		'hp': 80,
		'at': 60,
		'df': 110,
		'sa': 120,
		'sd': 100,
		'spd': 50
		},

	'virlich': {
		'type1': 'Fire',
		'type2': 'Fairy',
		'weight': 0.2,
		'hp': 35,
		'at': 50,
		'df': 35,
		'sa': 85,
		'sd': 70,
		'spd': 70
		},

	'volstarite': {
		'type1': 'Rock',
		'type2': 'Fire',
		'weight': 836.6,
		'hp': 65,
		'at': 120,
		'df': 135,
		'sa': 95,
		'sd': 70,
		'spd': 50
		},

	'vulkhet': {
		'type1': 'Fire',
		'type2': 'Flying',
		'weight': 12.1,
		'hp': 67,
		'at': 76,
		'df': 40,
		'sa': 35,
		'sd': 35,
		'spd': 46
		},

	'wyrmal': {
		'type1': 'Water',
		'type2': 'Fire',
		'weight': 18.8,
		'hp': 40,
		'at': 30,
		'df': 50,
		'sa': 60,
		'sd': 45,
		'spd': 50
		},

	'zanthera': {
		'type1': 'Normal',
		'type2': '(none)',
		'weight': 90.4,
		'hp': 83,
		'at': 125,
		'df': 80,
		'sa': 70,
		'sd': 80,
		'spd': 100
		},
    'bearbegazi': {
        'type1': 'Ice',
        'type2': 'Fairy',
		'weight': 29.7,
		'hp': 80,
		'at': 50,
		'df': 65,
		'sa': 80,
		'sd': 90,
		'spd': 65
        },
    'hollimin': {
        'type1': 'Fairy',
        'type2': 'Grass',
		'weight': 8.4,
		'hp': 40,
		'at': 60,
		'df': 50,
		'sa': 45,
		'sd': 50,
		'spd': 85
        },
};

var ALL_ITEMS = [
    'Adamant Orb', 'Air Balloon', 'Apicot Berry', 'Armor Fossil', 'Babiri Berry',
    'Belue Berry', 'Black Belt', 'Black Sludge', 'BlackGlasses', 'Bug Gem', 
    'Charcoal', 'Charti Berry', 'Chesto Berry', 'Chilan Berry', 'Choice Band', 
    'Choice Scarf', 'Choice Specs', 'Chople Berry', 'Claw Fossil', 'Coba Berry', 
    'Colbur Berry', 'Cover Fossil', 'Custap Berry', 'Dark Gem', 'DeepSeaScale', 
    'DeepSeaTooth', 'Dome Fossil', 'Draco Plate', 'Dragon Fang', 'Dragon Gem', 
    'Dread Plate', 'Durin Berry', 'Earth Plate', 'Electric Gem', 'Enigma Berry', 
    'Eviolite', 'Expert Belt', 'Fighting Gem', 'Fire Gem', 'Fist Plate', 'Flame Orb', 
    'Flame Plate', 'Flying Gem', 'Ganlon Berry', 'Ghost Gem', 'Griseous Orb', 
    'Grass Gem', 'Ground Gem', 'Haban Berry', 'Hard Stone', 'Helix Fossil', 'Ice Gem', 
    'Icicle Plate', 'Insect Plate', 'Iron Ball', 'Iron Plate', 'Jaboca Berry', 
    'Kasib Berry', 'Kebia Berry', "King's Rock", 'Lagging Tail', 'Lansat Berry', 
    'Leftovers', 'Leppa Berry', 'Liechi Berry', 'Life Orb', 'Light Ball', 
    'Lum Berry', 'Lustrous Orb', 'Macho Brace', 'Magnet', 'Meadow Plate', 
    'Metal Coat', 'Metal Powder', 'Micle Berry', 'Mind Plate', 'Miracle Seed', 
    'Muscle Band', 'Mystic Water', 'NeverMeltIce', 'Normal Gem', 'Occa Berry', 
    'Odd Incense', 'Old Amber', 'Oran Berry', 'Pamtre Berry', 'Passho Berry', 
    'Payapa Berry', 'Petaya Berry', 'Plume Fossil', 'Poison Barb', 'Poison Gem', 
    'Psychic Gem', 'Rare Bone', 'Rawst Berry', 'Razor Fang', 'Rindo Berry', 
    'Rock Gem', 'Rock Incense', 'Root Fossil', 'Rose Incense', 'Rowap Berry', 
    'Salac Berry', 'Sea Incense', 'Sharp Beak', 'Shuca Berry', 'Silk Scarf', 
    'SilverPowder', 'Sitrus Berry', 'Skull Fossil', 'Sky Plate', 'Soft Sand', 
    'Soul Dew', 'Spell Tag', 'Splash Plate', 'Spooky Plate', 'Starf Berry', 
    'Steel Gem', 'Stone Plate', 'Tanga Berry', 'Thick Club', 'Toxic Orb', 
    'Toxic Plate', 'TwistedSpoon', 'Wacan Berry', 'Water Gem', 'Watmel Berry', 
    'Wave Incense', 'Wise Glasses', 'Yache Berry', 'Zap Plate'
];

var ALL_ABILITIES = [
    'Adaptability', 'Air Lock', 'Analytic', 'Blaze', 'Clear Body', 'Cloud Nine', 
    'Contrary', 'Defeatist', 'Defiant', 'Download', 'Drizzle', 'Drought', 
    'Dry Skin', 'Filter', 'Flare Boost', 'Flash Fire', 'Flower Gift', 'Forecast', 
    'Guts', 'Heatproof', 'Huge Power', 'Hustle', 'Hyper Cutter', 'Ice Body', 
    'Infiltrator', 'Intimidate', 'Iron Fist', 'Klutz', 'Levitate', 'Lightningrod', 
    'Magic Guard', 'Marvel Scale', 'Mold Breaker', 'Motor Drive', 'Multiscale', 
    'Normalize', 'Overcoat', 'Overgrow', 'Poison Heal', 'Pure Power', 'Rain Dish', 
    'Reckless', 'Rivalry (+)', 'Rivalry (-)', 'Sand Force', 'Sand Rush', 
    'Sand Stream', 'Sand Veil', 'Sap Sipper', 'Scrappy', 'Sheer Force', 
    'Skill Link', 'Slow Start', 'Snow Cloak', 'Snow Warning', 'Solar Power', 
    'Solid Rock', 'Soundproof', 'Storm Drain', 'Swarm', 'Technician', 'Teravolt', 
    'Thick Fat', 'Tinted Lens', 'Torrent', 'Toxic Boost', 'Turboblaze', 'Unaware', 
    'Unnerve', 'Volt Absorb', 'Water Absorb', 'White Smoke', 'Wonder Guard'
];

var ALL_TYPES = {
    'Normal':{
        'nve':['Rock','Steel'],
        'immune':['Ghost']
    },
    'Grass':{
        'nve':['Grass','Fire','Flying','Bug','Poison','Dragon','Steel'],
        'se':['Water','Ground','Rock']
    },
    'Fire':{
        'nve':['Fire','Water','Rock','Dragon'],
        'se':['Grass','Ice','Bug','Steel']
    },
    'Water':{
        'nve':['Grass','Water','Dragon'],
        'se':['Fire','Ground','Rock']
    },
    'Electric':{
        'nve':['Grass','Electric','Dragon'],
        'se':['Water','Flying'],
        'immune':['Ground']
    },
    'Ice':{
        'nve':['Fire','Water','Ice','Steel'],
        'se':['Grass','Flying','Ground','Dragon']
    },
    'Flying':{
        'nve':['Electric','Rock','Steel'],
        'se':['Grass','Fighting','Bug']
    },
    'Fighting':{
        'nve':['Flying','Bug','Poison','Psychic','Fairy'],
        'se':['Normal','Ice','Rock','Dark','Steel'],
        'immune':['Ghost']
    },
    'Bug':{
        'nve':['Fire','Flying','Fighting','Poison','Ghost','Steel','Fairy'],
        'se':['Grass','Psychic','Dark']
    },
    'Poison':{
        'nve':['Poison','Ground','Rock','Ghost'],
        'se':['Grass','Fairy'],
        'immune':['Steel']
    },
    'Ground':{
        'nve':['Grass','Bug'],
        'se':['Fire','Electric','Poison','Rock','Steel'],
        'immune':['Flying']
    },
    'Rock':{
        'nve':['Fighting','Ground','Steel'],
        'se':['Fire','Ice','Flying','Bug']
    },
    'Psychic':{
        'nve':['Psychic','Steel'],
        'se':['Fighting','Poison'],
        'immune':['Dark']
    },
    'Ghost':{
        'nve':['Dark'],
        'se':['Psychic','Ghost'],
        'immune':['Normal']
    },
    'Dragon':{
        'nve':['Steel'],
        'se':['Dragon'],
        'immune':['Fairy']
    },
    'Dark':{
        'nve':['Fighting','Dark','Fairy'],
        'se':['Psychic','Ghost']
    },
    'Steel':{
        'nve':['Fire','Water','Electric','Steel'],
        'se':['Ice','Rock','Fairy']
    },
    'Fairy':{
        'nve':['Fire','Poison','Steel'],
        'se':['Dark','Dragon','Fighting'],
    }
};

exports.calcStat = calcStat;
exports.calcHP = calcHP;
exports.getTypeMatch = getTypeMatch;
exports.ALL_ABILITIES = ALL_ABILITIES;
exports.ALL_ATTACKS = ALL_ATTACKS;
exports.ALL_ITEMS = ALL_ITEMS;
exports.ALL_TYPES = ALL_TYPES;
exports.ALL_POKEMON = ALL_POKEMON;
