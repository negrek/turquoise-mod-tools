#!/usr/bin/env python
# -*- encoding: utf-8 -*-

'''
    Utility functions associated with actual RPG moderator activities. Does things like generate wild pokémon or trainers for a person to fight, handles battles, calculates capture success/failure, etc.
'''

def apply_level_up(pokemon, target_level, location, cursor):
    '''
        Perform all necessary operations necessary to get a pokémon from one level to the next. Returns both the pokémon and a string describing everything that happened.

        Expects to receive a valid database connection for some info retrieval.
    '''

    # TODO: sqlite cursors-as-dicts: worth it for this use case?
    # TODO: currently fails so hard when the identifier is for
    # a shiny, female, or formed pokémon! Fixxx it!

    # Get data on this pokémon's evolutionary line
    cursor.execute('''select pokemon.id, pokemon.name, pokemon.identifier, pokemon.HP, namejoin.name, namejoin.identifier, items.name, pokemon_evolution.* from pokemon left join pokemon_evolution on pokemon.id = pokemon_evolution.evolved_species_id left join items on pokemon_evolution.trigger_item_id = items.id inner join pokemon as namejoin on namejoin.id = pokemon.evolves_from_pokemon_id where pokemon.evolution_chain_id = (select evolution_chain_id from pokemon where identifier=?) order by pokemon.id''', (pokemon['identifier'],))

    mondata = cursor.fetchall()
    #print mondata
    description = ''

    moves_learned = get_level_up_moves(cursor, mondata[0][0], pokemon['level'], target_level)

    # If applicable, check evolutions
    if len(mondata) > 1:
        new_mon = check_evolution(pokemon, mondata, target_level, location)
        
        # if new_mon['species'] != mon['species']:
        #    # need to check whether a further evolution occurs
        #    # repeat this somehow!

    # Add any appropriate moves to the mon's data
    # Also announce them via the description
    if moves_learned:
        movelist = [move[0] for move in moves_learned]
        pokemon['moves_literal'] += ', ' + ', '.join(movelist)
        pokemon['moves'] += tuple(movelist)

        for move in movelist:
            description += '{0} learned {1}!\n'.format(pokemon['name'], move)

    print description
    print pokemon['moves']
    print pokemon['moves_literal']

    return (True, True)
    #return (description, pokemon)

def award_exp(pokemon, exp, location, cursor):
    '''
        Apply experience points to a pokémon, leveling it up, giving it new moves, and/or evolving it as necessary. Returns both the pokémon with all changes applied and a string describing what all happened.

        Expects to receive a valid database connection for some info retrieval.

    '''

    pokemon['exp'] = pokemon['exp'] + exp

    description = '{0} gained {1}% EXP'.format(pokemon['name'], exp)

    if exp >= 100:
        # At least one level gained
        # How many total?
        levels = pokemon['exp'] / 100

        # Set display EXP to remainder
        pokemon['exp'] = pokemon['exp'] % 100

        # Do leveling stuff
        level_description, pokemon = apply_level_up(pokemon, pokemon['level'] + levels, location, cursor)

        # Apply level-up moves, if applicable

        # Evolve the pokémon, if applicable

        # Check again for level-up moves after evolution

        # Calculate HP

    return True
    #return (description, pokemon)

def calculate_battle_exp(player_team, enemy):
    '''
        Whenever an opponent is knocked out, calculate the EXP awarded to all pokémon that participated (and, potentially, those wearing an EXP share).
    '''

    # TODO: more extensive testing of this formula

    weak_mon = ['caterpie', 'weedle', 'magikarp', 'wurmple', 'feebas', 'kricketot', 'derfin', 'cubly', 'quimpy', 'lamanda']
    strong_mon = ['chansey', 'blissey', 'audino', 'articuno', 'zapdos', 'moltres', 'mewtwo', 'mew', 'entei', 'raikou', 'suicune', 'ho-oh', 'lugia', 'celebi', 'regice', 'regirock', 'registeel', 'latias', 'latios', 'groudon', 'kyogre', 'rayquaza', 'jirachi', 'deoxys', 'uxie', 'mesprit', 'azelf', 'dialga', 'palkia', 'giratina', 'darkrai', 'cresselia', 'manaphy', 'heatran', 'regigigas', 'shaymin', 'arceus', 'victini', 'cobalion', 'terrakion', 'virizion', 'keldeo', 'tornadus', 'thundurus', 'landorus', 'zekrom', 'reshiram', 'kyurem', 'meloetta', 'genesect', 'itarvan', 'kolnoq', 'pujuccu', 'tmir', 'vydhwg', 'ateruz', 'bexsef']

    # Get number of participants
    participants = len([mon for mon in player_team if player_team[mon]['participated']])

    #print 'participants', participants

    # Check for exp share
    if len([mon for mon in player_team if player_team[mon]['item'] == 'expshare']):
        exp_share = 1
    else:
        exp_share = 0

    #print 'exp_share', exp_share

    for pokemon in player_team:
        # Get exponent (alpha) for formula
        mon = player_team[pokemon]

        base = (mon['level'] - 1) / 10 

        if mon['level'] <= 30:
            alpha = base + 1
        elif mon['level'] > 30 and mon['level'] <= 80:
            alpha = base + 2
        elif mon['level'] > 80 and mon['level'] <= 90:
            alpha = base + 3
        else:
            alpha = base + 4

        #print 'alpha', alpha

        exp = (float(enemy['level']) / mon['level']) ** alpha

        #print exp

        # Account for the potential for shinies
        # Remove them with great prejudice
        species = enemy['identifier'].rstrip('shiny')

        if species in weak_mon:
            exp = 0.8 * exp
        elif species in strong_mon:
            exp = 1.2 * exp

        #print 'After species bonus', exp

        if mon['traded']:
            exp = exp * 1.5

        if enemy['trained']:
            exp = exp * 1.5

        #print 'After trained', exp

        # Do EXP share shenanigans
        if mon['item'] == 'expshare' and mon['participated']:
            exp = round(exp * 25) + round(exp * 1.0 / participants * 25)
        elif mon['item'] == 'expshare':
            exp = round(exp * 25) 
        elif exp_share:
            exp = round(exp * 1.0 / participants * 25)
        else:
            exp = round(exp * 1.0 / participants * 50) 

        #print 'After exp share', exp

        #exp = round(exp * 100)

        # Hmm, evidently the amount of EXP that can be earned is bounded!
        if exp < 5:
            exp = 5
        elif exp > 500:
            exp = 500

        player_team[pokemon]['exp'] = exp

    # Send back the team with their EXP filled out
    return player_team

def calculate_hp(base_hp, level):
    '''
        Does what it says on the tin: calculates the HP value for a pokémon based on the base HP value and its level.
    '''

    return int((2 * int(base_hp) + 31 + 63) * float(level) / 100 + level + 10)

def format_pokemon(pokemon):
    '''
        Takes a dictionary of pokémon data and returns the BBCode for displaying that pokémon in standard form:

        [Level] Sex Sprite Current HP/Max HP [Ability] @ Item (Special Moves)
    '''

    if 'current_hp' in pokemon:
        # The pokémon's in a battle
        formatted = u'[{0}] [sp={1}][sp={2}] {3}/{4} [{5}]'.format(pokemon['level'], pokemon['sex'], pokemon['identifier'], pokemon['current_hp'], pokemon['max_hp'], pokemon['ability'])

    else:
        # The pokémon's just being displayed
        formatted = u'[{0}] [sp={1}][sp={2}] {3}/{3} [{4}]'.format(pokemon['level'], pokemon['sex'], pokemon['identifier'], pokemon['hp'], pokemon['ability'])

    if 'item' in pokemon:
        formatted += ' @ [sp={0}]'.format(pokemon['item'])

    if 'special_moves' in pokemon:
        if len(pokemon['special_moves']) == 1:
            formatted += ' (Knows {0})'.format(pokemon['special_moves'][0])
        elif len(pokemon['special_moves']) == 2:
            formatted += ' (Knows {0} and {1})'.format(pokemon['special_moves'][0], pokemon['special_moves'][1])
        else:
            formatted += ' (Knows {0} and {1})'.format(', '.join(pokemon['special_moves'][0:-1]), pokemon['special_moves'][-1])

    return formatted

def check_evolution(pokemon, line, final_level, location, trade=False, item=None):
    '''
        Given a pokemon and some information about its potential evolutions and current situation, determine whether it meets the requirements for any of its evolutions.

        Note that if it fulfills the requirements for *multiple* evolutions at once (extremely rare if not impossible), it will evolve to the one farthest along in the evolution line, regardless if that's actually the "terminal" one (in terms of branching evolutions).

        TODO: Contest-stat-based evolutions, whenever they actually become a thing that can happen.

        TODO: Currently broken because of species/identifier mismatch!!!
    '''

    # Because of the multitudinous ways
    # of representing sex in profiles (f or m or male or female, etc.)
    # convert to a code for easy comparison
    if 'f' in pokemon['sex']:
        coded_sex = 1
    elif 'm' in pokemon['sex']:
        coded_sex = 2
    else:
        coded_sex = 0

    # Iterate through each evo, checking reqs...
    print pokemon

    potential_evos = []

    for mon in line:
        # Okay, first of all it has to evolve
        # *from our current species*
        if mon[5] == pokemon['species']:
            # Get the gender requirement out of the way now
            if (mon[13] == coded_sex) or not mon[13]:
                # Check additional requirements
                if mon[10] == 1:
                    # Level-up evolution
                    if pokemon['level'] >= mon[11]:
                        # Okay, we got the level
                        # But what about any other other requirements?
                        if mon[6] and pokemon['item'].lower().replace(' ', '') == mon[6]:
                            # Fulfilled item evolution
                            potential_evos.append(mon)
                        elif mon[17] and pokemon['happy'] >= mon[16]:
                            # Fulfilled happiness evolution
                            potential_evos.append(mon)
                        elif mon[7] and mon[7] in pokemon['moves']:
                            # Fulfilled level-with-move evolution
                            potential_evos.append(mon)
                        elif mon[14] and location == mon[14]:
                            # Fulfilled location-based evolution
                            potential_evos.append(mon)
                        elif not mon[6] and not any(mon[14:]):
                            # Just vanilla leveling! We're good.
                            potential_evos.append(mon)

                elif mon[10] == 2:
                    # Trade evo
                    if trade:
                        # Check trade-with-item requirement
                        if mon[6] and pokemon['item'].lower().replace(' ', '') == mon[6]:
                            potential_evos.append(mon)
                        elif not mon[6]:
                            # No item required, so trade auto-triggers evo
                            potential_evos.append(mon)
                elif mon[10] == 3:
                    # A use-item evolution (typically stone)
                    if item and mon[6] == item:
                        potential_evos.append(mon)
                else:
                    # Hello, shedinja... _my worst enemy_!!
                    # On the upstream end, we're going to need to
                    # fluff this as the player "using" a pokéball
                    # on the nincada in order to get shedinja
                    if pokemon['species'] == 'nincada' and pokemon['level'] >= mon[11]:
                        # Okay, it fulfills the basic requirements
                        # Is a pokéball being used?
                        balls = ['Master Ball', 'Ultra Ball', 'Great Ball', 'Poké Ball', 'Safari Ball', 'Net Ball', 'Dive Ball', 'Nest Ball', 'Repeat Ball', 'Timer Ball', 'Luxury Ball', 'Premier Ball', 'Dusk Ball', 'Heal Ball', 'Quick Ball', 'Cherish Ball', 'Smoke Ball', 'Light Ball', 'Iron Ball', 'Lure Ball', 'Level Ball', 'Moon Ball', 'Heavy Ball', 'Fast Ball', 'Friend Ball', 'Love Ball', 'Park Ball', 'Sport Ball', 'Air Balloon', 'Dream Ball', 'Hover Ball', 'Coach Ball', 'Curve Ball', 'Faraday Ball', 'Fortune Ball', 'Pursuit Ball', 'Refresh Ball', 'Solar Ball', 'Specter Ball']

                        if item in balls:
                            potential_evos.append(mon)

    if len(potential_evos) > 1:
        # Well, crap
        # There's no way for us to distinguish
        # between multiple possible evolutions
        # So, return nada and let upstream deal with it
        return False
    else:
        return potential_evos[0]

def edit_profile(player):
    '''
        Given a player's data, generate the three posts that will translate that data into an orthodox profile that can be written into the appropriate thread.

        Posts are returned as a tuple of (huge) strings: ('Player Info', 'Player's Team', 'Global Plaza').

    '''

    player_info = "[size=5]Player Info[/size]\n\n[b]Name:[/b] {0}\n[b]Age:[/b] {1}\n[b]Gender:[/b] {2}\n\n[size=4]Inventory[/size]\n\n[b]Money:[/b] {3}P\n[b]Badges[/b]\nNew Logora: {4}\n\n[b]Key Items:[/b] {5}\n[b]General Items:[/b] {6}\n[b]Poké Balls:[/b] {7}\n[b]TMs/HMs:[/b] {8}\n\n[size=4]Trainer Details[/size]\n\n[b]Pokédex Type:[/b] New Logora\n\n---\n\n[size=5]Rival Info[/size]\n\n[b]Name:[/b] {9}\n[b]Age:[/b] {10}\n[b]Gender:[/b] {11}\n\n[size=4]Rival's Team[/size]\n\n{12}".format(player['name'], player['age'], player['gender'], player['money'], player['badge_literal'], format_profile_items(player['key_items']), format_profile_items(player['items']), format_profile_items(player['balls']), format_profile_items(player['machines']), player['rival_name'], player['rival_age'], player['rival_gender'], format_profile_pokemon(player['rival_team']))

    player_team_data = format_profile_pokemon(player['team'])

    player_team = "[size=5]Player's Team[/size]\n\n{0}".format(player_team_data)

    global_plaza_data = 'Nothing'

    global_plaza = "[size=5]Global Plaza[/size]\n\n{0}\n\n[size=5]PC: Box 1[/size]\n\n{1}".format(global_plaza_data, format_profile_pokemon(player['pc']))

    return (player_info, player_team, global_plaza)

def format_profile_items(item_dict):
    '''
        Takes a dictionary of item:quantity pairs and returns a string formatted for display in a player's profile.
    '''
    
    if not item_dict:
        return 'None yet'
    else:
        if type(item_dict) != type([]):
            return ', '.join(['[sp={0}] ({1})'.format(item, item_dict[item]) for item in item_dict])
        else:
            # TM/HM list!
            return ', '.join(['[sp={0}] {1}'.format(item[0], item[1]) for item in item_dict])

def format_profile_pokemon(pokemon_dict):
    '''
        Takes a dictionary of mon:data dictionaries and returns a string formatted for display in a player's profile.
    '''

    if not pokemon_dict:
        return 'None yet'
    else:
        indv_mon = []

        try:
            # Player pokemon
            for mon in pokemon_dict:
                if 'item' in pokemon_dict[mon] and pokemon_dict[mon]['item']:
                    item_string = '[sp={0}]'.format(pokemon_dict[mon]['item'])
                else:
                    item_string = 'None'

                if 'happy' in pokemon_dict[mon]:
                    happy_string = '\n[happiness={0}]'.format(mon['happy'])
                else:
                    happy_string = ''

                mon_string = '[{0}] [sp={1}] [sp={2}] {3}/{3} [{4}] @ {5}\n[b]Nickname:[/b] {6}\n[b]OT:[/b] {7}\n[b]Moves:[/b] {8}\n[exp={9}]{10}'.format(pokemon_dict[mon]['level'], pokemon_dict[mon]['sex'], pokemon_dict[mon]['identifier'], pokemon_dict[mon]['max'], pokemon_dict[mon]['ability'], item_string, pokemon_dict[mon]['name'], pokemon_dict[mon]['ot'], pokemon_dict[mon]['moves_literal'], pokemon_dict[mon]['exp'], happy_string)
                indv_mon.append(mon_string)

        except TypeError:
            # Rival pokemon
            for mon in pokemon_dict:
                mon_string = '[sp={0}] [sp={1}] [{2}]'.format(mon['sex'], mon['identifier'], mon['ability'])
                indv_mon.append(mon_string)
             
        return '\n\n'.join(indv_mon)

def generate_pokemon(cursor, pokemon, level, forme='', special_moves=0, get_item=False, get_shiny=False):
    '''
        This function performs the actual gruntwork of creating a pokémon.
        It chooses an appropriate sex, ability, and if applicable,
        special moves for a pokémon. It also calculates the pokémon's HP.
        In the end it returns a dict containing everything necessary
        to display the pokémon: name, identifier (for sprite generation),
        sex, ability, and if requested, any special moves it might have.

        If get_item is True, the pokémon's wild held item will
        also be rolled for.

        This function requires that a valid database connection be passed.

        TODO: Optimize this. Quite slow, relatively speaking, when you need multiple mon.

    '''

    import random

    final_mon = {}

    final_mon['level'] = int(level)
    final_mon['shiny'] = False

    pokemon_data = None

    # Get all the info we'll need for this 'mon
    monquery = cursor.execute('''select pokemon.name, pokemon.identifier, pokemon.gender_ratio, pokemon.gender_difference, pokemon.hp, abilities.name from pokemon left join pokemon_abilities on pokemon.id = pokemon_abilities.pokemon_id left join abilities on pokemon_abilities.ability_id = abilities.id where pokemon_abilities.is_dream = 0 and pokemon.id=?''', (pokemon,))
    raw_data = monquery.fetchall()

    # If applicable, roll for shininess
    # Shiny chance is 1/500
    if get_shiny:
        shiny_roll = random.randint(1, 500)

        if shiny_roll == 500:
            # Oooh, so ~shiny~
            final_mon['shiny'] = True

    # "Roll for ability"
    # In effect choose a return string at random
    if len(raw_data) > 1:
        pokemon_data = random.choice(raw_data)
    else:
        pokemon_data = raw_data[0]

    if forme:
        final_mon['name'] = pokemon_data[0] + '-' + forme
    else:
        final_mon['name'] = pokemon_data[0]

    final_mon['ability'] = pokemon_data[5]

    # Determine sex
    sex_ratio = float(pokemon_data[2])

    if sex_ratio == -1:
        # Genderless...
        final_mon['sex'] = 'na'

    else:
        # We gotta roll for it
        cutoff = sex_ratio / 8
        sex_roll = random.random()

        if sex_roll < cutoff:
            # It's female!
            final_mon['sex'] = 'f'
        else:
            # It's male!
            # Why is the sprite designator
            # not 'm' for this wtf
            final_mon['sex'] = 'male'

    # Now that we know the sex
    # we can build the identifier!
    identifier = pokemon_data[1]

    if pokemon_data[3]:
        # There's a gender difference!
        identifier = identifier + final_mon['sex'][0]

    if forme and forme == '?':
        # Punctuation unown have special forme designators...
        identifier = identifier + 'qm'
    elif forme and forme == '!':
        identifier = identifier + 'em'
    elif forme:
        identifier = identifier + forme.lower()

    if final_mon['shiny']:
        identifier = identifier + 'shiny'

    final_mon['identifier'] = identifier

    final_mon['hp'] = calculate_hp(pokemon_data[4], final_mon['level'])

    # If applicable, roll for wild held item
    if get_item:
        itemquery = cursor.execute('''select items.name, pokemon_items.item_rarity from pokemon_items left join items on pokemon_items.item_id = items.id where pokemon_items.pokemon_id=?''', (pokemon,))

        itemlist = cursor.fetchall()

        if len(itemlist) == 1:
            # Just roll for the single item
            itemroll = random.randint(1, 100)

            if itemroll <= itemlist[0][1]:
                # It has the item!
                # Format it for display
                item = itemlist[0][0].lower().replace(' ', '')
                final_mon['item'] = item

        elif itemlist:
            # More than one item possible...
            # Roll for it in our usual, terrible way
            potential_items = []

            for item in itemlist:
                potential_items = potential_items + [item[0]] * item[1]

            potential_items = potential_items + [''] * (100 - len(potential_items))

            # Now pick one at random
            potential_item = random.choice(potential_items)

            # If we actually got something, add it!
            if potential_item:
                item = potential_item.lower().replace(' ', '')
                final_mon['item'] = item

    # If applicable, roll for special moves
    if special_moves:
        # If it's smeargle, we instead pick from *all* moves
        if pokemon == 112:
            movequery = cursor.execute('''select name from moves''')
        else:
        # Makes sure that special moves chosen aren't also level-up moves
        # 'cause that's lame
        # And also that all special moves have an equal chance of being selected
            movequery = cursor.execute('''select distinct moves.name from pokemon_moves left join moves on pokemon_moves.move_id = moves.id where pokemon_moves.move_method_id != 1 and pokemon_moves.pokemon_id = ? and pokemon_moves.move_id not in(select distinct move_id from pokemon_moves where move_method_id = 1 and pokemon_id = ?)''', (pokemon, pokemon))

        movelist = movequery.fetchall()

        if len(movelist) == 0:
            # Welp, guess this is smeargle or something
            pass
        elif len(movelist) <= special_moves:
            # Not many to choose from... and we want to
            # return them all!
            final_mon['special_moves'] = [move[0] for move in movelist]
        else:
            # Pick some moves at random!
            final_mon['special_moves'] = [move[0] for move in random.sample(movelist, special_moves)]

    return final_mon

def get_level_up_moves(cursor, mon, current, target):
    '''
        Given a pokemon species id, its current level, and its final level, return a list of the moves (as fulltext strings, not id's or identifiers) it learns over that level span.

        Requires a valid database connection.
    '''

    cursor.execute('''select moves.name from pokemon_moves left join moves on pokemon_moves.move_id = moves.id where pokemon_moves.pokemon_id = ? and pokemon_moves.level_learned > ? and pokemon_moves.level_learned <= ?''', (mon, current, target))

    moves_learned = cursor.fetchall()

    return moves_learned

def get_location(posttree, db, area=''):
    '''
        Given an lxml-converted post tree and, possibly, an area declaration (from the command), determine the relevant location area id.
    '''

    import sqlite3

    # Get the name of the location from the nav bar
    navbits = posttree.xpath('id("content")/div[@class="navigation"]/a/@href')
    loc_raw = navbits[-1]
    fid = loc_raw[loc_raw.rfind('=') + 1:]

    # Set up the database connection
    con = sqlite3.connect(db)
    cursor = con.cursor()

    cursor.execute('''select location_areas.id, location_areas.name from locations left join location_areas on locations.id = location_areas.location_id where locations.fid=?''', (fid,))

    sub_areas = {sub[1]: sub[0] for sub in cursor.fetchall()}

    if not sub_areas:
            print 'No location found for the given thread!'
            return False

    if len(sub_areas) > 1:
        if area and area in sub_areas:
            loc = sub_areas[area]
        elif not area:
            print 'This location requires that a sub-location be specified!'
            return False
        else:
            print 'No sub-area found matching the given sub-area!'
            return False
    else:
        loc = sub_areas.values()[0]

    cursor.close()
    con.close()

    return loc

def parse_profile(profile_tree):
    '''
        Given an etree object representing a player's profile, extract and parse their profile information. Returns a trainer dictionary suitable for manipulation in a varitey of RP-related tasks.
    '''

    import requests
    from lxml import etree
    import re

    # Stand back... I'm gonna try parsing HTML with regex! *groans*
    titlere = re.compile(r"(?P<character>.+)'s? Profile \((?P<user>.+)\)")
    badgere = re.compile(r"New Logora:(.+?)<br />")
    inventoryre = re.compile('<span style="font-weight: bold;">(?P<key>.+):</span>(?P<val>.+)')
    expre = re.compile(r'\d+(?=%)', re.MULTILINE)
    happyre = re.compile(r'\d+(?=<)', re.MULTILINE)

    trainer = {}
    trainer['rival_team'] = []

    # Get profile post id's
    post_ids = profile_tree.xpath('id("posts")/table/@id')
    trainer['profile_ids'] = tuple((int(post[post.rfind('_') + 1:]) for post in post_ids))

    # Get badge info
    m = badgere.search(etree.tostring(profile_tree))
    if m:
        badges = m.group(1)

        if 'None' in badges:
            # No badges!
            trainer['badgecount'] = 0
            trainer['badge_literal'] = 'None yet'

        else:
            trainer['badges'] = len(badges.split(','))
            trainer['badge_literal'] = badges.rstrip('<br />').strip()

    else:
        # Err, well, that's odd.
        # No badge declaration found whatsoever
        # Assume that means no badges...
        trainer['badges'] = 0
        trainer['badge_literal'] = 'None yet'

    # We'll need the name of the topic to identify the trainer
    title = profile_tree.xpath('head/title/text()')
    m = titlere.search(title[0])

    if m:
        trainer['name'] = m.group('character')
        trainer['player'] = m.group('user')

    # Righty-o. Now, the profile contains three posts, each
    # with its own associated info
    posts = profile_tree.xpath('id("posts")/table/tr/td//div[@class="post_body"]')

    # First post is inventory and general info
    inventory = etree.tostring(posts[0]).split('<br />')

    # Try and parse some sense out of this mess
    for bit in inventory:
        match = inventoryre.search(bit)
        if match:
            # Okay! Is this data we care about?
            if match.group('key') == 'Money':
                trainer['money'] = int(match.group('val').strip().rstrip('P'))

            if match.group('key') == 'Age':
                if 'age' not in trainer:
                    trainer['age'] = match.group('val').strip()
                else:
                    trainer['rival_age'] = match.group('val').strip()

            if match.group('key') == 'Gender':
                if 'gender' not in trainer:
                    trainer['gender'] = match.group('val').strip()
                else:
                    trainer['rival_gender'] = match.group('val').strip()

            if match.group('key') == 'Name':
                # Trainer name is determined elsewhere
                # So we want to retain rival name only
                final_name = match.group('val').strip()
                if trainer['name'] != final_name:
                    trainer['rival_name'] = final_name

            # Okay! Only items after this
            elif match.group('key') == 'Key Items':
                if 'img' in match.group('val'):
                    trainer['key_items'] = parse_item_list(match.group('val'))
                else:
                    trainer['key_items'] = ''

            elif match.group('key') == 'General Items':
                if 'img' in match.group('val'):
                    trainer['items'] = parse_item_list(match.group('val'))
                else:
                    trainer['items'] = ''

            elif 'Balls' in match.group('key'):
                if 'img' in match.group('val'):
                    trainer['balls'] = parse_item_list(match.group('val'))
                else:
                    trainer['balls'] = ''

            elif 'TMs/HMs' in match.group('key'):
                # TODO: implement correctly
                # once somebody has one or more
                # so we can actually test this
                if 'img' in match.group('val'):
                    trainer['machines'] = parse_machine_list(match.group('val'))
                else:
                    trainer['machines'] = ''

        else:
            if bit.startswith('<img'):
                # Should be a rival's mon!
                trainer['rival_team'].append(parse_mon(bit))

    # Right, now the team
    trainer['team'] = {}
    curr_mon = {}
    i = 0
    team = etree.tostring(posts[1])

    # Split on breaks as per usual
    for bit in team.split('<br />'):
        stripbit = bit.strip()
        #print stripbit
        if stripbit.startswith('['):
            # Okay, we're starting a new pokémon here
            # Write out any old data if we need to
            if curr_mon:
                trainer['team'][i] = curr_mon
                i += 1

            # Now. The first line should be parseable using
            # the standard mon-parsing protocol
            curr_mon = parse_mon(bit)
        elif stripbit.startswith('<div'):
            # Iiiit's EXP and/or happiness!
            # These get returned as a full block,
            # so we need to do multiline re's!
            exp = expre.search(stripbit)

            # ...orrrr happiness!
            happy = happyre.search(stripbit)

            if exp:
                curr_mon['exp'] = int(exp.group(0))
            if happy:
                curr_mon['happy'] = int(happy.group(0))
        else:
            # It's a key/value pair of some kind
            m = inventoryre.search(bit)

            if m:
                
                if m.group('key') == 'Nickname':
                    curr_mon['name'] = m.group('val').strip()

                elif m.group('key') == 'OT':
                    # Store the OT, and also mark
                    # whether the 'mon is owned or not
                    # (matters for experience gain)
                    curr_mon['ot'] = m.group('val').strip()
                    if '({0})'.format(trainer['player']) not in curr_mon['ot']:
                        curr_mon['traded'] = 1
                    else:
                        curr_mon['traded'] = 0

                elif m.group('key') == 'Moves':
                    # Get a tuple of possible moves
                    # Store both literal and stripped moves
                    # for ease of use/ease of writing
                    curr_mon['moves_literal'] = m.group('val').strip()
                    curr_mon['moves'] = tuple([move.strip(' []') for move in m.group('val').split(', ')])

    # Get the last 'mon on there
    if curr_mon:
        trainer['team'][i] = curr_mon

    # Third post is global plaza and PC
    # No idea how GP is gonna be formatted, so skip for now
    trainer['pc'] = {}
    curr_pc = {}
    j = 0
    pc = etree.tostring(posts[2]).split('<br />')

    # The pokémon accumulation is going to be
    # essentially the same as for team parsing
    # TODO: make this a function

    for bit in pc:
        stripbit = bit.strip()
        if stripbit.startswith('['):
            # Okay, we're starting a new pokémon here
            # Write out any old data if we need to
            if curr_pc:
                trainer['pc'][j] = curr_mon
                j += 1

            # Now. The first line should be parseable using
            # the standard mon-parsing protocol
            curr_pc = parse_mon(bit)
        elif stripbit.startswith('<div'):
            # Iiiit's EXP and/or happiness!
            # These get returned as a full block,
            # so we need to do multiline re's!
            exp = expre.search(stripbit)

            # ...orrrr happiness!
            happy = happyre.search(stripbit)

            if exp:
                curr_pc['exp'] = int(exp.group(0))
            if happy:
                curr_pc['happy'] = int(happy.group(0))
        else:
            # It's a key/value pair of some kind
            m = inventoryre.search(bit)

            if m:
                
                if m.group('key') == 'Nickname':
                    curr_pc['name'] = m.group('val').strip()

                elif m.group('key') == 'OT':
                    # Store the OT, and also mark
                    # whether the 'mon is owned or not
                    # (matters for experience gain)
                    curr_pc['ot'] = m.group('val').strip()
                    if '({0})'.format(trainer['player']) not in curr_pc['ot']:
                        curr_pc['traded'] = 1
                    else:
                        curr_pc['traded'] = 0

                elif m.group('key') == 'Moves':
                    # Get a tuple of possible moves
                    # For PC 'mon, we only care about literal moves
                    curr_pc['moves_literal'] = m.group('val').strip()

    # Get the last 'mon on there
    if curr_pc:
        trainer['pc'][j] = curr_pc

    return trainer

def get_random_sql(cursor, fields, table, number, condition=None):
    '''
        Wrapper function for generating some number of entries, at random, from a SQLite table.

        TODO: Go to SQLAlchemy so it's possible (or rather, easy) to construct injection-proof queries with arbitrary conditions
    '''

    # Table names etc. can't be parameterized, so build query like so...
    if not condition:
        query = 'select {0} from {1} order by random() limit ?'.format(fields, table)
    else:
        query = 'select {0} from {1} where {2} order by random() limit ?'.format(fields, table, condition)

    result = cursor.execute(query, (number,))

    return result.fetchall()

def get_trainer(team, turquoise_db, trainer_db):
    '''
        Takes a player team and generates an appropriate trainer battle for it.

        TODO:
            - Teams matching trainer types (i.e. swimmers specializing in water-types
            - Trainer types appearing in particular areas (i.e. swimmers appearing on water routes)
    '''
    import sqlite3
    import random

    # Connect to the db
    con = sqlite3.connect(trainer_db)
    cursor = con.cursor()

    # First, random trainer class and name
    # Flip a coin: female trainer or male?
    sex = random.random()

    # Get a name of the appropriate gender
    # These "random" SQL selections are kinda slow...
    # But the number of rows is small enough that I believe it's negligible?
    # TODO: Optimize if necessary
    if sex < 0.5:
        raw_name = get_random_sql(cursor, 'name', 'names', 1, 'sex = 0')
        name = raw_name[0][0]

    else:
        raw_name = get_random_sql(cursor, 'name', 'names', 1, 'sex = 1')
        name = raw_name[0][0]

    # Get a trainer class of the appropriate gender
    if sex < 0.5:
        raw_class = get_random_sql(cursor, 'class', 'classes', 1, 'sex < 1')
        clean_class = raw_class[0]

    else:
        raw_class = get_random_sql(cursor, 'class, sex', 'classes', 1, 'sex >= 1')
        clean_class = raw_class[0]

    # Now get the trainer sprite identifier
    trainer_identifier = clean_class[0].replace(' ', '-').lower()

    if sex > 0.5 and int(clean_class[1]) == 1:
        trainer_identifier += '-f'

    # We're done with trainer stuffs, so close the db connection
    cursor.close()
    con.close()

    # Player's levels are important for various things
    team_levels = [team[member]['level'] for member in team.keys()]
    avg_level = sum(team_levels) / len(team_levels)

    # Right, now we want to choose a number of 'mons for our trainer
    # Want to average two 'mons, with more variation for people
    # with higher-level pogeys
    num = int(random.gauss(3, (avg_level / 30) + 1))

    if num < 1:
        num = 1
    elif num > 6:
        num = 6

    # Choose a level for each 'mon
    # Use mean level, variance of player's team
    # to determine a fair level range for the opponent
    team_levels = [team[member]['level'] for member in team.keys()]
    mu = avg_level
    sq_deviation = sum([(level - mu) ** 2. for level in team_levels])
    sigma = (sq_deviation / len(team_levels)) ** (1./2)

    # Now, we want to bias the level selection to handicap opponents
    # who got more 'mon than the player, and give them a boost if they
    # got fewer. For now, going with a penalty of 0.5 levels in each direction,
    # per pokémon under/over the player's number.
    mu = mu + int((len(team_levels) - num) * 1)

    # Mmm, torn as to whether it makes more sense to use a discrete
    # distribution here, or simply round the normal random numbers (what I'm doing now)
    # The binomial seems like the one to use if I went discrete...
    # TODO: Figure out if I want to use a discrete pdf here
    opponent_levels = [max((round(random.gauss(mu, sigma)), 2)) for i in xrange(num)]
    opponent_levels.sort()

    # Time to start working with pokémon!
    # Ooopen the db...
    mon_con = sqlite3.connect(turquoise_db)
    mon_cursor = mon_con.cursor()

    # Select EVOLUTIONARY LINES and determine which form is going to actually appear
    # First, there are some lines we don't want to see for low-level players
    # High BP mons, steel-types, the like
    # The likelihood of being restricted to seeing "friendly" mons
    # therefore depends on average team level...
    friendly_roll = random.random()

    if avg_level >= 15 and friendly_roll < (avg_level / 100.):
        friendly_bool = False
    else:
        friendly_bool = True

    # Then, decide whether we'll accept prohibited lines (10% chance)
    prohibited_roll = random.random()
    
    if prohibited_roll < 0.05 and not friendly_bool:
        # Ooh, might get a prohibited line this time! The intrigue!
        lines = get_random_sql(mon_cursor, 'id, evo_prohibited', 'pokemon_evolution_chains', num)

    elif prohibited_roll < 0.05 and friendly_bool:
        # Prohibited possible, unfriendly not
        lines = get_random_sql(mon_cursor, 'id, evo_prohibited', 'pokemon_evolution_chains', num, 'unfriendly is null')

    elif prohibited_roll >= 0.05 and not friendly_bool:
        # Prohibited impossible, unfriendly not
        lines = get_random_sql(mon_cursor, 'id, evo_prohibited', 'pokemon_evolution_chains', num, 'prohibited is null')

    else:
        # Nothing rare or difficult at all!
        lines = get_random_sql(mon_cursor, 'id, evo_prohibited', 'pokemon_evolution_chains', num, 'prohibited is null and unfriendly is null')
    #print lines

    final_team = []

    # Mmmrnf.
    # Certainly feels like some unnecessary queries here.
    # TODO: Optimize this?
    for index, line in enumerate(lines):
        raw_evos = mon_cursor.execute('''select pokemon.id, pokemon.evolves_from_pokemon_id, evolution.minimum_level from pokemon left join pokemon_evolution as evolution on pokemon.id = evolution.evolved_species_id where pokemon.evolution_chain_id=?''', (line[0],))
        evos = raw_evos.fetchall()

        forme = ''

        # TODO: Scale chance of non-level evolution by target level
        # (instead of just setting an absolute cutoff, i.e. lv 35)
        if len(evos) == 1:
            # No bothering about which to choose here!
            evo = evos[0][0]

            # If this is an unown, though, we need to choose its forme
            if evo == 173:

                from string import uppercase
                unown_formes = uppercase + '?!'
                forme = random.choice(unown_formes)

        # The mareep line is unusual among split-evo lines
        # It's ugly, but it's easiest to simply address it
        # individually right now
        elif line[0] == 41:
            # Is it high-enough leveled to be a flaafy?
            if opponent_levels[index] >= evos[1][2]:
                # Keep checking further requirements
                # Is it high-leveled enough to be an ampharos?
                if opponent_levels[index] >= evos[2][2]:
                    # Yup... but does it fulfill the requirements
                    # for a ramfere?
                    restricted_roll = random.random()

                    if opponent_levels[index] >= 35 and (random.random() < float(opponent_levels[index] - 35)/15 or opponent_levels[index] > 50) and restricted_roll <= 0.05:
                        evo = evos[3][0]
                    else:
                        # Make it an ampharos
                        evo = evos[2][0]
                else:
                    # Guess it's a flaaffy, then.
                    evo = evos[1][0]
            else:
                # Easy enough. Let's call it mareep!
                evo = evos[0][0]

        # Look for split-evolution lines
        # My *very best friends* >>
        # If they occur, you'll have a non-unique "evolves from mon"
        # Making the set() of "evolves from mon" smaller than the tuple()
        # "evolves from mon"
        elif len(set((item[1] for item in evos))) != len(tuple((item[1] for item in evos))):
            # This is a split evolution!
            # Look at each evo's requirements,
            # including any it qualifies for.
            final_evos = []

            for mon in evos[1:]:
                if mon[2] and opponent_levels[index] >= mon[2]:
                    # Passed this level evolution
                    # Roll for restricted evolution
                    if lines[index][1]:
                        # Roll for that...
                        restricted_roll = random.random()
                        #print restricted_roll
                        if restricted_roll <= 0.1:
                            final_evos.append(mon)

                    else:
                        final_evos.append(mon)

                elif not mon[2] and opponent_levels[index] >= 35:
                    if opponent_levels[index] < 50:
                        # Need to roll for whether this
                        # even goes through
                        evo_roll = random.random()
                        #print 'evo requirement', float(opponent_levels[index] - 35) / 15
                        #print 'evo_roll', evo_roll
                        if evo_roll > float(opponent_levels[index] - 35)/15:
                            break
                    # Passed "other" restriction
                    # Roll for restricted evolution
                    if lines[index][1]:
                        # Roll for that...
                        restricted_roll = random.random()
                        #print 'restricted roll', restricted_roll
                        if restricted_roll <= 0.05:
                            final_evos.append(mon)

                    else:
                        final_evos.append(mon)

            # Now actually chose the evolution
            if not len(final_evos):
                # Nothing passed... return the base form
                evo = evos[0][0]
            elif len(final_evos) == 1:
                # No need to be picky here...
                evo = final_evos[0][0]
            else:
                # Annnd now we need to randomize
                evo = random.choice(final_evos)[0]

        else:
            # Ah... determine the evolution suitable for this level
            # First, extract the "level conditions" on this line

            evo_conditions = [mon[2] for mon in evos]
            species_choice = 0

            for k, level in enumerate(evo_conditions[1:]):
                # If the 'mon is lower than the level
                # required for evolution, stop here and choose
                # that as our species
                if level and opponent_levels[index] < level:
                    break
                elif not level:
                    # Ah... evolves by something other
                    # than level
                    # For now, call that a level req of 35
                    if opponent_levels[index] <= 35:
                        break
                    else:
                        # Roll to see whether we evolve at all
                        if opponent_levels[index] < 50:
                            # Need to roll for whether this
                            # even goes through
                            evo_roll = random.random()
                            #print 'evo requirement', float(opponent_levels[index] - 35) / 15
                            #print 'evo_roll', evo_roll
                            if evo_roll > float(opponent_levels[index] - 35)/15:
                                break
                        if lines[index][1]:
                            # Restricted evolution.
                            # Roll for it.
                            restricted_roll = random.random()
                            #print restricted_roll
                            if restricted_roll > 0.1:
                                break

                        species_choice = k + 1
                else:
                    # We meet the requirements
                    # Carry on
                    species_choice = k + 1

            evo = evos[species_choice][0]

        #print evo_conditions
        #print evo

        # Roll for opponents' special moves
        # Higher-level 'mon are more likely to have more/any special moves
        # Was originally a nice draw from a binomial distribution, but
        # that requires numpy, and I don't want to require numpy just
        # for this. This is more clunky (and potentially computationally
        # expensive), but it works.
        # Currently set to 0.03% chance of a move per level.
        # Smeargle always gets a fixed number of special moves
        if evo == 112:
            special_moves = ((int(opponent_levels[index]) - 1) / 10) + 1
        elif avg_level >= 30:
            special_moves = sum([1 for i in xrange(int(opponent_levels[index])) if random.random() <= 0.003])
        else:
            special_moves = 0


        # TODO: Items for pokémon?? The problem is that
        # few are going to be general-use enough to stick on entirely
        # at random...

        # And now... actually generate the pokémon!
        final_team.append(generate_pokemon(mon_cursor, evo, opponent_levels[index], forme, special_moves))

    #print final_team

    mon_cursor.close()
    mon_con.close()

    trainer_output = u'{0} {1} would like to battle!\n[img]http://thousandroads.net/stuff/turquoise/images/{2}.png[/img]\n{3} sent out {4}!\n{5}'.format(clean_class[0], name, trainer_identifier, name, final_team[0]['name'], format_pokemon(final_team[0]))
    if len(final_team) > 1:
        trainer_output += '\n[spoiler=For Later]\n{0}\n[/spoiler]'.format('\n'.join(format_pokemon(mon) for mon in final_team[1:]))
    #print opponent_levels
    #print lines

    return trainer_output

def get_wild(team, location, db):
    '''
        Takes a trainer's data, a target location, and a database connection, and generates an appropriate wild encounter for them.
    '''

    import random
    import sqlite3
    from itertools import chain

    # DB connection
    con = sqlite3.connect(db)
    cursor = con.cursor()

    # Get highest-level pogey on trainer's team
    maxlev = max([team[mon]['level'] for mon in team])
    
    # Wild level; should usually be between 1 and 7
    # levels below maximum
    wild_level = round(random.gauss(maxlev - 4, 3))

    # If more than 5 levels above max level, reset
    # to max level + 5
    # Wild min level is 2, also
    if wild_level - maxlev > 5:
        wild_level = maxlev + 5
    elif wild_level < 2:
        wild_level = 2

    # Now actually choose the pokémon that appears
    slotquery = cursor.execute('''select encounter.slot_id, pokemon.id, encounter.formes, slot.rarity, encounter.min_level, encounter.max_level from location_area_slots as slot left join location_area_slot_encounters as encounter on slot.id = encounter.slot_id left join pokemon on encounter.pokemon_id = pokemon.id where slot.location_area_id = ? and encounter.min_level <= ? and encounter.max_level >= ?''', (location, wild_level, wild_level))

    #print wild_level
    slots = slotquery.fetchall()
    #print slots
    #print sum((slot[3] for slot in slots))
    if sum((slot[3] for slot in slots)) < 100:
        # Whuh-oh, there are either lower-than-max
        # slots here, or higher-than-min slots
        # Check for both
        # Thing is, there may be slots that fit the requirement
        # of simply being higher- or lower-leveled than us,
        # without us actually fitting into that range
        # So we need to exclude slots we've already seen
        slot_ids = [slot[0] for slot in slots]

        minquery = cursor.execute('''select encounter.slot_id, pokemon.id, encounter.formes, slot.rarity, encounter.min_level, encounter.max_level from location_area_slots as slot left join location_area_slot_encounters as encounter on slot.id = encounter.slot_id left join pokemon on encounter.pokemon_id = pokemon.id where slot.location_area_id = ? and encounter.min_level > ? and encounter.slot_id not in({0})'''.format(','.join('?' * len(slot_ids))), [location] + [wild_level] + slot_ids)
        minslots = minquery.fetchall()

        # So this is suuuper dumb, but apparently SQLite doesn't
        # offer the functionality necessary to do this easily
        # in bare SQL ('partition by')
        # So instead, we'll have to filter to the
        # minimum level possible for each slot at this stage
        if minquery:
            mindict = {}

            for slot in minslots:
                if slot[0] not in mindict:
                    mindict[slot[0]] = slot
                elif mindict[slot[0]][4] > slot[4]:
                    mindict[slot[0]] = slot

            final_minslots = [mindict[slot] for slot in mindict]
            #print 'min', final_minslots

            slots = slots + final_minslots

        maxquery = cursor.execute('''select encounter.slot_id, pokemon.id, encounter.formes, slot.rarity, encounter.min_level, encounter.max_level from location_area_slots as slot left join location_area_slot_encounters as encounter on slot.id = encounter.slot_id left join pokemon on encounter.pokemon_id = pokemon.id where slot.location_area_id = ? and encounter.max_level < ? and encounter.slot_id not in({0})'''.format(','.join('?' * len(slot_ids))), [location] + [wild_level] + slot_ids)

        maxslots = maxquery.fetchall()

        #print 'max', maxslots

        if maxquery:
            slots = slots + maxslots

    # If things still don't add up to 100, freak out
    if sum((slot[3] for slot in slots)) < 100:
        print slots
        print 'Error: Wild encounter chances do not add to 100 for this route.'
        return False

    # Transform our slots into a dict
    slot_dict = {slot[0]: {'pokemon': slot[1], 'forme': slot[2], 'rarity': slot[3], 'min': slot[4], 'max': slot[5]} for slot in slots if slot[1] not in slots}

    # Pick a pokemon...
    # Set up a list with number of entries for any pokemon
    # equal to its rarity, then choose one at random
    mondist = list(chain.from_iterable([[key] * slot_dict[key]['rarity'] for key in slot_dict]))
    mon_choice = random.choice(mondist) 
    #print mondist
    #print mon_choice

    # Now adjust the level, if our chosen 'mon
    # falls outside the acceptable level range for this route
    if wild_level < slot_dict[mon_choice]['min']:
        wild_level = slot_dict[mon_choice]['min']
    elif wild_level > slot_dict[mon_choice]['max']:
        wild_level = slot_dict[mon_choice]['max']

    # Now all we need to do is get the display info!
    if slot_dict[mon_choice]['forme']:
        # We need to choose a forme!
        formes = slot_dict[mon_choice]['forme'].split(', ')
        final_forme = random.choice(formes)
    else:
        final_forme = ''

    final_mon = generate_pokemon(cursor, slot_dict[mon_choice]['pokemon'], wild_level, forme=final_forme, get_item=True, get_shiny=True)

    formatted_mon = format_pokemon(final_mon)

    # And print our final message (at least in this version)
    #print 'A wild {0} appeared!\n{1}'.format(final_mon['name'], formatted_mon)

    cursor.close()
    con.close()

    return final_mon

def parse_item_list(string):
    '''
        Take one of those hideous item bylines from a profile and figure out what items/quantities of items are actually hiding there.
    '''
    import re

    final_items = {}
    imgre = re.compile(r'<img src=".*/(?P<identifier>.+)\..*\((?P<quantity>\d+)\)')
    noquantre = re.compile(r'<img src=".*/(?P<identifier>.+)\.')

    # WARNING: big trouble if items aren't separated by commas!
    items =  string.split(',')

    for item in items:
        if '(' in item:
            m = imgre.search(item)

            if m:
                final_items[m.group('identifier')] = int(m.group('quantity'))
        else:
            # No quantity given; so should just be one of whatever this is
            m = noquantre.search(item)

            if m:
                final_items[m.group('identifier')] = 1

    return final_items

def parse_machine_list(string):
    '''
        As above, take a clusterfucky list of TMs/HMs in a trainer's possession and extract the relevant information about them.
    '''
    import re

    machinere = re.compile(r'<.+/(?P<icon>.+)\..+> (?P<name>.+)')

    final_machines = []

    machines = string.split(',')

    for machine in machines:

        m = machinere.search(machine)

        if m:
            final_machines.append((m.group('icon'), m.group('name')))

    return final_machines

def parse_mon(string):
    '''
        Takes a string representing a pokémon, either in a profile or in battle, and extracts its vital statistics.
    '''

    # TODO: make this work for in-battle pokémon at the same time
    import re
    final_mon = {}

    monre = re.compile(r'\[(?P<level>\d+)\].*<img src.+/(?P<sex>.+)\..+<img src.+/(?P<species>.+)\..+ (?P<cur>\d+)/(?P<max>\d+).+\[(?P<ability>.+)\]')
    rivalre = re.compile(r'<img src.+/(?P<sex>.+)\..+<img src.+/(?P<species>.+)\..+\[(?P<ability>.+)\]')
    itemre = re.compile(r'@ (?P<item>.+)')

    m = monre.search(string)

    if m:
        final_mon = {'level': int(m.group('level')), 'sex': m.group('sex'), 'identifier': m.group('species'), 'cur': int(m.group('cur')), 'max': int(m.group('max')), 'ability': m.group('ability')}
    else:
        # Rival pokémon in a profile?
        rivalmon = rivalre.search(string)

        if rivalmon:
            final_mon = {'sex': rivalmon.group('sex'), 'identifier': rivalmon.group('species'), 'ability': rivalmon.group('ability')}

    i = itemre.search(string)

    if i:
        if 'None' not in i.group('item'):
            final_mon['item'] = i.group('item')
        else:
            final_mon['item'] = ''

    return final_mon

def proc_pickup(mon):
    '''
        Given a pokémon with pickup (whose ability has been activated), determine what item gets picked up.
    '''

    import json
    import random

    # Get our data together
    # TODO: this oughta not be hardcoded...
    with open('resource/pickup.json') as f:
        pickup_table = json.load(f)

    gems = [
        'Fire Gem',
        'Water Gem',
        'Electric Gem',
        'Grass Gem',
        'Ice Gem',
        'Fighting Gem',
        'Poison Gem',
        'Ground Gem',
        'Flying Gem',
        'Psychic Gem',
        'Bug Gem',
        'Rock Gem',
        'Ghost Gem',
        'Dark Gem',
        'Steel Gem',
        'Dragon Gem',
        'Normal Gem',
    ]

    # Right. First, we determine what category
    # of item we're getting.
    catroll = random.random()

    if catroll <= 0.02:
        category = 'vrare'
    elif catroll > 0.02 and catroll <= 0.10:
        category = 'rare'
    elif catroll > 0.10 and catroll <= 0.70:
        category = 'uncommon'
    else:
        category = 'common'

    # Now, use the pokémon's level
    # to choose the rarity bracket.
    # lv 100's screw up our little algorithm,
    # so handle them separately
    if mon['level'] == 1:
        bracket = 0
    else:
        bracket = (mon['level'] - 1) / 10

    # Now, choose the actual item...
    if category == 'common':
        # There's only one option, so...
        item = pickup_table[category][str(bracket)]
    else:
        # Pick one at random!
        item = random.choice(pickup_table[category][str(bracket)])

    # One more thing... if this is a
    # type gem, we need to know *what* type!
    if item == 'Type Gem':
        item = random.choice(gems)

    print 'Picked up {0}!'.format(item)
    return item

if __name__ == '__main__':
    '''
        Quick-test of random wild generation.
    '''

    import requests
    from lxml import etree
    from turquoise_utils import get_profile

    html_parser = etree.HTMLParser(encoding='utf-8')

    r = requests.get('http://pokemonturquoise.com/showthread.php?tid=251')
    post_content = etree.fromstring(r.text, html_parser)

    raw_profile = get_profile(post_content, html_parser)
    test_profile = parse_profile(raw_profile)

    print test_profile

    edit_profile(test_profile)

    # TODO: still not sure if this EXP calc is thrashed out!
    # I know you hate testing this thing, but...
    '''
    player_team = {0: {'species': 'spraylet', 'participated': 1, 'traded': 1, 'level': 20, 'item': 'potion'}}
    enemy = {'species': 'caterpie', 'level': 5, 'trained': 1}

    test = calculate_battle_exp(player_team, enemy)
    print test
    '''

    '''
    import sqlite3
    import codecs
    from ConfigParser import SafeConfigParser

    config = SafeConfigParser()

    with codecs.open('turquoise.cfg', 'r', encoding='utf-8') as f:
        config.readfp(f)

    con = sqlite3.connect(config.get('database', 'main_db'))
    cursor = con.cursor()

    #mon = {'moves': ('Tackle', 'Growl', 'Razor Leaf'), 'cur': 29, 'name': 'Tetris', 'level': 7, 'max': 29, 'sex': 'female', 'item': '', 'exp': 88, 'moves_literal': 'Tackle, Growl, Razor Leaf', 'ot': 'Selenia Deboit (Ventus)', 'traded': 0, 'species': 'acafia', 'ability': 'Overgrow'}
    mon = {'moves': ('Tackle', 'Growl', 'Razor Leaf'), 'cur': 29, 'name': 'Tetris', 'level': 7, 'max': 29, 'sex': 'female', 'item': '', 'exp': 88, 'moves_literal': 'Tackle, Growl, Razor Leaf', 'ot': 'Selenia Deboit (Ventus)', 'traded': 0, 'species': 'acafia', 'ability': 'Overgrow'}
    print mon['moves']
    print mon['moves_literal']

    location = 52

    award_exp(mon, 500, location, cursor)

    cursor.close()
    con.close()
    '''
