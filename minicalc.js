#!/usr/bin/env node

var program = require('commander');
var damage = require('./honko_base.js');
var util = require('./util.js');

program
  .version('0.0.1')
  .option('-A, --attack [stage]', 'Attack stage of attacker. Prefix "p" indicates "+", "n" for "-"')
  .option('-D, --defense [stage]', 'Defense stage of defender. Prefix "p" indicates "+", "n" for "-"')
  .option('-S, --special_attack [stage]', 'Special attack stage of attacker. Prefix "p" indicates "+", "n" for "-"')
  .option('-F, --special_defense [stage]', 'Special defense stage of defender. Prefix "p" indicates "+", "n" for "-"')
  .option('-P, --speed [stage]', 'Speed stage of attacker. Prefix "p" indicates "+", "n" for "-"')
  .option('-d, --doubles', 'This is a double battle')
  .option('-i, --item [item]', 'The attacker\'s held item')
  .option('-a, --ability [ability]', 'The attacker\'s ability')
  .option('-t, --reflect', 'Reflect is in play on the target\'s side')
  .option('-l, --light_screen', 'Light screen is in play on the target\'s side')
  .option('-f, --foresight', 'The target is affected by foresight')
  .option('-g, --gravity', 'Gravity is in effect on the field')
  .option('-h, --helping_hand', 'Attack is aided by helping hand')
  .option('-w, --weather', 'Put the given weather into effect')
  .option('-s, --switchin', 'The target is just switching in')
  .option('-c, --critical [stage]', 'Add the specified amount to the crit modifier of the move being used', parseInt)
  .option('-r, --respond [attack]', 'Call another round of battle, with the defender using the specified attack on the original attacker')
  .parse(process.argv);

// Process args 2-6 give us the initial attacker and level
// defender and level, and attack used
var params = process.argv.slice(2)

function stagesetter(flag) {
    // Converts command-line stat stage notation
    // to an actual number
    if (flag.length > 2) {
        // Invalid!
        console.log('Invalid value supplied for stat boost!');
    } else {
        if (flag[0] === 'p') {
            return parseInt(flag[1]);
        } else {
            return -1 * parseInt(flag[1]);
        }
    }
}

var monmaker = function(species, level, flags) {
    this.level = level;
    this.type1 = util.ALL_POKEMON[species]['type1'];
    this.type2 = util.ALL_POKEMON[species]['type2'];
    this.basehp = util.ALL_POKEMON[species]['hp'];
    this.baseat = util.ALL_POKEMON[species]['at'];
    this.basedf = util.ALL_POKEMON[species]['df'];
    this.basesa = util.ALL_POKEMON[species]['sa'];
    this.basesd = util.ALL_POKEMON[species]['sd'];
    this.basesp = util.ALL_POKEMON[species]['spd'];
    this.atm = flags.attack ? stagesetter(flags.attack) : 0;
    this.dfm = flags.defense ? stagesetter(flags.defense) : 0;
    this.sam = flags.special_attack ? stagesetter(flags.special_attack) : 0;
    this.sdm = flags.special_defense ? stagesetter(flags.special_defense) : 0;
    this.spm = flags.speed ? stagesetter(flags.speed) : 0;
    this.hp = util.calcHP(this.basehp, this.level);
    this.at = util.calcStat(this.baseat, this.level);
    this.df = util.calcStat(this.basedf, this.level);
    this.sa = util.calcStat(this.basesa, this.level);
    this.sd = util.calcStat(this.basesd, this.level);
    this.usp = util.calcStat(this.basesp, this.level);
    this.sp = util.calcStat(this.basesp, this.level);
    this.item = flags.item ? flags.item : 'Focus Sash';
    this.ability = flags.ability ? flags.ability : 'Run Away';
    this.w = util.ALL_POKEMON[species]['weight'];
}

/*
var attacker = new monmaker('crocoal', 5);
var defender = new monmaker('crocoal', 5);

console.log(attacker);
console.log(defender);
*/

var movemaker = function(name) {
    this.name = name;
    this.bp = util.ALL_ATTACKS[name][0];
    this.type = util.ALL_ATTACKS[name][1];
    this.physical = util.ALL_ATTACKS[name][2] === 'Physical';
    this.crit = util.ALL_ATTACKS[name][3];
};

// All our battle flags
var flagmaker = function(program) {
    this.weather = program.weather ? program.weather : "(none)";
    this.isDoubles = program.doubles ? true: false;
    this.isReflect = program.reflect ? true: false;
    this.isLightScreen = program.light_screen ? true: false;
    this.isForesight = program.foresight ? true: false;
    this.isGravity = program.gravity ? true: false;
    this.isHelpingHand = program.helping_hand ? true: false;
    this.isSwitchin = program.switchin ? true: false;
    this.crit = program.critical ? parseInt(program.critical) : 0;
    this.isCriticalHit = false;
}

// Roll for crit
// If flagged to negative one, skip
function get_crit (move, crit_arg) {

    // Under normal circumstances, we still have crits!
    var cutoff = 0.0625;
    var crit_roll = Math.random();

    if (crit_arg !== -1 || move.crit !== 0) {
        // If flagged to six, also skip
        // But set isCrit to true
        if (crit_arg === 3 || move.crit == 3) {
            return true;
        } else {

            // Determine final crit stage and roll
            var crit_stage = crit_arg + move.crit;

            // Get cutoff based on crit stage
            switch (crit_stage) {
                case 1:
                    var cutoff = 0.125;
                    break;

                case 2:
                    var cutoff = 0.5;
                    break;

                case 3:
                    var cutoff = 1;
                    break;

                case 4:
                    var cutoff = 1;
                    break;

                case 5:
                    var cutoff = 1;
                    break;

                case 6:
                    var cutoff = 1;
                    break;

            }
        }

        // Finally, determine critical hit
        if (crit_roll <= cutoff) {
                return true;
        } else {
            return false;
        }
    }
}

function do_damage_calc (attacker_name, attacker_level, defender_name, defender_level, attack, flags) {

    // Get our pokémon together
    attacker = new monmaker(attacker_name, attacker_level, flags);
    defender = new monmaker(defender_name, defender_level, flags);

    // The attack...
    move = new movemaker(attack);

    // Set flags...
    flags = new flagmaker(flags);

    // Quick 'n dirty speed check
    // First, calculate final speed (include modifiers)
    // Modifier is assumed to be for the attacker
    attacker.sp = damage.getFinalSpeed(attacker.sp, attacker.spm, attacker.item);
    if (attacker.sp > defender.sp) {
        console.log(attacker_name + ' attacks first!');
    } else if (attacker.sp === defender.sp) {

        // Speed tie!
        tieroll = Math.random();
        if (tieroll <= 0.5) {
            console.log(attacker_name + ' attacks first!');
        } else {
            console.log(defender_name + ' attacks first!');
        }
        
    } else {
        console.log(defender_name + ' attacks first!');
    }

    /*
    console.log(attacker);
    console.log(defender);
    console.log(move);
    console.log(flags);
    */

    // ...annnd look for a crit...
    var isCriticalHit = get_crit(move, flags.crit);

    // ...and finally, get results!
    var results = damage.getAllDamage(attacker, defender, move, flags.weather, flags.isDoubles, flags.isReflect, flags.isLightScreen, flags.isForesight, flags.isGravity, flags.isHelpingHand, flags.isSwitchin, isCriticalHit);

    // Now we need to actually choose which damage result we get
    var rindex = Math.floor(Math.random() * 16);
    var final_damage = results.damage[rindex];

    console.log(results.damage);
    console.log(results.desc);
    console.log(final_damage);
}

do_damage_calc(params[0], params[1], params[3], params[4], params[2], program);

// And if there's a response, send that attack back!
if (program.respond) {

    do_damage_calc(params[3], params[4], params[0], params[1], program.respond, program);
}
