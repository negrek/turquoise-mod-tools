#!/usr/bin/env python
# -*- encoding: utf-8 -*-

'''
    Utility functions associated with interfacing with the board environment--does things like log in, write posts, extract data from posts, read the active threads list, and so on.
'''

def build_url(config, url):
    '''
        Given a configuration parser and a desired url suffix, return a proper full url to the page intended.
    '''

    return str(config.get('board_info', 'url') + url)

def edit_post(session, config, pid, content):
    '''
        Edit the target post to replace its current content with new content.
    '''
    from lxml import etree
    import requests
    
    # GET the thread in order to extract some parameters
    thread = session.get(build_url(config, config.get('board_info', 'edit')), params={'pid': pid})

    html_parser = etree.HTMLParser()
    tree = etree.fromstring(thread.text, html_parser)
    input_form = tree.xpath('id("content")/form[@name="input"]')
    input_params = {key: val for key, val in zip(input_form[0].xpath('input/@name'), input_form[0].xpath('input/@value'))}

    input_params.update({key: val for key, val in zip(input_form[0].xpath('table/tr/td/input/@name'), input_form[0].xpath('table/tr/td/input/@value'))})

    # Now set some options that are a pain to extract and likely
    # won't change anyway
    input_params['postoptions[signature]'] = 1
    #input_params['attachment'] = ''
    input_params['submit'] = 'Update Post'
    input_params['icon'] = -1

    # And the actual post content
    input_params['message'] = content

    # This url should actually be given as a parameter of the function, hmm.
    r = session.post(build_url(config, config.get('board_info', 'edit')), params={'pid': pid, 'processed': 1}, data=input_params)

    return r.text

def get_profile(thread, html_parser):
    '''
        Given an etree object representing an RPG thread and an lxml parser object, extract the profile link for the last poster. Then download their profile and return an etree object containing its data for further parsing.
    '''

    from lxml import etree
    import requests

    posts = thread.xpath('id("posts")/table/@id')

    last_pid = posts[-1][posts[-1].rfind('_') + 1:]

    # Find the profile link
    profile_link = thread.xpath('id("post_{0}")/tr/td//a[@target="_blank" and text()="RPG Profile"]/@href'.format(last_pid))[0]

    profile_response = requests.get(profile_link)
    profile_raw = etree.fromstring(profile_response.text, html_parser)

    return profile_raw

def get_url_param(url, param):
    '''
        Given a url string, parse out the forum id (denoted by ?fid=ID). Return that, or false if no id found.
    '''
    import re

    # Note the need to escape the first backslash in this re
    # Despite the use of a raw string
    # Non-raw string requires three backslashes
    restring = r'\\?{0}=(\d+)'.format(param)
    param_re = re.compile(restring)

    valmatch = param_re.search(url)

    if valmatch:
        val = valmatch.group(1)
        return val
    else:
        return False

def get_search_results(response):
    '''
        Given a response that should contain search results, return links to the items (threads or posts) 
    '''
    from lxml import etree

    html_parser = etree.HTMLParser()
    tree = etree.fromstring(response, html_parser)

def handle_ooc(post, post_id, commands, html_parser):
    '''
        Accepts a post containing at least one OOC command, parses out all commands, and carries out appropriate response(s).
    '''

    # Most commands require a read/write on the poster's player profile
    # So get that.
    trainer = get_profile(post, post_id, html_parser)

    return True

def detect_error(response):
    '''
        Inspects a response object to determine whether a "real" page was loaded, or whether an error was returned.
    '''
    from lxml import etree

    html_parser = etree.HTMLParser()
    tree = etree.fromstring(response, html_parser)
    title = tree.xpath('/html/head/title/text()')
    
    if title[0] == 'Error':
        # onoes an error
        return True
    else:
        return False

def make_post(session, config, tid, post):
    '''
        Make a post in the given thread. Probably I'll later fancy this up so you can change some of the optional parameters (turn off your sig, for example), but for now, mostly about getting the job done.
    '''
    from lxml import etree
    import requests
    
    # GET the thread in order to extract some parameters
    thread = session.get(build_url(config, config.get('board_info', 'reply')), params={'tid': tid})

    html_parser = etree.HTMLParser()
    tree = etree.fromstring(thread.text, html_parser)
    input_form = tree.xpath('id("content")/form[@name="input"]')
    input_params = {key: val for key, val in zip(input_form[0].xpath('input/@name'), input_form[0].xpath('input/@value'))}

    input_params.update({key: val for key, val in zip(input_form[0].xpath('table/tr/td/input/@name'), input_form[0].xpath('table/tr/td/input/@value'))})

    # Now set some options that are a pain to extract and likely
    # won't change anyway
    input_params['postoptions[signature]'] = 1
    input_params['postoptions[subscriptionmethod]'] = ''
    #input_params['attachment'] = ''
    input_params['submit'] = 'Post Reply'
    input_params['icon'] = -1

    # And the actual post content
    input_params['message_new'] = post
    input_params['message'] = post

    # This url should actually be given as a parameter of the function, hmm.
    r = session.post(build_url(config, config.get('board_info', 'reply')), data=input_params)

    return r.text

def show_page(data):
    '''
        Testing function that prints the data returned by a Twisted network request.
    '''

    print data
    return True

def turquoise_login(username='Negrek', password='', cookie_file='turquoise_cookies.txt'):
    '''
        Takes the username and password to use to log in, as well as where to store the login cookie.

        In the future might want to specify whether the login made goes to invisible mode or not, but for now I think I'll just have it set to auto-invisible.
    '''
    import datetime
    import pickle
    import requests, requests.utils

    s = requests.session()

    login_url = 'http://pokemonturquoise.com/member.php'

    post_dict = {
            'action': 'do_login',
            'password': password,
            'url': '',
            'username': username,
            }

    s.post(login_url, data=post_dict)

    # That should get us the login cookie
    # Save it to a file for later
    with open(cookie_file, 'wb') as f:
        pickle.dump(requests.utils.dict_from_cookiejar(s.cookies), f)

    f.close()

    # Now return the session for immediate use
    return s

if __name__ == '__main__':
    '''
       Quick-test of login capabilities; body should
       contain forum contents if login successful,
       error message if not.
    '''
    import codecs
    from ConfigParser import SafeConfigParser

    config = SafeConfigParser()

    with codecs.open('turquoise.cfg', 'r', encoding='utf-8') as f:
        config.readfp(f)

    session = turquoise_login(config.get('account_info', 'username'), config.get('account_info', 'password'))

    #r = session.get('http://pokemonturquoise.com/forumdisplay.php?fid=67')

    postcontent = u'Making sure this still works\n\nSpooky editing at a distance!'

    p = edit_post(session, config, 3424, postcontent)

    print p
