<?php
/**
 * Turquoise Mod Tools Scripts
 * Copyright 2014 Negrek
 *
 * Website: http://thousandroads.net
 *
 * TODO: multipage everything, sort threads by time AND THEN randomly
 */

define("IN_MYBB", 1);
define('THIS_SCRIPT', 'rpgmod.php');

$templatelist = "rpg_updatetable,rpg_updatetable_thread";

require_once "./global.php";

/*
require_once('FirePHPCore/fb.php');
ob_start();
*/

// Load global language phrases
$lang->load("modtools");
$lang->load("forumdisplay");

// Get RPG mod usergroups
// I tried doing this with array_map, but I GUESS NOT
$mod_ids = array();

foreach(explode(',', $mybb->settings['modtools_groups']) as $group)
{
    $mod_ids[] = intval($group);
}

if($mybb->user['uid'] == 0 || $mybb->settings['modtools_enable'] != 1 || !$mybb->user['ismoderator'])
{
	error_no_permission();
}

// Fetch the threads that need updates!
// First, what forums might they reside in?
$forums = array_map("intval", explode(',', $mybb->settings['modtools_forums']));

// Work out pagination, which page we're at, as well as the limits.
$perpage = $mybb->settings['threadsperpage'];
$page = intval($mybb->input['page']);
if($page > 0)
{
    $start = ($page-1) * $perpage;
}
else
{
    $start = 0;
    $page = 1;
}
$end = $start + $perpage;
$lower = $start+1;
$upper = $end;

// Thread counter
$threadcount = 0;

// Because the thread prefix variable isn't necessarily defined...
if ($mybb->settings['updatetable_prefixes'])
{
    $query = $db->query("
        SELECT t.*, p.displaystyle AS threadprefix
        FROM ".TABLE_PREFIX."threads t
        LEFT JOIN ".TABLE_PREFIX."threadprefixes p ON(p.pid=t.prefix)
        WHERE t.fid IN(".$mybb->settings['modtools_forums'].") AND t.uid = t.lastposteruid AND t.prefix NOT IN(".$mybb->settings['updatetable_prefixes'].") and t.sticky = 0
    ORDER BY t.lastpost
    ");
} else {
    $query = $db->query("
        SELECT t.*, p.displaystyle AS threadprefix
        FROM ".TABLE_PREFIX."threads t
        LEFT JOIN ".TABLE_PREFIX."threadprefixes p ON(p.pid=t.prefix)
        WHERE t.fid IN(".$mybb->settings['modtools_forums'].") AND t.uid = t.lastposteruid and t.sticky = 0
    ORDER BY t.lastpost
    ");
}

while($thread = $db->fetch_array($query))
{
    $bgcolor = alt_trow();
    $prefix = '';

    if($thread['userusername'])
    {
        $thread['username'] = $thread['userusername'];
    }

    $thread['profilelink'] = build_profile_link($thread['username'], $thread['uid']);
			
    // If this thread has a prefix, insert a space between prefix and subject
    if($thread['prefix'] != 0)
    {
        $thread['threadprefix'] .= '&nbsp;';
    }
			
    $thread['subject'] = htmlspecialchars_uni($thread['subject']);

    // Stuff to do with the last poster
    $lastpostdate = my_date($mybb->settings['dateformat'], $thread['lastpost']);
    $lastposttime = my_date($mybb->settings['timeformat'], $thread['lastpost']);
    $thread['lastpostlink'] = get_thread_link($thread['tid'], 0, "lastpost");
    $lastposteruid = $thread['lastposteruid'];
    $thread_link = get_thread_link($thread['tid']);

    // Don't link to guest's profiles (they have no profile).
    if($lastposteruid == 0)
    {
        $lastposterlink = $thread['lastposter'];
    }
    else
    {
        $lastposterlink = build_profile_link($thread['lastposter'], $lastposteruid);
    }

    $thread['replies'] = my_number_format($thread['replies']);

    // Do the whole point of this table--time since last update!
    $now = new DateTime();
    $updated = new DateTime();
    $updated->setTimestamp($thread['lastpost']);
    $since_update = $now->diff($updated);
    $hours_since_update = $since_update->days * 24 + $since_updated->h;

    if ($hours_since_update >= 48)
    {
        $color = "red";
    } else if ($hours_since_update >= 24)
   {
       $color = "orange";
   } else {
       $color = "green";
   }

    eval("\$threads .= \"".$templates->get("rpg_updatetable_thread")."\";");

    $threadcount++;
}

// Did we get anything?
if($threadcount == 0)
{
    error($lang->modtools_no_updates_required);
}

if(!$mybb->settings['maxmultipagelinks'])
{
    $mybb->settings['maxmultipagelinks'] = 5;
}

/*
fb($mybb->settings['modtools_enable']);
fb($mybb->settings['modtools_groups']);
fb($mybb->settings['modtools_forums']);
fb($mybb->usergroup);
*/

eval("\$modcp_nav = \"".$templates->get("rpg_updatetable")."\";");
output_page($modcp_nav);

?>
