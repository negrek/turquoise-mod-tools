<?php

if(!defined("IN_MYBB"))
{
    die("You cannot access this file directly. Please make sure IN_MYBB is defined.");
}

/*
require_once('FirePHPCore/fb.php');
ob_start();
*/

function updatetable_info()
{
    return array(
        "name" => "Turquoise Update Table",
        "description" => "Tracks what players need updates and how long they've been waiting for them.",
        "website" => "http://pokemonturquoise.com",
        "author" => "Negrek",
        "authorsite" => "http://thousandroads.net",
        "version" => "1.0",
        "guid" => "",
        "compatibility" => "16*"
    );
}

function updatetable_activate() {

    global $db;

    $updatetable_group = array(
        'gid' => 'NULL',
        'name' => 'updatetable',
        'title' => 'Turquoise Update Table',
        'description' => 'Settings For Turquoise Update Table',
        'disporder' => '1',
        isdefault => '0',
    );

    $db->insert_query('settinggroups', $updatetable_group);

    $gid = $db->insert_id();

    $updatetable_enable = array(
        'sid' => 'NULL',
        'name' => 'updatetable_enable',
        'title' => 'Do you want to enable the update table?',
        'description' => 'If you set this option to "on", this plugin will be active on your board.',
        'optionscode' => 'onoff',
        'value' => '1',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    $updatetable_prefixes = array(
        'sid' => 'NULL',
        'name' => 'updatetable_prefixes',
        'title' => 'Thread prefixes to exclude from the update table',
        'description' => "Enter the ID\'s of thread prefixes you would NOT like included in the update table--the ones that don\'t require updating (e.g. inactive threads, IRP threads). Enter them as a comma-separated list (e.g. \'1,2,4\').",
        'optionscode' => 'text',
        'value' => '',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    $db->insert_query('settings', $updatetable_enable);
    $db->insert_query('settings', $updatetable_prefixes);

    rebuild_settings();

}

function updatetable_deactivate() {

    global $db;

    $db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name IN ('updatetable_enable', 'updatetable_prefixes')");

    $db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='updatetable'");

    rebuild_settings();

}

?>
