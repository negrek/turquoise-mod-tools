<?php

if(!defined("IN_MYBB"))
{
    die("You cannot access this file directly. Please make sure IN_MYBB is defined.");
}

/*
require_once('FirePHPCore/fb.php');
ob_start();
*/

function random_mycode_info()
{
    return array(
        "name" => "Random MyCode",
        "description" => "Adds a custom MyCode that allows the user to define a list of items, one of which will appear at random each time the code is viewed.",
        "website" => "http://pokemonturquoise.com",
        "author" => "Negrek",
        "authorsite" => "http://thousandroads.net",
        "version" => "1.0",
        "guid" => "",
        "compatibility" => "16*"
    );
}

$plugins->add_hook("parse_message", "parse_random_mycode");

function parse_random_mycode($message) {

    global $parser;

    if ($parser->options['allow_mycode']) {

        $message = preg_replace_callback(
            '#\[random\](.*?)\[/random\]#is',
            function ($matches) {
                // Split match on pipes
                $choices = explode('|', $matches[1]);

                // Pick something at random
                return $choices[array_rand($choices)];
            },
            $message
        );

    }

    return $message;
}

?>
