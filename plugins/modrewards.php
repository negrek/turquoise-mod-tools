<?php

if(!defined("IN_MYBB"))
{
    die("You cannot access this file directly. Please make sure IN_MYBB is defined.");
}

require_once('FirePHPCore/fb.php');
ob_start();

$plugins->add_hook('newreply_start', 'modrewards_log');
// Not sure if right hook; need some of the post's info, so...


function modrewards_info()
{
    return array(
        "name" => "Mod Rewards",
        "description" => "Keeps track of the updates a mod makes and allows them to be exchanged for cool prizes.",
        "website" => "http://pokemonturquoise.com",
        "author" => "Negrek",
        "authorsite" => "http://thousandroads.net",
        "version" => "1.0",
        "guid" => "",
        "compatibility" => "16*"
    );
}

function modrewards_activate() {

    global $db;

    $modrewards_group = array(
        'gid' => 'NULL',
        'name' => 'modrewards',
        'title' => 'Mod Rewards',
        'description' => 'Settings For Mod Rewards',
        'disporder' => '1',
        isdefault => '0',
    );

    $db->insert_query('settinggroups', $modrewards_group);

    $gid = $db->insert_id();

    $modrewards_enable = array(
        'sid' => 'NULL',
        'name' => 'modrewards_enable',
        'title' => 'Do you want to enable mod rewards?',
        'description' => 'If you set this option to "on", this plugin will be active on your board.',
        'optionscode' => 'onoff',
        'value' => '1',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    // Define the user groups affected by this query
    $modrewards_groups = array(
        'sid' => 'NULL',
        'name' => 'modrewards_groups',
        'title' => 'Which groups get rewards?',
        'description' => "Specify the user group ID\'s that are eligible for mod rewards. Enter them as a comma-separated list (e.g. \'1,2,4\').",
        'optionscode' => 'text',
        'value' => '',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    // Define the forums affected by this query
    $modrewards_forums = array(
        'sid' => 'NULL',
        'name' => 'modrewards_forums',
        'title' => 'Which forums are affected?',
        'description' => "Specify the forum ID\'s that are part of the RP and which mods should get rewards for updating in. Enter them as a comma-separated list (e.g. \'1,2,4\').",
        'optionscode' => 'text',
        'value' => '',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    // Define the target thread where results are posted
    $modrewards_thread = array(
        'sid' => 'NULL',
        'name' => 'modrewards_thread',
        'title' => 'What thread tracks updates?',
        'description' => "Specify the ID of the thread where mod updates will be tracked and mods will be able to claim their rewards.",
        'optionscode' => 'text',
        'value' => '',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    $db -> insert_query('settings', $modrewards_enable);
    $db -> insert_query('settings', $modrewards_groups);
    $db -> insert_query('settings', $modrewards_forums);
    $db -> insert_query('settings', $modrewards_thread);

    rebuild_settings();

}

function modrewards_deactivate() {

    global $db;

    $db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name IN ('modrewards_enable')");

    $db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='modrewards'");

    rebuild_settings();

}

function modrewards_log() {

    global $db;
    global $mybb;
    global $fid;

    // Is the plugin active?
    if ($mybb->settings['modrewards_enable'] == 1) {

        // Get list of RP moderator groups
        $mod_ids = array_map("intval", explode(',', $mybb->settings['modrewards_groups']));

        // Is this reply coming from an RPG mod?
        if (in_array($mybb->usergroup['gid'], $mod_ids))
        {

            // Get list of RP forums
            $forum_ids = array_map("intval", explode(',', $mybb->settings['modrewards_forums']));

            // In an RPG thread?
            if(in_array($fid, $forum_ids))
            {

                fb($mybb);
                // That they didn't start?
        // Then add the post link to the clearinghouse
            }
        }
    }
}

?>
