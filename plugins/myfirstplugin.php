<?php

if(!defined("IN_MYBB"))
{
    die("You cannot access this file directly. Please make sure IN_MYBB is defined.");
}

$plugins->add_hook('global_start', 'myfirstplugin_global_start');

function myfirstplugin_info()
{
    return array(
        "name" => "My First Plugin",
        "description" => "This is my first plugin! :)",
        "website" => "http://pokemonturquoise.net",
        "author" => "Negrek",
        "authorsite" => "http://thousandroads.net",
        "version" => "1.0",
        "guid" => "",
        "compatibility" => "16*"
    );
}

function myfirstplugin_activate() {

    global $db;

    $myfirstplugin_group = array(
        'gid' => 'NULL',
        'name' => 'myfirstplugin',
        'title' => 'My First Plugin',
        'description' => 'Settings For My First Plugin',
        'disporder' => '1',
        isdefault => '0',
    );

    $db->insert_query('settinggroups', $myfirstplugin_group);

    $gid = $db->insert_id();

    $myfirstplugin_setting = array(
        'sid' => 'NULL',
        'name' => 'myfirstplugin_enable',
        'title' => 'Do you want to enable My First Plugin?',
        'description' => 'If you set this option to yes, this plugin will be active on your board.',
        'optionscode' => 'yesno',
        'value' => '1',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    $db -> insert_query('settings', $myfirstplugin_setting);

    rebuild_settings();

}

function myfirstplugin_deactivate() {

    global $db;

    $db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name IN ('myfirstplugin_enable')");

    $db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='myfirstplugin'");

    rebuild_settings();

}

function myfirstplugin_global_start() {

    global $mybb;

    if ($mybb->settings['myfirstplugin_enable'] == 1) {

        echo "My first plugin works!";
    }
}

?>
