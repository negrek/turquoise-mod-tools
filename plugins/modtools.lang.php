<?php
/*
 * Plug-in Name: Turquoise Mod Tools
 * Copyright 2014 Negrek
 * http://thousandroads.net
 *
 * contains language used in various modtools sub-plugins and pages
*/

// Update table phrases
$l['modtools_updatetable'] = 'Players Awaiting Updates';
$l['modtools_no_updates_required'] = 'Looks like nobody needs an update right now. Nice work!';
$l['modtools_time_since_last_update'] = 'Hours Since Last Update';
