<?php

if(!defined("IN_MYBB"))
{
    die("You cannot access this file directly. Please make sure IN_MYBB is defined.");
}

$plugins->add_hook('global_start', 'cronsmoother_global_start');
//$plugins->add_hook('showthread_linear', 'cronsmoother_global_start');
//$plugins->add_hook('showthread_threaded', 'cronsmoother_global_start');
// Immediately before this, calls "build_postbit" on "showpost"
// This returns a bunch of templates, so it's probably easier to
// modify the postbit variable, then re-run it on the modified post(s)


function cronsmoother_info()
{
    return array(
        "name" => "Cron Smoother",
        "description" => "Sorts out time zone issues to ensure scheduled tasks run reliably.",
        "website" => "http://mybb.com",
        "author" => "pu6",
        "authorsite" => "http://mybb.com",
        "version" => "1.3.1",
        "guid" => "",
        "compatibility" => "16*"
    );
}

function cronsmoother_activate() {

    global $db;

    $cronsmoother_group = array(
        'gid' => 'NULL',
        'name' => 'cronsmoother',
        'title' => 'Cron Smoother',
        'description' => 'Settings For Cron Smoother',
        'disporder' => '1',
        isdefault => '0',
    );

    $db->insert_query('settinggroups', $cronsmoother_group);

    $gid = $db->insert_id();

    $cronsmoother_setting = array(
        'sid' => 'NULL',
        'name' => 'cronsmoother_enable',
        'title' => 'Do you want to enable Cron Smoother?',
        'description' => 'If you set this option to yes, this plugin will be active on your board.',
        'optionscode' => 'yesno',
        'value' => '1',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    $db -> insert_query('settings', $cronsmoother_setting);

    rebuild_settings();

}

function cronsmoother_deactivate() {

    global $db;

    $db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name IN ('cronsmoother_enable')");

    $db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='cronsmoother'");

    rebuild_settings();

}

function cronsmoother_global_start() {

    global $mybb;

    if ($mybb->settings['cronsmoother_enable'] == 1) {

        echo $showpost;
    }
}

?>
