<?php

if(!defined("IN_MYBB"))
{
    die("You cannot access this file directly. Please make sure IN_MYBB is defined.");
}

/*
require_once('FirePHPCore/fb.php');
ob_start();
*/

function modtools_info()
{
    return array(
        "name" => "Turquoise Mod Tools",
        "description" => "A plugin containing general functions and settings to be used by other moderation plugins.",
        "website" => "http://pokemonturquoise.com",
        "author" => "Negrek",
        "authorsite" => "http://thousandroads.net",
        "version" => "1.0",
        "guid" => "",
        "compatibility" => "16*"
    );
}

function modtools_activate() {

    global $db;

    $modtools_group = array(
        'gid' => 'NULL',
        'name' => 'modtools',
        'title' => 'Turquoise Mod Tools',
        'description' => 'Settings For Turquoise Mod Tools',
        'disporder' => '1',
        isdefault => '0',
    );

    $db->insert_query('settinggroups', $modtools_group);


    $gid = $db->insert_id();

    $modtools_enable = array(
        'sid' => 'NULL',
        'name' => 'modtools_enable',
        'title' => 'Do you want to enable mod tools?',
        'description' => "If you set this option to ''on'', this plugin will be active on your board. Note that if this plugin is disabled, you won''t be able to use any of the associated moderator plugins--and they may act very strangely!",
        'optionscode' => 'onoff',
        'value' => '1',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    // Define the forums affected by this query
    $modtools_forums = array(
        'sid' => 'NULL',
        'name' => 'modtools_forums',
        'title' => 'Which forums are affected?',
        'description' => "Specify the forum ID\'s that are part of the RP and where these tools should be available. Enter them as a comma-separated list (e.g. \'1,2,4\').",
        'optionscode' => 'text',
        'value' => '',
        'disporder' => 1,
        'gid' => intval($gid),
    );

    $db -> insert_query('settings', $modtools_enable);
    $db -> insert_query('settings', $modtools_groups);
    $db -> insert_query('settings', $modtools_forums);

    rebuild_settings();

}

function modtools_deactivate() {

    global $db;

    $db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name IN ('modtools_enable', 'modtools_groups', 'modtools_forums')");

    $db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='modtools'");

    rebuild_settings();

}

?>
