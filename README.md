# Pokémon Turquoise Mod Tools

This is a collection of utilities intended to make it easy for moderators to update player adventures in the [Pokémon Turquoise RPG](http://pokemonturquoise.com). It currently includes tools for generating wild pokémon, generic trainer NPC's, and for calculating damage in battle.

## Installation

These scripts run on [Python2.7](http://www.python.org/download/) and [Node.js](http://nodejs.org); see their respective sites for detailed installation instructions. The [Python package manager pip](http://www.pip-installer.org/en/latest/installing.html) is also recommended. In addition, the following packages are required:

* [The `requests` package](http://docs.python-requests.org/en/latest/)

    `$ pip install requests`

    Or see [the requests website](http://www.python-requests.org/en/latest/user/install/) for instructions on installing from source.

* [commander.js](https://github.com/visionmedia/commander.js/)

    `$ npm install commander`

Then move the file `turquoise-example.cfg` out of the `resource` directory and into the main directory and rename it `turquoise.cfg`. In order to use any of the utilities that automatically edit player profiles, you'll need to fill out the "username" and "password" fields with your actual username and password (not in quotation marks). In addition, if you feel the need to alter the directory structure or run the scripts off your own database(s) for whatever reason, be sure to edit the paths in this file to point to their new location.

## Generating Wild Pokémon

```
$ vimperator_wild.py <URL> [--area AREA]
```

Simply navigate to the thread you want to generate a wild encounter for and copy and paste its url into the command line (surrounded by quotation marks). If the area being searched for wilds contains any sub-areas (e.g. "Upper Caverns" vs "Lower Caverns"), the sub-area to be searched **must** be specified.

Note that the script assumes that the **last** post in the thread URL given represents the player you want to generate a wild for. If the last post is by the mod (or someone else), the pokémon generated will be based on **their** party and may be of an inappropriate level as a result.

The script rolls for all aspects of a pokémon: level, species, sex, ability, wild held item, and shininess. No further randomization should be necessary; the output can be directly pasted back into the thread without modification. Sample output:

```
A wild Paras appeared!
[5] [sp=f][sp=paras] 23/23 [Dry Skin]
```

As noted above, the URL should be quoted; likewise multi-part area names. Thus:

```
$ vimperator_wild.py 'http://pokemonturquoise.com/showthread.php?tid=354' --area 'Upper Caverns'
```

**NOT**

```
$ vimperator_wild.py http://pokemonturquoise.com/showthread.php?tid=354 --area Upper Caverns
```

## Generating Trainers

```
$ vimperator_trainer.py <URL>
```

Operation is essentially identical to that of the wild generation script, except that no area need be specified. As with the wild pokémon generator, the URL should be provided in quotation marks. The output is ready to be pasted directly back into the thread, although it is probably a good idea to glance at the team produced to be sure it's appropriate for the play. Sample output:

```
Baker Shirley would like to battle!
[img]http://thousandroads.net/stuff/turquoise/images/baker.png[/img]
Shirley sent out Fallorite!
[3] [sp=na][sp=fallorite] 18/18 [Levitate]
[spoiler=For Later]
[3] [sp=f][sp=freye] 20/20 [Damp] (Knows Sludge Wave)
[6] [sp=f][sp=donarith] 30/30 [Compoundeyes]
[3] [sp=male][sp=aeolagio] 23/23 [Poison Point]
[4] [sp=f][sp=snorunt] 21/21 [Ice Body]
[/spoiler]
```

Note that held items are not chosen for any trainer pokémon; if you want some, you'll have to add them yourself.

## Generating Arbitrary Pokemon

```
$ generate_pokemon.py species level
```

At times you may wish to generate a specific pokémon, such as for a plot trainer. You can use `generate_pokemon.py` to produce any number of pokémon of a set species and level. For example, if you wanted to create a Baaresa Town Revivalist tester with a klaitning, medicham, and ampharos, you could use:

```
./generate_pokemon.py klaitning 28 medicham 30 ampharos 32 -r
```

Sample output:

```
[28] [sp=f][sp=klaitning] 92/92 [Volt Absorb]
[30] [sp=male][sp=medicham] 104/104 [Pure Power]
[32] [sp=f][sp=ampharos] 129/129 [Static] (Knows Thunderbolt)
```

The `-r` flag will randomly generate "special moves" for the pokémon, according to the same rules used to add them to trainers. If you want to force a pokémon to have a particular number of special moves, you can use the `-m` option to specify how many you'd like. The `-i` option rolls for wild held items, and the `-s` flag gives the pokémon a chance at being shiny.

Note that all options set this way apply to __all__ pokémon entered with the command. If you want only some of them to have certain attributes, you'll need to make multiple calls to `generate_pokemon`.

## Gift Pokémon

```
$ generate_pokemon.py species level -p <URL>
```

You may also wish to add an arbitrary pokémon to a player's profile, such as when adding a birthday promo or a player's starter pokémon. `generate_pokemon` can be used to determine all of the pokémon's attributes, then automatically edit it into the player's profile. For example, let's say I request a birthday promo minijina named Belladonna if female and Medianoche if male. You could send me one by running the following command:

The only output produced by this script is a success or error message. You'll need to visit the target player's profile to see the final result.

```
./generate_pokemon.py minijina 5 -s -p 'http://pokemonturquoise.com/showthread.php?tid=125' --nickname_f Belladonna --nickname_m Medianoche
```

All options described in the section above can be used with this mode of the script as well (e.g. `-s` gives the pokémon a chance to be shiny). In addition, you can either set a nickname regardless of the pokémon's sex using the `--nickname` option, or specify male/female-specific names with `--nickname_m` and `--nickname_f`. The more specific names will override the generic nickname option if they're given together.

You can also use the `-l` command to specify whether to send the pokémon to the player's team or PC box, or `-o` if you want the pokémon to have an original trainer other than the player themselves.

## Calculating Damage

```
$ node minicalc.js <attacker> <level> <Attack> <defender> <level> [options...]
```

At its most basic, the calculator is supposed to make it easy to produce a full round of damage calculations in a single step. The most important option is `-r`: this is the "response" option, which specifies the attack that the defending pokémon uses against the attacker. For example, if you have a level five crocoal tackling a level five mareep, who responds with thundershock, the command is:

```
$ node minicalc.js crocoal 5 Tackle mareep 5 -r 'Thunder Shock'
```

Producing the following output:

```
crocoal attacks first!
[ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6 ]
undefined undefined Tackle vs. undefined / undefined undefined
5
crocoal attacks first!
[ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7 ]
undefined undefined Thunder Shock vs. undefined / undefined undefined
6
```

The attack's information always appears before the defender's information in cases where the `-r` flag is used. The speed of each attacking pokémon is calculated, and which pokémon should move first, ignoring priority, is printed for your convenience. In the case of a speed tie, there is an equal chance of each pokémon being displayed in this line, so you can safely take the first line of output to determine which pokémon should move first. For each attack, the range of possible damage rolls is given, as well as a summary line that will note any additional effects on the attack--attack or defense drops, the presence of a light screen, if the attack scored a critical, and so on. Finally, the actual outcome of the damage roll is given; this is the value you should use when deducting HP from the pokémon. 

There are numerous other options and flags related to battle conditions so that you can account for things like stat boosts and drops, item and ability effects, the use of helping hand, and so on. For a complete list and further instructions, use the `--help` flag. The strangest notation is probably used on stat boosts and drops: a positive boost should be given as a "p" followed by the number of stages, and a negative boost as an "n" followed by the number of stages. Thus, if you wanted to factor in a pokémon's +3 defense, you'd pass `-D p3`, and a two-stage attack drop would look like `-A n2`. Note that you do **not** need to give parameters for STAB or type effectiveness: these are both automatically accounted for.

An unfortunate shortcoming of the `-r` command is that it applies the exact same battle modifiers to both the attacker and defender when rolling damage, so you will need to calculate each attack individually unless both attacker and defender fortuitously have the same attack boost/ability/whatever.

Another trap to avoid is the fact that the names of pokémon given to the script should be in **lower** case, while attack names must be **upper** case. If you receive an error along the lines of "Cannot read property '0' of undefined", improper capitalization is probably the culprit (spelling mistakes will do it too, though). Note that some attacks are two words, like Solar Beam and Thunder Shock, and those will need to be quoted in order for the calculator to work correctly.

Note that the script **does** roll critical hits automatically, but **does not** roll secondary attack effects; you'll need to handle that yourself. Additionally, the critical stage of all moves is accounted for automatically, so you don't need to do anything special to account for the fact that razor leaf has an elevated critical rate, for example. However, if the critical rate is modified in some other way, such as by an item or ability, you will need to manually enter the modifier using the `-c` option. Note that the value passed to this flag is the number of stages **added** to the crit rate, not the final crit stage after all modifications have been accounted for. Therefore, if a spraylet uses focus energy, you'd want to add `-c 2` to its command whether it's using water pulse or slash; slash will still get the correct stage-3 crit modifier overall. 
